#include "clusterTree.h"
#include <iostream>

clusterTree::clusterTree()
{
    //root = NULL;
}

void clusterTree::traverse(node *node, std::vector<int> &leaves)
{
    //in-order traversal
    //append each leaf we encounter to the vector
    if (node->left == NULL && node->right == NULL){
        //this is a leaf; add it
        leaves.push_back(node->data);
    }
    else{
        if (node->left != NULL){
            traverse(node->left, leaves);
        }
        if (node->right != NULL){
            traverse(node->right, leaves);
        }
    }
}

std::vector<int> clusterTree::get_permutation(node *root_node)
{
    std::vector<int> leaves;
    traverse(root_node, leaves);
    return leaves;
}

std::vector<int> seriate()
{
    //implement me
}

std::vector<clusterTree::node *> clusterTree::get_nodes_at_level(node *node, int level)
{
    std::vector<clusterTree::node *> nodes;
    std::vector<clusterTree::node *> old_nodes;
    nodes.push_back(node);
    int i = 0;
    while (i < level){
        old_nodes = nodes;
        nodes.clear();
        for (int j = 0; j < old_nodes.size(); j++)
        {
            if (old_nodes[j]->left != NULL)
            {
                nodes.push_back(old_nodes[j]->left);
            }
            if (old_nodes[j]->right != NULL)
            {
                nodes.push_back(old_nodes[j]->right);
            }
        }
        i++;
    }

    return nodes;
}

std::vector<clusterTree::node *> clusterTree::get_n_sub_nodes(node *node, int n)
{
    std::vector<clusterTree::node *> nodes;
    std::vector<clusterTree::node *> old_nodes;
    nodes.push_back(node);
    int i = 0;
    do {
        old_nodes = nodes;
        nodes.clear();
        for (int j = 0; j < old_nodes.size(); j++)
        {
            if (old_nodes[j]->left != NULL)
            {
                nodes.push_back(old_nodes[j]->left);
            }
            if (old_nodes[j]->right != NULL)
            {
                nodes.push_back(old_nodes[j]->right);
            }
        }
        i++;
        if (i > 400) break;
    } while (nodes.size() < n);

    //FIXME: need to break if we iterate more times than number of levels
    return nodes;
}

void clusterTree::insert(std::vector<int> data, int parent, int level)
{
    std::vector<node*> possible_leaves = get_nodes_at_level(root, level);
    for (int i = 0; i < possible_leaves.size(); i++)
    {
        if (possible_leaves[i]->data == parent)
        {
            possible_leaves[i]->right = new node;
            possible_leaves[i]->right->data = (data[0] > data[1])? data[0] : data[1]; //max
            possible_leaves[i]->left = new node;
            possible_leaves[i]->left->data = (data[0] > data[1])? data[1] : data[0]; //min
        }
        if (possible_leaves[i]->data != parent
                && possible_leaves[i]->left == NULL
                && possible_leaves[i]->right == NULL)
        {
            possible_leaves[i]->right = new node;
            possible_leaves[i]->right->data = possible_leaves[i]->data;
        }
    }
    possible_leaves.clear();
}

void clusterTree::descendant_indices(node *node, std::vector<int>& index_list)
{
    if (node->left != NULL && node->right != NULL)
    {
        index_list.push_back(node->left->data);
        index_list.push_back(node->right->data);
        descendant_indices(node->left, index_list);
        descendant_indices(node->right, index_list);
    }
    if (node->right != NULL && node->left == NULL)
    {
        descendant_indices(node->right, index_list);
    }
}

void clusterTree::turn_on_descendants(node *node)
{
    node->flag = true;
    if (node->left != NULL && node->right != NULL)
    {
        turn_on_descendants(node->left);
        turn_on_descendants(node->right);
    }
    if (node->right != NULL && node->left == NULL)
    {
        turn_on_descendants(node->right);
    }
}

std::vector<int> clusterTree::descendant_indices_n_clusters(node *node, int n, int dataSize)
{
    std::vector<clusterTree::node *> nodes = get_n_sub_nodes(node, n);
    std::vector<int> cluster_assignments;
    cluster_assignments.resize(dataSize);

    int cluster_label = 1;
    for (int i = 0; i < nodes.size(); i++)
    {
        //get the indices of all trees that end up in this cluster
        std::vector<int> return_list;
        std::vector<int> index_list;
        if (nodes[i]->data <= dataSize){  //if the cluster is a single tree:
            return_list.push_back(nodes[i]->data);
        }

        if (nodes[i]->left != NULL)
        {
           descendant_indices(nodes[i]->left, index_list);
        }
        if (nodes[i]->right != NULL)
        {
            descendant_indices(nodes[i]->right, index_list);
        }
        for (int j = 0; j < index_list.size(); j++)
        {
            if ((index_list[j] != NULL || std::to_string(index_list[j]) == "0") && index_list[j] <= dataSize)
            {
                return_list.push_back(index_list[j]);
            }
        }
        for (int j = 0; j < return_list.size(); j++)
        {
            if (return_list[j] == 0) std::cout << "ADDING 0: " << return_list[j] << std::endl;
            cluster_assignments[return_list[j]] = cluster_label;
        }
        cluster_label++;
    }
    return cluster_assignments;
}

std::vector<int> clusterTree::get_clusters(node *node, std::vector<std::vector<int> > linkage, int level)
{
    return descendant_indices_n_clusters(node, level, linkage.size()+1);
}

