#ifndef AXIS_H
#define AXIS_H

#include <QtGui>
#include <QWidget>
#include <QMouseEvent>
#include <vector>

class Axis : public QWidget
{
    Q_OBJECT

public:
    explicit Axis(QWidget *parent = 0);
    void setHorizontal(bool ori);
    void setRight(bool r);
    void addSlider(float pos);

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    float min; float max; //data range
    float datamin; float datamax;
    int tickcount;
    float fontsize;
    QString label;
    bool horizontal;
    bool right;
    bool slider_on;
    float slider_pos;
    void moveSlider(float pos);
    std::vector<float> labelPositions;
    std::vector<int> labelNames;

signals:
    void newSliderPos(int pos);

public slots:
    void setRange(float min, float max);
    void scaleRange(float min, float max);
    void setFontSize(float f);
    void setTickCount(int f);
    void setLabel(QString l);
    void addTick(float frac, int name);
    void resetTicks();
    void setColor(QColor c);
};

#endif // VIEW_H
