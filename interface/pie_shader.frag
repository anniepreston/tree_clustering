#version 330
in vec4 out_color;
in vec2 out_pos;
in vec2 out_center;

uniform float width;
uniform float height;

out vec4 FragColor;

void main(void)
{
    float radius = 0.35;

    float ratio = float(height/width);

    if (sqrt(pow(out_pos.x - out_center.x, 2)
             + pow((out_pos.y - out_center.y)*ratio, 2)) < radius){
        FragColor = vec4(out_color.x/255., out_color.y/255., out_color.z/255., out_color.w);
    }
    else FragColor = vec4(0.0, 0.0, 0.0, 0.0);
}
