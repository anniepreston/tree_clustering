#ifndef PHASEPLOT_H
#define PHASEPLOT_H

#include "dataManage.h"
#include <QGLWidget>
#include "GLShaderProgram.h"

class PhasePlot : public QGLWidget
{
    Q_OBJECT
public:
    PhasePlot(dataManage* dataManager = 0, QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();

private:
    int n;

    std::unique_ptr<dataManage> dataManager;
    //shaders
    GLShaderProgram::Ptr phaseProgram;

    GLVertexArray::Ptr phaseVAO;
    GLArrayBuffer::Ptr colorVBO;
    GLArrayBuffer::Ptr posVBO;
    GLArrayBuffer::Ptr velVBO;
    GLArrayBuffer::Ptr offsetVBO;
    GLArrayBuffer::Ptr relMassVBO;
    GLArrayBuffer::Ptr timeVBO;

    void createPhasePlot();
    void setShader();

    std::vector<int> getNIndicesPerCluster();
    Vec4f calcDists(Vec3f pos, Vec3f ref, Vec4f &max);
    Vec4f calcVelDiffs(Vec3f vel, Vec3f ref, Vec4f &max, Vec4f &min);

    std::string y_var;
    std::string y_component;

    bool same_n_per_cluster = true;

public slots:
    void refresh();

};

#endif // PHASEPLOT_H
