#version 330
in vec2 in_position;
in vec3 in_color;

out vec3 out_color;

void main(void)
{
    out_color = in_color;
    gl_Position = vec4(2*in_position.x-1., -(2*in_position.y-1.), 0, 1);
}
