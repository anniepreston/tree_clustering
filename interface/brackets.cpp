#include "brackets.h"
#include <QPainter>
#include <iostream>

Brackets::Brackets(QWidget *parent)
    : QWidget(parent)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

void Brackets::paintEvent(QPaintEvent*)
{
    for(int i = 0; i < 5; i++){
        QPoint bracket[4] = {
            QPoint(0, i*180),
            QPoint(30, 10+i*180),
            QPoint(30, 170+i*180),
            QPoint(0, 180+i*180)
        };
        QPainter painter(this);
        QColor color = color_set[i];
        QPen pen(color);
        pen.setWidth(5);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen(pen);
        painter.setBrush(color);
        painter.drawPolyline(bracket, 4);

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    emit bracketsDrawn();
}

void Brackets::drawBrackets()
{
    update();
}

