#include "pieplot.h"
#include <vector>
#include <cmath> //trig
#include <math.h>

piePlot::piePlot(dataManage *dataManager, QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent), dataManager(dataManager)
{
}

void piePlot::initializeGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    pieVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    centerVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    auto cluster_sizes = dataManager->getClusterSizes();
    createPies(cluster_sizes);

    pieShader = GLShaderProgram::create(dataManager->path + "pie_shader.vert", dataManager->path + "pie_shader.frag");
    setShaders();

    paintGL();
    doneCurrent();
}

void piePlot::createPies(std::vector<int> cs){
    std::vector<Vec2f> posArray;
    std::vector<Vec4f> colorArray;
    std::vector<Vec2f> centerArray;

    int max = 0;
    float x_offset = 0.5;
    float delta = 10.0;
    for (auto & c : cs) max += c;

    for (int i = 0; i < cs.size(); i++){
        float y_offset = 1.0-(float(i)+float(i+1))/(2*float(cs.size()));
        Vec2f pos_prev = Vec2f(0, delta);
        float angle_prev = 0.0;
        for (int j = 0; j < cs.size(); j++){
            float frac = cs[j]/float(max);
            float angle = frac*360;
            if (frac < 0.5){
                if (angle + angle_prev <= 90){ //I
                    float theta = (angle + angle_prev)*(M_PI)/180.;
                    float adj = delta*cos(theta);
                    float opp = delta*sin(theta);

                    posArray.push_back(Vec2f(pos_prev.x + x_offset, pos_prev.y + y_offset));
                    posArray.push_back(Vec2f(x_offset, y_offset));
                    posArray.push_back(Vec2f(opp + x_offset, adj + y_offset));
                    pos_prev = Vec2f(opp, adj);
                }
                else if (angle + angle_prev <= 180 && angle + angle_prev > 90){ //II
                    float theta = (angle + angle_prev - 90)*(M_PI)/180.;
                    float adj = delta*cos(theta);
                    float opp = delta*sin(theta);

                    posArray.push_back(Vec2f(pos_prev.x + x_offset, pos_prev.y + y_offset));
                    posArray.push_back(Vec2f(x_offset, y_offset));
                    posArray.push_back(Vec2f(adj + x_offset, -opp + y_offset));
                    pos_prev = Vec2f(adj, -opp);
                }
                else if (angle + angle_prev <= 270 && angle + angle_prev > 180){ //III
                    float theta = (angle + angle_prev - 180)*(M_PI)/180.;
                    float adj = delta*cos(theta);
                    float opp = delta*sin(theta);

                    posArray.push_back(Vec2f(pos_prev.x + x_offset, pos_prev.y + y_offset));
                    posArray.push_back(Vec2f(x_offset, y_offset));
                    posArray.push_back(Vec2f(-opp + x_offset, -adj + y_offset));
                    pos_prev = Vec2f(-opp, -adj);
                }
                else if (angle + angle_prev <= 360 && angle + angle_prev > 270){ //IV
                    float theta = (angle + angle_prev - 270)*(M_PI)/180.;
                    float adj = delta*cos(theta);
                    float opp = delta*sin(theta);

                    posArray.push_back(Vec2f(pos_prev.x + x_offset, pos_prev.y + y_offset));
                    posArray.push_back(Vec2f(x_offset, y_offset));
                    posArray.push_back(Vec2f(-adj + x_offset, opp + y_offset));
                    pos_prev = Vec2f(-adj, opp);
                }
            }
            /*
            else{
                for (int k = 0; k < 4; k++){
                    float degs = float(k+1)*angle/4. + angle_prev;
                    float diff;
                    if (degs <= 90) diff = 0;
                    else if (degs > 90 && degs <= 180) diff = 90;
                    else if (degs > 180 && degs <= 270) diff = 180;
                    else if (degs > 270 && degs <= 360) diff = 270;

                    float theta = (degs - diff)*(M_PI)/180.;
                    float adj = delta*cos(theta);
                    float opp = delta*sin(theta);

                    posArray.push_back(Vec2f(pos_prev.x + x_offset, pos_prev.y + y_offset));
                    posArray.push_back(Vec2f(x_offset, y_offset));
                    float x, y;
                    if (degs <= 90) { x = opp; y = adj; }
                    else if (degs > 90 && degs <= 180) { x = adj; y = -opp; }
                    else if (degs > 180 && degs <= 270) { x = -opp, y = -adj; }
                    else if (degs > 270 && degs <= 360) { x = -adj; y = opp; }
                    posArray.push_back(Vec2f(x + x_offset, y + y_offset));
                    pos_prev = Vec2f(x, y);
                }
            }
            */

            angle_prev += angle;
            Vec3f color = dataManager->color_set[j];
            float alpha = 0.2;
            if (j == i){
                alpha = 1.0;
                //color = dataManager->color_set[j];
            }
            for (int k = 0; k < 3; k++){
                colorArray.push_back(Vec4f(color, alpha));
                centerArray.push_back(Vec2f(x_offset, y_offset));
            }
        }
    }

    pieVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_STATIC_DRAW);
    colorVBO->update(colorArray.size()*sizeof(Vec4f), &colorArray.front(), GL_STATIC_DRAW);
    centerVBO->update(centerArray.size()*sizeof(Vec2f), &centerArray.front(), GL_STATIC_DRAW);
    pieVAO = GLVertexArray::create();

    vboSize = posArray.size();
}

void piePlot::setShaders()
{
    pieShader->setVertexAttribute("in_center", pieVAO, centerVBO, 2, GL_FLOAT, false);
    pieShader->setVertexAttribute("in_position", pieVAO, pieVBO, 2, GL_FLOAT, false);
    pieShader->setVertexAttribute("in_color", pieVAO, colorVBO, 4, GL_FLOAT, false);

    pieShader->setUniform("width", float(width()));
    pieShader->setUniform("height", float(height()));
}

void piePlot::paintGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    int size = vboSize;

    pieShader->begin();
    pieVAO->drawArrays(GL_TRIANGLES, vboSize);
    pieShader->end();
}

void piePlot::resizeGL(int w, int h)
{
    makeCurrent();
    glViewport(0, 0, w, h);
    paintGL();
    doneCurrent();
}

void piePlot::refresh()
{
    makeCurrent();
    std::cout << "Pie: refresh" << std::endl;
    initializeGL();
    std::cout << "pie: update" << std::endl;
    update();
    std::cout << "pie: done refreshing" << std::endl;
}
