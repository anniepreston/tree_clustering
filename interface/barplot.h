#ifndef BARPLOT_H
#define BARPLOT_H

#include "dataManage.h"
#include <QGLWidget>

class barPlot : public QGLWidget
{
    Q_OBJECT
    dataManage* dataManager;
public:
    barPlot(dataManage* dataManager = 0, QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();

public slots:
    void refresh();

private:
    GLShaderProgram::Ptr barShader;

    GLVertexArray::Ptr barVAO;
    GLArrayBuffer::Ptr barVBO;
    GLArrayBuffer::Ptr colorVBO;

    void createBars(std::vector<int> cs);
    void setShaders();
};

#endif // BARPLOT_H
