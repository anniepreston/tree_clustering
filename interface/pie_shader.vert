#version 330
in vec2 in_position;
in vec4 in_color;
in vec2 in_center;

out vec4 out_color;
out vec2 out_center;
out vec2 out_pos;

void main(void)
{
    out_color = in_color;
    out_center = in_center;
    out_pos = in_position;

    gl_Position = vec4(-(2*in_position.x-1.), 2*in_position.y-1., 0, 1);
}
