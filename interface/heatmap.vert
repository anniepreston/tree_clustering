#version 330
in vec2 position;
in vec3 color;
in float alpha;

out vec3 out_color;
out float out_alpha;

void main(void)
{
    //scale position (0, 1) to (-1, 1)
    float x = (2*position.x - 1.0);
    float y = (2*position.y - 1.0);

    out_color = color;
    out_alpha = alpha;

    gl_Position = vec4(x, y, 0.0, 1.0);
}
