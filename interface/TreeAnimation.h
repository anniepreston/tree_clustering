#ifndef ANIMATION_H
#define ANIMATION_H
#include <QGLWidget>
#include "GLArrayBuffer.h"
#include "GLShaderProgram.h"
#include "GLVertexArray.h"
#include <QApplication>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <GLUT/glut.h>

class TreeAnimation : public QGLWidget
{
    Q_OBJECT

public:
    TreeAnimation(QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void drawScene();

    GLShaderProgram::Ptr treeProgram;
    GLVertexArray::Ptr treeVAO;
    GLArrayBuffer::Ptr posVBO;
    GLArrayBuffer::Ptr colorVBO;

public slots:
    void begin();
    void drawBins();

private:
    void loadTrees();
    void initShaders();
    void setShaderAttributes();
    void animateAddTrees();
    void binTrees();

    void setupDrawCallback();

    std::vector<std::vector<float>> allTrees; //FIXME: eventually share this with dataManage
    float min_mass; float max_mass;
    std::string path = "/Users/anniepreston/repos/tree_clustering/interface/";
    int tree_size;

    //FIXME: these color lists should be global
    std::vector<std::vector<int>> color_set = {{228, 26, 28}, {55, 126, 184}, {77, 175, 74}, {152, 78, 163}, {255, 127, 0}, {255, 255, 51}, {166, 86, 40}, {247, 129, 191}, {153, 153, 153}};

signals:
    void doneDrawingTrees();
    void treesDrawn(int n);
};

#endif // ANIMATION_H
