#include "Axis.h"
#include <iostream>

Axis::Axis(QWidget *parent)
    : QWidget(parent)
{
    datamin = 0;
    datamax = 100;
    min = datamin;
    max = datamax;

    label = "";
    fontsize = 14.0f;
    tickcount = 6;
    slider_on = false;
    slider_pos = 0.9;
}

void Axis::addSlider(float pos)
{
    slider_pos = pos;
    slider_on = true;
    repaint();
}

void Axis::moveSlider(float pos)
{
    slider_pos = pos;
    repaint();
}

void Axis::setLabel(QString l){
    label = l;
    repaint();
}

void Axis::setFontSize(float f){
    fontsize = f;
    repaint();
}

void Axis::setTickCount(int f){
    tickcount = f;
    repaint();
}

void Axis::setRange(float new_min, float new_max){ //do this upon loading a new data set
    datamin = new_min;
    datamax = new_max;
    float avg = 0.5*(new_max-new_min);

    //scale based on the [-0.9, 0.9] rendering window:
    //FIXME: use correct factor
    float fac = 0.9;
    datamin = avg - (1./fac)*(avg-datamin);
    datamax = avg + (1./fac)*(datamax-avg);
    min = datamin;
    max = datamax;

    repaint();
}

void Axis::scaleRange(float new_min, float new_max){ //do this  upon zooming/panning

    float temp_min = datamin + (1 + new_min) * 0.5 * (datamax-datamin);
    float temp_max = datamin + (1 + new_max) * 0.5 * (datamax-datamin);
    min = temp_min; max = temp_max;
    repaint();
}

void Axis::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::TextAntialiasing);

    QPen wpen(QColor(0,0,0,255));
    painter.setPen(wpen);
    QFont font = painter.font();
    font.setPointSizeF(fontsize);
    painter.setFont(font);

    float major_length;
    float minor_length;

    //draw axis:

    //horizontal case:
    if (horizontal == true) {
        painter.drawLine(0, 0.1*height(), width(), 0.1*height());
    }
    //vertical case:
    else if (horizontal == false && right == false) {
        major_length = height();
        minor_length = width();
        painter.drawLine(minor_length*.9,0,minor_length*.9,major_length);

        for (int j = 0; j < labelPositions.size(); j++){
            painter.drawText(QPoint(0.4*width(), labelPositions[j]*height() - 8), QString::number(labelNames[j], 'f', 0));
        }
    }
    else if (horizontal == false && right == true) {
        major_length = height();
        minor_length = width();
        painter.drawLine(minor_length*.1, 0, minor_length*.1, major_length);
        for (int j = 0; j < labelPositions.size(); j++){
            painter.drawText(QPoint(0.4*width(), labelPositions[j]*height() - 8), QString::number(labelNames[j], 'f', 0));
        }
    }

    //draw ticks:
    for (int i = 0; i <= tickcount; i++){
        if (horizontal == false && right == false){
            float value = min + float(float(i)/tickcount)*(max-min);
            float pos = height() - i*(major_length/tickcount);
            painter.drawLine(width()*0.8, pos, width()*0.9, pos);
            if (value >= 0) painter.drawText(QPoint(0.3*width(), pos), QString::number(value, 'g', 2));
        }
        else if (horizontal == false && right == true){
            float value = min + float(float(i)/tickcount)*(max-min);
            float pos = height() - i*(major_length/tickcount);
            painter.drawLine(width()*0.2, pos, width()*0.1, pos);
            if (value >= 0) painter.drawText(QPoint(0.3*width(), pos), QString::number(value, 'g', 2));
        }
        else {
            int value = int(min + float(float(i)/tickcount)*(max-min));
            float pos = i*(width()/tickcount);
            painter.drawLine(pos, 0.2*height(), pos, 0.1*height());
            if (value >= 0) painter.drawText(QPoint(pos + 5, 0.5*height()), QString::number(value));
        }
    }

    //draw label:
    if (horizontal == true) {
        painter.drawText(QRect(QPoint(0.05*width(),0.3*height()),QPoint(0.9*width(),0.8*height())),Qt::AlignLeading, label);
    }
    else if (horizontal == false){
        painter.save();
        painter.translate(0.05*width(), 0.7*height());
        painter.rotate(-90);
        painter.drawText(QRect(QPoint(0,0),QPoint(height(), width())), Qt::AlignHCenter, label);
        painter.restore();
    }
}

void Axis::setHorizontal(bool ori)
{
    this->horizontal = ori;
    repaint();
    update();
}

void Axis::setRight(bool r)
{
    this->right = r;
    repaint();
    update();
}

void Axis::mousePressEvent(QMouseEvent *event)
{
    if (horizontal == true) slider_pos = float(float(event->x())/float(this->width()));
    else if (horizontal == false) slider_pos = float(float(event->y())/float(this->height()));
    repaint();
}

void Axis::mouseMoveEvent(QMouseEvent *event)
{
    if (horizontal == true) slider_pos = float(float(event->x())/float(this->width()));
    else if (horizontal == false) slider_pos = float(float(event->y())/float(this->height()));
    repaint();
}

void Axis::mouseReleaseEvent(QMouseEvent *event)
{
    if (horizontal == true) slider_pos = float(float(event->x())/float(this->width()));
    else if (horizontal == false) slider_pos = float(float(event->y())/float(this->height()));
    emit newSliderPos(int(min + slider_pos*(max-min)));
    repaint();
}

void Axis::addTick(float frac, int name)
{
    labelNames.push_back(name);
    labelPositions.push_back(frac);

    repaint();
}

void Axis::resetTicks()
{
    labelNames.clear();
    labelPositions.clear();
    repaint();
}

void Axis::setColor(QColor c)
{

}
