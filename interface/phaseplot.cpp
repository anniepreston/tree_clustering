#include "phaseplot.h"
#include "mainwindow.h"
#include "dataManage.h"
#include <math.h>
#include <limits>

//NOTE: different numbers of trees plotted per cluster give misleading visual results -- choose top x trees per cluster, or random, but should
//be same per cluster

//OR, another option: heat map style -- areas are colored/shaded by relative density of nodes (halos) in that area on the plot

PhasePlot::PhasePlot(dataManage *dataManager, QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent), dataManager(dataManager)
{
}

void PhasePlot::initializeGL(){
    makeCurrent();

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    posVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    velVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    offsetVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    y_var = "velocity"; //velocity or position
    y_component = "z"; //x, y, z, or m (mag)

    posVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    velVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    offsetVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    relMassVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    timeVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    createPhasePlot();
    phaseProgram = GLShaderProgram::create(dataManager->path + "phase_shader.vert",
                                           dataManager->path + "phase_shader.frag");
    if (phaseProgram == NULL)
        close();

    setShader();
    paintGL();
}

void PhasePlot::resizeGL(int w, int h){
}

void PhasePlot::paintGL(){
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    phaseProgram->begin();
    phaseVAO->drawArrays(GL_TRIANGLE_STRIP, n);
    phaseProgram->end();
    doneCurrent();
}

void PhasePlot::refresh(){
    makeCurrent();
    initializeGL();
    update();
}

void PhasePlot::createPhasePlot(){
    //NOTE: could scale within individual trees, or scale everything to the same range.

    this->makeCurrent();
    std::vector<int> cluster_membership = dataManager->getClusterAssignments();
    std::vector<int> ts = dataManager->getTimesteps();
    ts.push_back(499); //FIXME
    std::vector<Vec3f> color_set = dataManager->color_set;

    std::vector<Vec3f> colors;
    std::vector<float> times;
    std::vector<Vec4f> pos_diffs;
    std::vector<Vec4f> vel_diffs;
    std::vector<Vec2f> offsets;
    std::vector<float> radii;

    auto halos = dataManager->getAllProgs();
    auto ids = getNIndicesPerCluster();

    int s = 0;
    if (halos.size() > 0) s = halos.size();
    for (int i = 0; i < s; i++){

        if (same_n_per_cluster == true){
            if (std::find(ids.begin(), ids.end(), i) == ids.end()){
                continue;
            }
        }

        //taking max mag. per halo:
        float min = std::numeric_limits<float>::min();
        float max = std::numeric_limits<float>::max();
        Vec4f max_pos_diff = Vec4f(min, min, min, min);
        Vec4f max_vel_diff = Vec4f(min, min, min, min);
        Vec4f min_vel_diff = Vec4f(max, max, max, max);
        std::vector<Vec4f> positions;
        std::vector<Vec4f> velocities;

        // (sanity check: should have exactly one halo for t=499 in each row)
        int id = cluster_membership[i];
        int t_final = ts[ts.size()-1];
        auto final = halos[i][0];
        auto ref_vel = final.xyzvel;
        auto ref_pos = final.xyzpos;
        float ref_mass = final.mass;

        float yo = float(id-1)/(dataManager->getClusterSizes().size());
        Vec4f y_off = Vec4f(yo, yo, yo, yo);

        for (int j = 0; j < halos[i].size(); j++){
            for (int k = 0; k < 4; k++){
                times.push_back(float(halos[i][j].time)/float(t_final));
                colors.push_back(color_set[id+3]);
                positions.push_back(calcDists(halos[i][j].xyzpos, ref_pos, max_pos_diff));
                velocities.push_back(calcVelDiffs(halos[i][j].xyzvel, ref_vel, max_vel_diff, min_vel_diff));
                radii.push_back(halos[i][j].mass/ref_mass); //FIXME: can do more sophisticated mass scaling
            }
            times.push_back(NAN);
            colors.push_back(Vec3f(NAN, NAN, NAN));
            positions.push_back(Vec4f(NAN, NAN, NAN, NAN));
            velocities.push_back(Vec4f(NAN, NAN, NAN, NAN));
            radii.push_back(NAN);
            offsets.push_back(Vec2f(-.1, -.1)); offsets.push_back(Vec2f(-.1, .1));
            offsets.push_back(Vec2f(.1, -.1)); offsets.push_back(Vec2f(.1, .1));
            offsets.push_back(Vec2f(NAN, NAN));
        }
        //scale positions and velocities
        for (auto &p : positions){
            p = Vec4f(p.x/(4*max_pos_diff.x) + 0.1,
                      p.y/(4*max_pos_diff.y) + 0.1,
                      p.z/(4*max_pos_diff.z) + 0.1,
                      p.w/(4*max_pos_diff.w) + 0.1);

            pos_diffs.push_back(p + y_off);
        }
        for (auto& v : velocities){
            float x, y, z, w;
            float x_max;
            max_vel_diff.x > -min_vel_diff.x ? x_max = max_vel_diff.x : x_max = -min_vel_diff.x;
            if (v.x > 0) x = 0.16*((v.x*0.5)/(x_max) + 0.5) + 0.005;
            else x = 0.16*((v.x+x_max)*0.5/(x_max)) + 0.005;

            float y_max;
            max_vel_diff.y > -min_vel_diff.y ? y_max = max_vel_diff.y : y_max = -min_vel_diff.y;
            if (v.y > 0) y = 0.16*((v.y*0.5)/(y_max) + 0.5) + 0.005;
            else y = 0.16*((v.y+y_max)*0.5/(y_max)) + 0.005;

            float z_max;
            max_vel_diff.z > -min_vel_diff.z ? z_max = max_vel_diff.z : z_max = -min_vel_diff.z;
            if (v.z > 0) z = 0.16*((v.z*0.5)/(z_max) + 0.5) + 0.005;
            else z = 0.16*((v.z+z_max)*0.5/(z_max)) + 0.005;

            float w_max;
            max_vel_diff.w > -min_vel_diff.w ? x_max = max_vel_diff.w : w_max = -min_vel_diff.w;
            if (v.w > 0) w = 0.16*((v.w*0.5)/(w_max) + 0.5) + 0.005;
            else w = 0.16*((v.w+w_max)*0.5/(w_max)) + 0.005;

            v = Vec4f(x, y, z, w);

            vel_diffs.push_back(v + y_off);
        }
    }

    n = vel_diffs.size();

    posVBO->update(pos_diffs.size()*sizeof(Vec4f), &pos_diffs.front(), GL_STATIC_DRAW);
    velVBO->update(vel_diffs.size()*sizeof(Vec4f), &vel_diffs.front(), GL_STATIC_DRAW);
    colorVBO->update(colors.size()*sizeof(Vec3f), &colors.front(), GL_STATIC_DRAW);
    offsetVBO->update(offsets.size()*sizeof(Vec2f), &offsets.front(), GL_STATIC_DRAW);
    relMassVBO->update(radii.size()*sizeof(GL_FLOAT), &radii.front(), GL_STATIC_DRAW);
    timeVBO->update(times.size()*sizeof(GL_FLOAT), &times.front(), GL_STATIC_DRAW);
    phaseVAO = GLVertexArray::create();
}

Vec4f PhasePlot::calcDists(Vec3f pos, Vec3f ref, Vec4f &max){
    float x = fabs(pos.x - ref.x); if (x > max.x) max.x = x;
    float y = fabs(pos.y - ref.y); if (y > max.y) max.y = y;
    float z = fabs(pos.z - ref.z); if (z > max.z) max.z = z;
    float w = sqrt(pow((pos.x - ref.x),2) + pow((pos.y - ref.y),2) + pow((pos.z - ref.z),2));
    if (w > max.w) max.w = w;
    return Vec4f(x, y, z, w);
}

Vec4f PhasePlot::calcVelDiffs(Vec3f vel, Vec3f ref, Vec4f &max, Vec4f &min){
    //really, we're comparing speeds, so take absolute value of the velocities

    float x = fabs(vel.x) - fabs(ref.x); if (x > max.x) max.x = x; if (x < min.x) min.x = x;
    float y = fabs(vel.y) - fabs(ref.y); if (y > max.y) max.y = y; if (y < min.y) min.y = y;
    float z = fabs(vel.z) - fabs(ref.z); if (z > max.z) max.z = z; if (z < min.z) min.z = z;
    float w = sqrt(vel.x*vel.x + vel.y*vel.y + vel.z*vel.z) - sqrt(ref.x*ref.x + ref.y*ref.y + ref.z*ref.z);
    if (w > max.w) max.w = w; if (w < min.w) min.w = w;

    return Vec4f(x, y, z, w);
}

void PhasePlot::setShader(){
    phaseProgram->setVertexAttribute("pos_diff", phaseVAO, posVBO, 4, GL_FLOAT, false);
    phaseProgram->setVertexAttribute("vel_diff", phaseVAO, velVBO, 4, GL_FLOAT, false);
    phaseProgram->setVertexAttribute("color", phaseVAO, colorVBO, 3, GL_FLOAT, false);
    phaseProgram->setVertexAttribute("offset", phaseVAO, offsetVBO, 2, GL_FLOAT, false);
    phaseProgram->setVertexAttribute("radius", phaseVAO, relMassVBO, 1, GL_FLOAT, false);
    phaseProgram->setVertexAttribute("time", phaseVAO, timeVBO, 1, GL_FLOAT, false);

    phaseProgram->setUniform("width", float(2322));
    phaseProgram->setUniform("height", float(1422));

    int pos_or_vel;
    y_var == "position" ? pos_or_vel = 1 : pos_or_vel = 0;
    int component;
    if (y_component == "x") component = 0;
    else if (y_component == "y") component = 1;
    else if (y_component == "z") component = 2;
    else component = 3;

    phaseProgram->setUniform("pos_or_vel", pos_or_vel);
    phaseProgram->setUniform("component", component);
}

std::vector<int> PhasePlot::getNIndicesPerCluster(){
    //find n of smallest cluster
    std::vector<int> returnIndices;

    auto cluster_sizes = dataManager->getClusterSizes();
    int min;
    if (cluster_sizes.size() > 0) min = *std::min_element(cluster_sizes.begin(), cluster_sizes.end());

    auto cluster_ids = dataManager->getClusterAssignments();

    std::cout << "smallest cluster size: " << min << std::endl;

    //for each cluster, get list of IDXs in this cluster
    for (int i = 1; i <= cluster_sizes.size(); i++){
        std::vector<int> indicesInCluster;
        for (int j = 0; j < cluster_ids.size(); j++){
            if (cluster_ids[j] == i){
                indicesInCluster.push_back(j);
            }
        }
        //std::iota(indices.begin(), indices.end(), 0);
        std::random_shuffle(indicesInCluster.begin(), indicesInCluster.end());
        std::vector<int> subsetIDs = std::vector<int>(indicesInCluster.begin(), indicesInCluster.begin()+min);
        returnIndices.insert(returnIndices.end(), subsetIDs.begin(), subsetIDs.end());
    }

    return returnIndices;
}
