#version 330
in vec3 out_color;
in float out_alpha;
out vec4 FragColor;

void main(void)
{
    float alpha = out_alpha;
    FragColor = vec4(out_color, alpha);
}

