#include "TreeAnimation.h"
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <limits>         // numeric limits
#include <math.h>

//FIXME: later, will use this to store all trees. currently, these are just trees within one mass bin.
//extern std::vector<std::vector<float>> trees;

static TreeAnimation* currentInstance;

extern "C"
void drawCallback()
{
    currentInstance->drawScene();
}

void TreeAnimation::setupDrawCallback()
{
    ::currentInstance = this;
    ::glutDisplayFunc(::drawCallback);
}

TreeAnimation::TreeAnimation(QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent)
{
}

void TreeAnimation::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    paintGL();
}

void TreeAnimation::initializeGL()
{
    makeCurrent();

    //general openGL settings here
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    posVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    treeVAO = GLVertexArray::create();

    loadTrees();

    initShaders();

    doneCurrent();
}

void TreeAnimation::begin()
{
    initializeGL();
/*
    char *myargv [1];
    int myargc=1;
    myargv [0]=strdup ("Myappname");
    glutInit(&myargc, myargv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(800, 800);
    glutCreateWindow("animation");
    setupDrawCallback();
    glutMainLoop( );
    */
    animateAddTrees();
}

void TreeAnimation::drawScene()
{
    animateAddTrees();
}

void TreeAnimation::loadTrees()
{
    min_mass = std::numeric_limits<float>::max();
    max_mass = std::numeric_limits<float>::min();

    allTrees.clear();
    std::string row, field;
    std::vector<int> line;
    std::vector<float> tree_row;
    //FIXME: load full dataset
    std::ifstream tree_file("/Users/annie/repos/tree_clustering/full_main_masses.csv");
    if (tree_file.is_open()){
        while (std::getline(tree_file, row))
        {
            tree_row.clear();
            line.clear();
            std::stringstream ss(row);
            while (std::getline(ss, field, ','))
            {
                float entry = std::stof(field);
                if (entry > 0) { tree_row.push_back(entry); }
                if (entry > 0 && entry > max_mass) { max_mass = entry; }
                if (entry > 0 && entry < min_mass) { min_mass = entry; }
            }
            std::reverse(tree_row.begin(), tree_row.end());
            allTrees.push_back(tree_row);
        }

        //take log of values
        for (auto &t: allTrees){ //for each tree
            for (auto &e : t){   //for each timestep
                e = log10(e);
            }
        }
        max_mass = log10(max_mass);
        min_mass = log10(min_mass);

        //scale y values to {-1, 1}
        for (auto &t : allTrees){     //for each tree
            for (auto &e : t){     //for each timestep
                e = 2*(e - min_mass)/(max_mass - min_mass) - 1;
            }
        }
    }
    else{
        std::cout << "couldn't open masses file " << std::endl;
    }
}

void TreeAnimation::initShaders()
{
    treeProgram = GLShaderProgram::create(path + "map.vert", path + "map.frag");
    if (treeProgram == NULL)
        close();
}

void TreeAnimation::setShaderAttributes()
{
    treeProgram->setVertexAttribute("pos", treeVAO, posVBO, 2, GL_FLOAT, false);
    treeProgram->setVertexAttribute("color", treeVAO, colorVBO, 4, GL_FLOAT, false);
}

void TreeAnimation::animateAddTrees()
{
    //FIXME: for now, assuming a max tree length of 100

    //for each subset of trees:
    std::vector<Vec2f> positions;
    std::vector<Vec4f> colors;

    //animated option
    /*
    for(int i = 0; i < allTrees.size(); i+=100000)
    {
        for (int k = i; k < i+100000; k++)
        {
            for (int j = 0; j < allTrees[k].size(); j++)
            {
                float x_pos = (2/100.)*(j+100-allTrees[k].size()) - 1;
                positions.push_back(Vec2f(x_pos, allTrees[k][j]));
                colors.push_back(Vec4f(0.0, 0.0, 0.0, 1.0));
            }
        }
        //emit treesDrawn(i);
        colors.push_back(Vec4f(0.0, 0.0, 0.0, 0.0));
        positions.push_back(Vec2f(NAN, NAN));

        posVBO->update(positions.size()*sizeof(Vec2f), &positions.front(), GL_STATIC_DRAW);
        colorVBO->update(colors.size()*sizeof(Vec4f), &colors.front(), GL_STATIC_DRAW);
        treeVAO = GLVertexArray::create();

        setShaderAttributes();
        tree_size = colors.size();
        paintGL();
        update();
    }
    */

    //static option (could do opacity fade-in?)
    for (int i = 0; i < allTrees.size(); i++)
    {
        for (int j = 0; j < allTrees[i].size(); j++)
        {
            float x_pos = (2/100.)*(j+100-allTrees[i].size()) - 1;
            positions.push_back(Vec2f(x_pos, allTrees[i][j]));
            colors.push_back(Vec4f(0.0, 0.0, 0.0, 1.0));
        }
        colors.push_back(Vec4f(0.0, 0.0, 0.0, 0.0));
        positions.push_back(Vec2f(NAN, NAN));
    }

    posVBO->update(positions.size()*sizeof(Vec2f), &positions.front(), GL_STATIC_DRAW);
    colorVBO->update(colors.size()*sizeof(Vec4f), &colors.front(), GL_STATIC_DRAW);
    treeVAO = GLVertexArray::create();

    setShaderAttributes();
    tree_size = colors.size();
    paintGL();
    update();

    //mass binning


    emit treesDrawn(allTrees.size());
    std::this_thread::sleep_for(std::chrono::seconds(3));
    emit doneDrawingTrees();
}

void TreeAnimation::binTrees()
{
    std::cout << "drawing tree bins" << std::endl;
    std::vector<Vec2f> positions;
    std::vector<Vec4f> colors;
    std::vector<float> bin_ends = {1.0, 0.6, 0.2, -0.2, -0.6, -1.0};

    for (int i = 0; i < allTrees.size(); i++)
    {
        float max_mass = allTrees[i][allTrees[i].size()-1];
        Vec4f color;
        for (int k = 0; k < 6; k++)
        {
            if(max_mass < bin_ends[k] && max_mass > bin_ends[k+1]) {
                color = Vec4f(color_set[k][0]/255., color_set[k][1]/255., color_set[k][2]/255., 1.0);
            }
        }
        for (int j = 0; j < allTrees[i].size(); j++)
        {
            float x_pos = (2/100.)*(j+100-allTrees[i].size()) - 1;
            positions.push_back(Vec2f(x_pos, allTrees[i][j]));
            colors.push_back(color);
        }
        colors.push_back(Vec4f(0.0, 0.0, 0.0, 0.0));
        positions.push_back(Vec2f(NAN, NAN));
    }

    posVBO->update(positions.size()*sizeof(Vec2f), &positions.front(), GL_STATIC_DRAW);
    colorVBO->update(colors.size()*sizeof(Vec4f), &colors.front(), GL_STATIC_DRAW);

    setShaderAttributes();
    tree_size = colors.size();

    paintGL();
    update();
}

void TreeAnimation::drawBins()
{
//FIXME: nothing here for now re: nodes
    //color trees by bin color
    std::this_thread::sleep_for(std::chrono::seconds(1));
    binTrees();
}

void TreeAnimation::paintGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    treeProgram->begin();
    treeVAO->drawArrays(GL_LINE_STRIP, tree_size);
    treeProgram->end();

    swapBuffers();

    doneCurrent();
}
