#include "scatterplot.h"
#include "mainwindow.h"
#include "dataManage.h"

ScatterPlot::ScatterPlot(dataManage *dataManager, QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent), dataManager(dataManager)
{
}

void ScatterPlot::initializeGL(){
    makeCurrent();
    glViewport(0, 0, 160, 160);

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    scatterVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    centerVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    y_var = "mass";
    x_var = "n_mergers";

    x_threshold = 0.1;
    y_threshold = dataManager->timesteps.size()-1;

    createPlot();
    scatterProgram = GLShaderProgram::create(dataManager->path + "scatter_shader.vert", dataManager->path + "scatter_shader.frag");
    if (scatterProgram == NULL)
        close();

    setShader();
    paintGL();
}

void ScatterPlot::scale(std::vector<float> &A){
    auto it = std::max_element(A.begin(), A.end());
    float max = A[std::distance(A.begin(), it)];
    std::cout << "scale max: " << max << std::endl;
    auto it_min = std::min_element(A.begin(), A.end());
    float min = A[std::distance(A.begin(), it_min)];
    std::cout << "scale min: " << min << std::endl;

    for (auto & a : A) a = float((a-min)/(max-min));
}

void ScatterPlot::createPlot(){
    this->makeCurrent();
    auto trees = dataManager->getTrees();
    auto series = dataManager->getSeries();

    std::vector<float> x;
    std::vector<float> y;

    if (dataManager->dataType == "cosmology"){
        int i = 0;
        for (auto & t : trees){
            if (x_var == "mass"){
                auto timesteps = dataManager->timesteps;
                int time = timesteps[int(x_threshold)];
                for (auto & h : t) {
                    if (h.time == time) x.push_back(h.mass);
                }
            }
            else if (x_var =="n_mergers"){
                int temp = 0;
                for (auto & h : t) if (h.merger_ratio > x_threshold/100.) temp++;
                x.push_back(float(temp));
            }
            else if (x_var == "velocity"){
                auto timesteps = dataManager->timesteps;
                int time = timesteps[int(x_threshold)];
                for (auto & h : t) {
                    if (h.time == time) x.push_back(h.velocity);
                }
            }
            else if (x_var == "property"){
                float mass = 1.0;
                for (auto & h : t) {
                    if (h.time == 63) mass = h.mass;
                }
                auto properties = dataManager->getProperties();
                x.push_back(properties[i][int(x_threshold)]/mass);
            }
            i++;
        }
        i = 0;
        for (auto & t : trees){
            if (y_var == "mass"){
                int time = dataManager->timesteps[floor(y_threshold)];
                for (auto & h : t) if (h.time == time) y.push_back(h.mass);
            }
            else if (y_var == "n_mergers"){
                int temp = 0;
                for (auto & h : t) if (h.merger_ratio > y_threshold/100.) temp++;
                y.push_back(float(temp));
            }
            else if (y_var == "velocity"){
                int time = dataManager->timesteps[floor(y_threshold)];
                for (auto & h : t) if (h.time == time) y.push_back(h.velocity);
            }
            else if (y_var == "property"){
                float mass = 1.0;
                for (auto & h : t) {
                    if (h.time == 63) mass = h.mass;
                }
                auto properties = dataManager->getProperties();
                y.push_back(properties[i][int(y_threshold)]/mass);
            }
            i ++;
        }
    }
    else if (dataManager->dataType == "climate"){
        for (auto & t : series){
            y.push_back(t[0].lat);
            x.push_back(t[0].lon);
        }
    }

    if (x.size() > 0 && y.size() > 0){
        emit this->updateXAxis(x[std::distance(x.begin(), std::min_element(x.begin(), x.end()))],
                     x[std::distance(x.begin(), std::max_element(x.begin(), x.end()))]);
        emit this->updateYAxis(y[std::distance(y.begin(), std::min_element(y.begin(), y.end()))],
                     y[std::distance(y.begin(), std::max_element(y.begin(), y.end()))]);
        emit this->updateXLabel(QString::fromStdString(x_var));
        emit this->updateYLabel(QString::fromStdString(y_var));

        scale(x);
        scale(y);
    }

    std::vector<int> cluster_membership = dataManager->getClusterAssignments();

    std::vector<Vec2f> posArray;
    std::vector<Vec2f> centerArray;

    float delta = 0.01;
    for (int i = 0; i < x.size(); i++){
        posArray.push_back(Vec2f(x[i]-delta, y[i]-delta));
        posArray.push_back(Vec2f(x[i]-delta, y[i]+delta));
        posArray.push_back(Vec2f(x[i]+delta, y[i]-delta));
        posArray.push_back(Vec2f(x[i]+delta, y[i]+delta));
        posArray.push_back(Vec2f(NAN, NAN));
        for (int j = 0; j < 5; j++) centerArray.push_back(Vec2f(x[i], y[i]));
    }

    std::vector<Vec3f> colorArray;
    std::vector<Vec3f> colors = dataManager->color_set;

    for (auto & i : cluster_membership){
        for (int j = 0; j < 4; j++){
            colorArray.push_back(colors[i]);
        }
        colorArray.push_back(Vec3f(NAN));
    }

    for (int i = 0; i < x.size(); i++){
        colorArray.push_back(Vec3f(1.0, 0.0, 0.5));
        colorArray.push_back(Vec3f(1.0, 0.0, 0.5));
        colorArray.push_back(Vec3f(1.0, 0.0, 0.5));
        colorArray.push_back(Vec3f(1.0, 0.0, 0.5));
        colorArray.push_back(Vec3f(NAN, NAN, NAN));
    }

    scatterVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_STATIC_DRAW);
    colorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_STATIC_DRAW);
    centerVBO->update(centerArray.size()*sizeof(Vec2f), &centerArray.front(), GL_STATIC_DRAW);
    scatterVAO = GLVertexArray::create();
}

void ScatterPlot::setShader(){
    scatterProgram->setVertexAttribute("position", scatterVAO, scatterVBO, 2, GL_FLOAT, false);
    scatterProgram->setVertexAttribute("color", scatterVAO, colorVBO, 3, GL_FLOAT, false);
    scatterProgram->setVertexAttribute("center", scatterVAO, centerVBO, 2, GL_FLOAT, false);

    scatterProgram->setUniform("width", float(2322));
    scatterProgram->setUniform("height", float(1422));
}

void ScatterPlot::paintGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    scatterProgram->begin();
    int num;
    if (dataManager->dataType == "climate") num = 5*dataManager->getSeries().size();
    else if (dataManager->dataType == "cosmology") num = 5*dataManager->getTrees().size();
    else num = 0;
    scatterVAO->drawArrays(GL_TRIANGLE_STRIP, num);
    scatterProgram->end();
    doneCurrent();
}

void ScatterPlot::resizeGL(int w, int h)
{
}

void ScatterPlot::setXAxis(std::string value, float t)
{
    this->makeCurrent();

    x_var = value;
    x_threshold = t;
    createPlot();
    setShader();
    paintGL();
    update();
}

void ScatterPlot::setYAxis(std::string value, float t)
{
    this->makeCurrent();
    y_var = value;
    y_threshold = t;
    createPlot();
    setShader();
    paintGL();
    update();
}

void ScatterPlot::refresh(){
    std::cout << "scatterplot: refresh" << std::endl;
    makeCurrent();
    std::cout << "scatter: initializing gl" << std::endl;
    initializeGL();
    std::cout << "scatter: update" << std::endl;
    update();
    std::cout << "scatter: done with refresh" << std::endl;
}
