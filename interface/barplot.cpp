#include "barplot.h"
#include <vector>

barPlot::barPlot(dataManage *dataManager, QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent), dataManager(dataManager)
{
}

void barPlot::initializeGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    barVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    auto cluster_sizes = dataManager->getClusterSizes();
    createBars(cluster_sizes);

    barShader = GLShaderProgram::create(dataManager->path + "bar_shader.vert", dataManager->path + "bar_shader.frag");
    setShaders();

    paintGL();
    doneCurrent();
}

void barPlot::createBars(std::vector<int> cs){
    std::vector<Vec2f> posArray;
    std::vector<Vec3f> colorArray;

    float feather = 1./400.;

    int max = 0;
    for (auto & c : cs) max += c;
    for (int i = 0; i < cs.size(); i++){
        float ymin = float(i)/float(cs.size())+feather;
        float ymax = float(i+1)/float(cs.size())-feather;
        float xmin = 1.-float(cs[i])/float(max)+feather;
        float xmax = 1.-feather;
        posArray.push_back(Vec2f(xmin, ymin)); posArray.push_back(Vec2f(xmax, ymin));
        posArray.push_back(Vec2f(xmin, ymax)); posArray.push_back(Vec2f(xmax, ymax));
        for (int j = 0; j < 4; j++) {
            colorArray.push_back(dataManager->color_set[i]);
        }

        colorArray.push_back(Vec3f(NAN, NAN, NAN));
        posArray.push_back(Vec2f(NAN, NAN));
    }
    barVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_STATIC_DRAW);
    colorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_STATIC_DRAW);
    barVAO = GLVertexArray::create();
}

void barPlot::setShaders()
{
    barShader->setVertexAttribute("in_position", barVAO, barVBO, 2, GL_FLOAT, false);
    barShader->setVertexAttribute("in_color", barVAO, colorVBO, 3, GL_FLOAT, false);
}

void barPlot::paintGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    barShader->begin();
    barVAO->drawArrays(GL_TRIANGLE_STRIP, 5*dataManager->getClusterSizes().size());
    barShader->end();
}

void barPlot::resizeGL(int w, int h)
{
    makeCurrent();
    glViewport(0, 0, w, h);
    paintGL();
    doneCurrent();
}

void barPlot::refresh()
{
    makeCurrent();
    initializeGL();
    update();
}
