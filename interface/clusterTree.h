#ifndef CLUSTERTREE_H
#define CLUSTERTREE_H
#include <vector>

class clusterTree
{
public:
    struct node
    {
        int data;
        node *left = NULL;
        node *right = NULL;
        bool flag = false;  //node is "off" by default
    };

    clusterTree();

    void                insert(std::vector<int> data, int parent, int level);
    std::vector<node *> get_n_sub_nodes(node *node, int n);
    std::vector<node *> get_nodes_at_level(node *node, int level);
    void                descendant_indices(clusterTree::node *node, std::vector<int>& index_list);
    std::vector<int>    descendant_indices_n_clusters(clusterTree::node *node, int n, int dataSize);
    std::vector<int>    get_clusters(clusterTree::node *node, std::vector<std::vector<int> > linkage, int level);
    clusterTree::node   get_query_node(int n, int index); // n = number of clusters previously requested; index = id of node selected
    void                turn_on_descendants(clusterTree::node *node);

    node *root;

    //needed for seriation:
    void                initial_layout();
    std::vector<int>    get_permutation(node *root_node);
    void                traverse(node *node, std::vector<int> &leaves);
    std::vector<int>    seriate();

private:
    void fillVBOs();
};

#endif // CLUSTERS_H
