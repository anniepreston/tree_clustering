#version 330
in vec2 position;
in vec3 color;
in vec2 center;

out vec3 out_color;
out vec2 out_center;

uniform float width;
uniform float height;

out float out_width;
out float out_height;

void main(void)
{
    out_width = width; out_height = height;
    out_color = color;
    out_center = center;
    gl_Position = vec4(2*position.x-1., 2*position.y-1., 0, 1);
}
