#version 330
in vec2 pos;
in vec2 center;
in vec3 color;
in float ratio;

out vec4 out_color;
out vec4 out_pos;
out vec2 out_center;
out float out_ratio;

void main(void)
{
    out_ratio = ratio;
    out_color = vec4(color.x, color.y, color.z, 0.7);
    out_pos = vec4(pos.x, pos.y, 0.0, 1.0);
    out_center = center;
    gl_Position = out_pos;
}
