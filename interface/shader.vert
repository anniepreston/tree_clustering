#version 330
in vec2 in_position;
in vec3 color;
in float y_offset;
in float alpha;

uniform vec2  y_range;
uniform float scale;

out vec3 out_color;
out float out_alpha;

void main(void)
{
    //FIXME!!!!!
    float x;
    float y;
    float delta_y;
    if (scale != 0){
        y = 2.0*((y_range.y - y_offset) -y_range.x)/(y_range.y-y_range.x) - 1.0;
        float test_scale = 1.0/(y_range.y);
        delta_y = test_scale*(0.9*in_position.y + 1.0);
        x = in_position.x;
    }
    else{
        x = in_position.x;
        y = in_position.y + y_offset;
        delta_y = 0;
    }

    out_color = color;
    out_alpha = alpha;
    gl_Position = vec4(x, y + delta_y, 0.0, 1.0);
}
