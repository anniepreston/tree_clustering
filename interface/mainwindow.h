#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QGLWidget>
#include <QMainWindow>
#include "dataManage.h"
#include "TreeAnimation.h"
#include "Map.h"
#include "heatmap.h"
#include "scatterplot.h"
#include "clusters.h"
#include "barplot.h"
#include "pieplot.h"
#include "phaseplot.h"

extern int n_clusters;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    friend class dataManage;
    friend class Map;
    friend class ScatterPlot;
    friend class PhasePlot;

    dataManage *dataManager;
    barPlot *bars;
    piePlot *pies;
    Map *map;
    Heatmap *heatmap;
    TreeAnimation *treeAnimation;
    QLabel* labels[4];
    QMenu* loadMenu;
    QMenu* dataMenu;
    QMenu* clusterMenu;
    QMenu* groupBy;
    ScatterPlot *scatterPlot;
    PhasePlot *phasePlot;
    Clusters *clusters;
    QVBoxLayout *verticalAxes;

public slots:
    void addAxes();
    void startAnimation();
    void showClusters(bool t);
    void showScatter(bool t);
    void showPhase(bool t);
    void showWelcome();
    void setAxisRange(bool horiz, float min, float max);

    void labelTreesDrawn(int n);

    void showList(bool t);
    void updateList(std::vector<unsigned int> tags);
    void showHalos(std::vector<unsigned int> tags, std::vector<int> times);
    void highlightTrees();
    void createMenuBar();

    void setXMass(bool);
    void updateXMass(int);
    void setXMassLabel(int);
    void setXVel(bool);
    void updateXVel(int);
    void setXVelLabel(int);
    void setXMerger(bool);
    void updateXMerger(int);
    void setXMergerLabel(int);

    void setYMass(bool);
    void updateYMass(int);
    void setYMassLabel(int);
    void setYVel(bool);
    void updateYVel(int);
    void setYVelLabel(int);
    void setYMerger(bool);
    void updateYMerger(int);
    void setYMergerLabel(int);

    void setXProperty(bool);
    void setYProperty(bool);

    void setResolution(QAction *);
    void setColor(QAction *);
    void toggleFullTrees(bool);

    void initClimate(bool);
    void renameWindow(QAction*);

signals:
    void highlightTrees(int idx);
    void showRandomTrees(int n);
    void setScatterX(std::string label, float t);
    void setScatterY(std::string label, float t);
    void refresh();
    void updateResolution(int i);

public slots:
    void pathAddSimilarity(QAction *);
    void pathAddNorm(bool t);
    void pathAddLinkage(QAction *);
    void pathAddBin(QAction *);
    void pathAddSet(QAction *);

    //climate data:
    void usgs_pathAddStartTime(QAction *);
    void usgs_pathAddLinkage(QAction *);

private:
    void resize();
    void setPathDefaults();
    void setFileNames(std::string p);

    Ui::MainWindow *ui;
    void setAxes();
    void setScatterControls();
    void updateScatterControls();
    QMenu *colorMenu;
    QAction *color_ratio;
    QAction *color_value;
    QAction *color_category;

    QAction *cosmologyData;
    QAction *climateData;

    QMenu *plotMenu;
    QAction *plot_mass;
    QAction *plot_vel;

    QMenu *xMenu;
    QMenu *yMenu;
    QActionGroup *yActionGroup;
    QActionGroup *xActionGroup;

    QLabel *x_mass_label;
    QLabel *x_vel_label;
    QLabel *x_merger_label;
    QSlider *x_mass_slider;
    QRadioButton *x_mass_button;
    QRadioButton *x_vel_button;
    QRadioButton *x_merger_button;
    QSlider *x_vel_slider;
    QSlider *x_merger_slider;
    QButtonGroup *xgroup;

    QLabel *y_mass_label;
    QLabel *y_vel_label;
    QLabel *y_merger_label;
    QSlider *y_mass_slider;
    QRadioButton *y_mass_button;
    QRadioButton *y_vel_button;
    QRadioButton *y_merger_button;
    QSlider *y_vel_slider;
    QSlider *y_merger_slider;
    QButtonGroup *ygroup;

    QRadioButton *repTrees;
    QRadioButton *randomTrees;
    QSlider *random_slider;
    QSlider *representative_slider;

    QCheckBox *fullTreesCheckBox;

    QMenu *similarityMenu;
    QMenu *normMenu;
    QMenu *linkageMenu;
    QMenu *binMenu;
    QMenu *setMenu;

    QAction *mass; QAction *velocity;
    QAction *normed; QAction *unnormed;
    QAction *single; QAction *complete; QAction *average;
    QAction *A; QAction *B;
    QAction *binD; QAction *binE;
    QAction *t0; QAction *t744;

    //file(s) path
    std::map<std::string, std::string> path_components;
    std::map<std::string, std::string> usgs_path_components;
    std::string constructPath(std::map<std::string, std::string>);
    std::string constructUSGSPath(std::map<std::string, std::string>, bool);

    void updateProperties();
};

#endif // MAINWINDOW_H
