#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "clusterTree.h"
#include "Map.h"
#include <QDebug>
#include <QDir>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qApp->setStyleSheet("QToolBar { color: red } "); //FIXME: not working

    //set up file loading
    path_components.insert(std::make_pair("base", "/Users/annie/repos/tree_clustering/interface/data/"));
    path_components.insert(std::make_pair("similarity", ""));
    path_components.insert(std::make_pair("norm", ""));
    path_components.insert(std::make_pair("linkage", ""));
    path_components.insert(std::make_pair("bin", ""));
    path_components.insert(std::make_pair("set", ""));

    usgs_path_components.insert(std::make_pair("base", "/Users/annie/repos/tree_clustering/interface/climate_data/"));
    usgs_path_components.insert(std::make_pair("time", ""));
    usgs_path_components.insert(std::make_pair("linkage", ""));

    //initialize widgets
    dataManager = new dataManage;
    dataManager->init();
    map = new Map;
    heatmap = new Heatmap;

    //bars = new barPlot(dataManager);
    pies = new piePlot(dataManager);
    clusters = new Clusters(dataManager);
    scatterPlot = new ScatterPlot(dataManager);
    phasePlot = new PhasePlot(dataManager);

    createMenuBar();
    setAxes();
    setScatterControls();
    setPathDefaults();
    dataManager->updateResolution(4); //default

    //ui->clusterImageLayout->addWidget(bars);
    ui->clusterImageLayout->addWidget(pies);
    ui->clusterImageLayout->addWidget(clusters);
    ui->clusterImageLayout->setStretch(0, 1);
    ui->clusterImageLayout->setStretch(1, 10);

    ui->tabWidget->setCurrentIndex(1);
    ui->scatterLayout->addWidget(scatterPlot);
    ui->phaseLayout->addWidget(phasePlot);

    connect(clusters, SIGNAL(addAxes()), this, SLOT(addAxes()));

    clusters->setColorScheme("category");

    resize();

    QToolBar *clusterTools = new QToolBar;
    ui->clusterPrefLayout->addWidget(clusterTools);

    plotMenu = new QMenu("plot");
    plotMenu->setContextMenuPolicy(Qt::DefaultContextMenu);
    plot_mass = plotMenu->addAction("plot by mass");
    plot_vel = plotMenu->addAction("plot by velocity");
    clusterTools->addWidget(plotMenu);

    QRadioButton *braidButton = new QRadioButton;
    ui->clusterPrefLayout->addWidget(braidButton);
    braidButton->setText("clusters: braid");
    connect(braidButton, SIGNAL(clicked(bool)), clusters, SLOT(showBraid(bool)));

    QRadioButton *stackButton = new QRadioButton;
    ui->clusterPrefLayout->addWidget(stackButton);
    stackButton->setText("clusters: stack");
    connect(stackButton, SIGNAL(clicked(bool)), clusters, SLOT(showClusters(bool)));

    QRadioButton *gridButton = new QRadioButton;
    ui->clusterPrefLayout->addWidget(gridButton);
    gridButton->setText("trees: grid");
    connect(gridButton, SIGNAL(clicked(bool)), clusters, SLOT(showTreesInGrid(bool)));

    QButtonGroup *layoutGroup = new QButtonGroup;
    layoutGroup->addButton(braidButton);
    layoutGroup->addButton(stackButton);

    QRadioButton *allTrees = new QRadioButton;
    allTrees->setText("all trees");
    ui->clusterPrefLayout->addWidget(allTrees);
    connect(allTrees, SIGNAL(clicked(bool)), clusters, SLOT(showAllData(bool)));

    randomTrees = new QRadioButton;
    randomTrees->setText("random trees");
    ui->clusterPrefLayout->addWidget(randomTrees);
    connect(randomTrees, SIGNAL(pressed()), this, SLOT(highlightTrees()));
    connect(this, SIGNAL(showRandomTrees(int)), clusters, SLOT(showRandomTrees(int)));

    random_slider = new QSlider;
    random_slider->setRange(1, 20);
    random_slider->setOrientation(Qt::Horizontal);
    ui->clusterPrefLayout->addWidget(random_slider);
    connect(random_slider, SIGNAL(sliderPressed()), this, SLOT(highlightTrees()));

    repTrees = new QRadioButton;
    repTrees->setText("representative trees");
    ui->clusterPrefLayout->addWidget(repTrees);
    connect(repTrees, SIGNAL(pressed()), this, SLOT(showRandomTrees(int)));//FIXME: this doesn't do anything.

    representative_slider = new QSlider;
    representative_slider->setRange(1, 20);
    representative_slider->setOrientation(Qt::Horizontal);
    connect(representative_slider, SIGNAL(sliderPressed()), this, SLOT(highlightTrees()));
    ui->clusterPrefLayout->addWidget(representative_slider);

    fullTreesCheckBox = new QCheckBox;
    fullTreesCheckBox->setText("show full trees");
    ui->clusterPrefLayout->addWidget(fullTreesCheckBox);
    connect(fullTreesCheckBox, SIGNAL(toggled(bool)), this, SLOT(toggleFullTrees(bool)));
    //connect()

    /*
    QIcon randomIcon = QIcon(":/icon_cluster.png");
    ui->randomButton->setIcon(randomIcon);

    ui->showClustersButton->setAutoExclusive(false);
    ui->allTreesButton->setAutoExclusive(false);

    connect(ui->allTreesButton, SIGNAL(clicked(bool)), clusters, SLOT(showAllData(bool)));
    connect(ui->showClustersButton, SIGNAL(clicked(bool)), clusters, SLOT(showClusters(bool)));
    connect(ui->gridButton, SIGNAL(pressed()), clusters, SLOT(showTreesInGrid()));
    */

    connect(this, SIGNAL(setScatterX(std::string,float)), scatterPlot, SLOT(setXAxis(std::string,float)));
    connect(this, SIGNAL(setScatterY(std::string,float)), scatterPlot, SLOT(setYAxis(std::string,float)));

    connect(scatterPlot, SIGNAL(updateXAxis(float,float)), ui->scatterXAxis, SLOT(setRange(float,float)));
    connect(scatterPlot, SIGNAL(updateYAxis(float,float)), ui->scatterYAxis, SLOT(setRange(float,float)));
    connect(scatterPlot, SIGNAL(updateXLabel(QString)), ui->scatterXAxis, SLOT(setLabel(QString)));
    connect(scatterPlot, SIGNAL(updateYLabel(QString)), ui->scatterYAxis, SLOT(setLabel(QString)));

    connect(this, SIGNAL(refresh()), clusters, SLOT(refresh()));
    connect(this, SIGNAL(refresh()), scatterPlot, SLOT(refresh()));
    connect(this, SIGNAL(refresh()), phasePlot, SLOT(refresh()));
    //connect(this, SIGNAL(refresh()), bars, SLOT(refresh()));
    connect(this, SIGNAL(refresh()), pies, SLOT(refresh()));

    clusters->full_trees = fullTreesCheckBox->isChecked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resize(){
}

void MainWindow::showWelcome()
{
    ui->tabWidget->setCurrentIndex(0);
}

void MainWindow::startAnimation()
{
    ui->tabWidget->setCurrentIndex(2);
    treeAnimation->begin();
}

void MainWindow::showClusters(bool t)
{
    ui->tabWidget->setCurrentIndex(1);
}


void MainWindow::addAxes()
{
    while (!ui->clusterAxisLayout->isEmpty()){
        QWidget *w = ui->clusterAxisLayout->takeAt(0)->widget();
        delete w;
    }
    while (!ui->verticalAxisLayout->isEmpty()){
        QWidget *w = ui->verticalAxisLayout->takeAt(0)->widget();
        delete w;
    }
    //y
    int num = dataManager->getClusterSizes().size();
    if (num > 0){
        if (clusters->braid == false){
            Axis* axis[num];
            for (int i = 0; i < num; i++)
            {
                axis[i] = new Axis;
                axis[i]->setHorizontal(false);
                axis[i]->setRight(true);
                axis[i]->setRange(0, 1);
                axis[i]->setTickCount(2);
                ui->verticalAxisLayout->addWidget(axis[i]);
            }
            axis[0]->setLabel("mass");
        }
        else if (clusters->braid == true){
            Axis* yaxis = new Axis;
            yaxis->setHorizontal(false);
            yaxis->setRange(0, 1);
            ui->verticalAxisLayout->addWidget(yaxis);
            yaxis->setLabel("mass");
        }
        QLabel* label = new QLabel;
        label->setText("relative cluster size");
        ui->clusterAxisLayout->addWidget(label);

        Axis* xaxis = new Axis;
        xaxis->setHorizontal(true);
        xaxis->setRange(dataManager->timesteps[0], dataManager->timesteps[dataManager->timesteps.size()-1]);
        xaxis->setGeometry(100, 0, 1000, 40);
        ui->clusterAxisLayout->addWidget(xaxis);
        xaxis->setLabel("timestep");

        ui->clusterAxisLayout->setStretch(0, 1);
        ui->clusterAxisLayout->setStretch(1, 10);
    }
}

void MainWindow::labelTreesDrawn(int n){
    QString text = QString::number(n) + " merger trees";
}

void MainWindow::setAxes()
{
    ui->scatterXAxis->setHorizontal(true);
    ui->scatterYAxis->setHorizontal(false);
}

void MainWindow::setAxisRange(bool horiz, float min, float max)
{
    //scale to fit data range within window (as set in group_shader.vert)
    float scale_factor = 0.7;
    float avg = (max - min)/2.0;
    min = avg - (1./scale_factor)*(avg-min);
    max = avg + (1./scale_factor)*(max-avg);

    //if (horiz == true) ui->xAxis->setRange(min, max);
    //else ui->yAxis->setRange(min, max);
}

void MainWindow::showList(bool t)
{
    /*
    if (t == true) ui->stackedWidget_2->setCurrentIndex(1);
    else ui->stackedWidget_2->setCurrentIndex(0);
    */
}

void MainWindow::updateList(std::vector<unsigned int> tags)
{
    /*
    ui->listWidget->clear();
    for (auto &t : tags){
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::number(t));
        ui->listWidget->addItem(item);
    }
    */
}

void MainWindow::showScatter(bool t)
{
    ui->tabWidget->setCurrentIndex(3);
}

void MainWindow::showPhase(bool t)
{
    ui->tabWidget->setCurrentIndex(2);
}

void MainWindow::showHalos(std::vector<unsigned int> tags, std::vector<int> times)
{
    //insert a new widget in vertical layout above tree rendering (future: NEW CLASS: haloView)
    /*
    if (ui->mainLayout->indexOf(clusters) == 0){
        QWidget *halos = new QWidget();
        QHBoxLayout *haloLayout = new QHBoxLayout();
        for (int i = 0; i < 4; i++){
            labels[i] = new QLabel();
            labels[i]->setText("ID: " + QString::number(tags[i]) + ", Time: " + QString::number(times[i]));
            haloLayout->addWidget(labels[i]);
        }
        halos->setLayout(haloLayout);
        ui->mainLayout->insertWidget(0, halos);
    }
    else {
        for (int i = 0; i < 4; i++){
            labels[i]->setText("ID: " + QString::number(tags[i]) + ", Time: " + QString::number(times[i]));
        }
    }
    */
}

void MainWindow::highlightTrees()
{
    if (sender() == representative_slider) clusters->random = false;
    if (sender() == random_slider) clusters->random = true;

    if (randomTrees->isChecked()){
        int n_random = random_slider->value(); //FIXME
        randomTrees->setText("random trees: " + QString::number(n_random));
        clusters->random = true;
        emit showRandomTrees(n_random);
    }
    if (repTrees->isChecked()){
        int n = representative_slider->value();
        repTrees->setText("representative trees: " + QString::number(n));
        clusters->random = false;
        emit showRandomTrees(n);
    }
}

void MainWindow::createMenuBar()
{
    dataMenu = new QMenu("Data");
    cosmologyData = new QAction("Cosmology", this);
    climateData = new QAction("Climate", this);
    cosmologyData->setText("cosmology"); climateData->setText("climate");
    dataMenu->addAction(cosmologyData);
    dataMenu->addAction(climateData);
    cosmologyData->setCheckable(true); climateData->setCheckable(true);
    cosmologyData->setChecked(true);
    connect(dataMenu, SIGNAL(triggered(QAction*)), this, SLOT(renameWindow(QAction*)));

    dataMenu->addSeparator();

    QMenu *massBin = new QMenu("Mass Bin");
    QMenu *startDate = new QMenu("Start Date");
    massBin->setEnabled(false);
    startDate->setEnabled(false);
    t0 = new QAction("Jan. 16, 1968", this);
    t744 = new QAction("Jan. 16, 2040", this);
    startDate->addAction(t0); startDate->addAction(t744);
    QActionGroup *timeGroup = new QActionGroup(this);
    timeGroup->addAction(t0); timeGroup->addAction(t744);
    t0->setCheckable(true); t744->setCheckable(true);
    t0->setChecked(true);
    connect(startDate, SIGNAL(triggered(QAction*)), this, SLOT(usgs_pathAddStartTime(QAction*)));

    binD = new QAction("10^13 - 10^14 MSun", this);
    binE = new QAction("10^14 - 10^15 MSun", this);
    QActionGroup *binGroup = new QActionGroup(this);
    massBin->addAction(binD);
    massBin->addAction(binE);
    binD->setCheckable(true); binE->setCheckable(true);
    connect(massBin, SIGNAL(triggered(QAction*)), this, SLOT(pathAddBin(QAction*)));
    binGroup->addAction(binD); binGroup->addAction(binE);
    binE->setChecked(true);

    dataMenu->addMenu(massBin);
    dataMenu->addMenu(startDate);

    connect(cosmologyData, SIGNAL(toggled(bool)), massBin, SLOT(setEnabled(bool)));
    connect(climateData, SIGNAL(toggled(bool)), startDate, SLOT(setEnabled(bool)));

    QActionGroup *dataSetActions = new QActionGroup(this);
    dataSetActions->addAction(cosmologyData);
    dataSetActions->addAction(climateData);

    ui->menuBar->addMenu(dataMenu);
    connect(dataMenu, SIGNAL(triggered(QAction*)), dataManager, SLOT(setDataType(QAction*)));
    connect(climateData, SIGNAL(triggered(bool)), this, SLOT(initClimate(bool)));

    dataMenu->addSeparator();
    QMenu *dataSetMenu = new QMenu("Dataset");
    dataSetMenu->setEnabled(false);

    A = new QAction("Set 0", this);
    B = new QAction("Set 1", this);
    dataSetMenu->addAction(A); dataSetMenu->addAction(B);
    QActionGroup *setGroup = new QActionGroup(this);
    setGroup->addAction(A); setGroup->addAction(B);
    A->setCheckable(true); B->setCheckable(true);
    A->setChecked(true);
    connect(dataSetMenu, SIGNAL(triggered(QAction*)), this, SLOT(pathAddSet(QAction*)));

    dataMenu->addMenu(dataSetMenu);

    connect(cosmologyData, SIGNAL(toggled(bool)), dataSetMenu, SLOT(setEnabled(bool)));

    clusterMenu = new QMenu("Clusters");
    QMenu *number = new QMenu("Number");

    //FIXME: better interface for this?
    QAction *two = new QAction("2", this);
    QAction *three = new QAction("3", this);
    QAction *four = new QAction("4", this);
    QAction *five = new QAction("5", this);
    QAction *six = new QAction("6", this);
    QAction *seven = new QAction("7", this);
    QAction *eight = new QAction("8", this);

    two->setCheckable(true); three->setCheckable(true);
    four->setCheckable(true); five->setCheckable(true);
    six->setCheckable(true); seven->setCheckable(true);
    eight->setCheckable(true);
    four->setChecked(true);

    QActionGroup *numbers = new QActionGroup(this);
    numbers->addAction(two); numbers->addAction(three);
    numbers->addAction(four); numbers->addAction(five);
    numbers->addAction(six); numbers->addAction(seven);
    numbers->addAction(eight);

    number->addAction(two); number->addAction(three);
    number->addAction(four); number->addAction(five);
    number->addAction(six); number->addAction(seven);
    number->addAction(eight);

    two->setCheckable(true); three->setCheckable(true);
    four->setCheckable(true); five->setCheckable(true);
    six->setCheckable(true); seven->setCheckable(true);
    eight->setCheckable(true);

    connect(number, SIGNAL(triggered(QAction*)), this, SLOT(setResolution(QAction*)));

    ui->menuBar->addMenu(clusterMenu);
    clusterMenu->addMenu(number);

    clusterMenu->addSeparator();
    groupBy = new QMenu("Group By");
    mass = new QAction("Mass", this);
    velocity = new QAction("Velocity", this);
    QActionGroup *groupGroup = new QActionGroup(this);
    groupGroup->addAction(mass);
    groupGroup->addAction(velocity);
    mass->setCheckable(true); velocity->setCheckable(true);
    groupBy->addAction(mass); groupBy->addAction(velocity);
    mass->setChecked(true);
    connect(groupBy, SIGNAL(triggered(QAction*)), this, SLOT(pathAddSimilarity(QAction*)));
    clusterMenu->addMenu(groupBy);

    QMenu *linkageMenu = new QMenu("Linkage");
    complete = new QAction("Complete", this);
    average = new QAction("Average", this);
    single = new QAction("Single", this);
    linkageMenu->addAction(complete);
    linkageMenu->addAction(average);
    linkageMenu->addAction(single);
    complete->setCheckable(true); average->setCheckable(true);
    single->setCheckable(true); complete->setChecked(true);
    QActionGroup *linkageGroup = new QActionGroup(this);
    linkageGroup->addAction(complete);
    linkageGroup->addAction(single);
    linkageGroup->addAction(average);
    connect(linkageMenu, SIGNAL(triggered(QAction*)), this, SLOT(pathAddLinkage(QAction*)));
    connect(linkageMenu, SIGNAL(triggered(QAction*)), this, SLOT(usgs_pathAddLinkage(QAction*)));
    clusterMenu->addMenu(linkageMenu);

    normed = new QAction("Normalized", this);
    normed->setCheckable(true);
    normed->setChecked(true);
    clusterMenu->addAction(normed);
    connect(normed, SIGNAL(triggered(bool)), this, SLOT(pathAddNorm(bool)));

    clusterMenu->addSeparator();

    colorMenu = new QMenu("Color By");
    color_ratio = new QAction("Ratio", this);
    color_value = new QAction("Value", this);
    color_category = new QAction("Category", this);
    colorMenu->addAction(color_ratio);
    colorMenu->addAction(color_value);
    colorMenu->addAction(color_category);

    QActionGroup *colorGroup = new QActionGroup(this);
    colorGroup->addAction(color_ratio);
    colorGroup->addAction(color_value);
    colorGroup->addAction(color_category);
    color_ratio->setCheckable(true);
    color_value->setCheckable(true);
    color_category->setCheckable(true);
    color_category->setChecked(true);

    clusterMenu->addMenu(colorMenu);
    connect(colorMenu, SIGNAL(triggered(QAction*)), this, SLOT(setColor(QAction*)));
}

void MainWindow::pathAddSimilarity(QAction *a){
    std::string value;
    a == mass ? value = "mass_comparison" : value = "velocity_comparison";
    std::cout << value << std::endl;

    path_components["similarity"] = value;
    std::string p = constructPath(path_components);
    setFileNames(p);

    //property file here?
    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::setFileNames(std::string p){
    dataManager->setMassFile(p + "mass.csv");
    dataManager->setMergerFile(p + "mergers.csv");
    dataManager->setLinkageFile(p + "linkage");
    dataManager->setTimestepFile(p + "timestep.csv");
    dataManager->setVelocityFile(p + "velocity.csv");
    dataManager->setSimilarityFile(p + "sim.csv");
    dataManager->setFlatMassFile(p + "flat_mass.csv");
    dataManager->setPairedMassFile(p + "paired_mass.csv");
    dataManager->setFlatVelFile(p + "flat_vel.csv");
    dataManager->setPairedVelFile(p + "paired_vel.csv");
    dataManager->setFlatTimeFile(p + "flat_time.csv");
    dataManager->setPairedTimeFile(p + "paired_time.csv");
    dataManager->setFlatXYZVelFile(p + "flat_xyzvel.csv");
    dataManager->setFlatXYZPosFile(p + "flat_xyzpos.csv");
}

void MainWindow::pathAddNorm(bool t){
    std::string value;
    t == true ? value = "normalized" : value = "unnormalized";

    path_components["norm"] = value;
    std::string p = constructPath(path_components);

    setFileNames(p);
    //property file here?

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::pathAddLinkage(QAction *a){
    std::string value;
    if (a == single) value = "single";
    else if (a == complete) value = "complete";
    else if (a == average) value = "average";

    path_components["linkage"] = value;
    std::string p = constructPath(path_components);

    setFileNames(p);
    //property file here?

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();

}

void MainWindow::pathAddBin(QAction *a){
    std::string value;
    a == binE ? value = "e" : value = "d";

    path_components["bin"] = value;
    std::string p = constructPath(path_components);

    setFileNames(p);
    //property file here?

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::pathAddSet(QAction *a){
    std::string value;
    a == A ? value = "0" : value = "1";
    std::cout << "path add set: value: " << value << std::endl;

    path_components["set"] = value;
    std::string p = constructPath(path_components);

    setFileNames(p);
    //property file here?

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::usgs_pathAddLinkage(QAction *q){
    std::string value;
    if (q == complete) value = "complete";
    else if (q == average) value = "average";
    else if (q == single) value = "single";
    usgs_path_components["linkage" ] = value;
    std::string p = constructUSGSPath(usgs_path_components, false);

    dataManager->setTGFile(p + "TG.csv");
    dataManager->setTFFile(p + "TF.csv");
    dataManager->setT0File(p + "T0.csv");
    dataManager->setT33File(p + "T33.csv");
    dataManager->setLonFile(p + "long.csv");
    dataManager->setLatFile(p + "lat.csv");

    p = constructUSGSPath(usgs_path_components, true);
    dataManager->setUSGSLinkageFile(p + ".csv");

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::usgs_pathAddStartTime(QAction *q){
    std::string value;
    q == t0 ? value = "0" : value = "744";
    usgs_path_components["time" ] = value;
    std::string p = constructUSGSPath(usgs_path_components, false);

    dataManager->setTGFile(p + "TG.csv");
    dataManager->setTFFile(p + "TF.csv");
    dataManager->setT0File(p + "T0.csv");
    dataManager->setT33File(p + "T33.csv");
    dataManager->setLonFile(p + "long.csv");
    dataManager->setLatFile(p + "lat.csv");

    p = constructUSGSPath(usgs_path_components, true);
    dataManager->setUSGSLinkageFile(p + ".csv");

    dataManager->init();
    updateScatterControls();
    updateProperties();
    emit this->refresh();
}

void MainWindow::setResolution(QAction *q){
    int i = q->text().toInt();
    std::cout << "setting resolution: " << i << std::endl;
    dataManager->updateResolution(i);
    dataManager->init();
    emit this->refresh();
}

void MainWindow::initClimate(bool t){
    std::cout << "init climate data" << std::endl;
    dataManager->init();
    std::cout << "done initializing data manager" << std::endl;
    emit this->refresh();
}

void MainWindow::setScatterControls()
{
    ////////////////
    /// X AXIS
    ////////////////

    QLabel *x_label = new QLabel;
    x_label->setText("     X Axis");
    ui->x_scatter_controls->addWidget(x_label, 0, 0);

    x_mass_button = new QRadioButton;
    x_mass_button->setText("mass at t = ");
    ui->x_scatter_controls->addWidget(x_mass_button, 1, 0);

    x_mass_slider = new QSlider;
    x_mass_slider->setOrientation(Qt::Horizontal);
    ui->x_scatter_controls->addWidget(x_mass_slider, 2, 0);

    x_mass_label = new QLabel;
    x_mass_label->setText(QString::number(x_mass_slider->value()));
    ui->x_scatter_controls->addWidget(x_mass_label, 1, 1);

    connect(x_mass_slider, SIGNAL(valueChanged(int)), this, SLOT(setXMassLabel(int)));
    connect(x_mass_button, SIGNAL(clicked(bool)), this, SLOT(setXMass(bool)));
    connect(x_mass_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateXMass(int)));

    x_vel_button = new QRadioButton;
    x_vel_button->setText("velocity at t = ");
    ui->x_scatter_controls->addWidget(x_vel_button, 3, 0);

    x_vel_slider = new QSlider;
    x_vel_slider->setOrientation(Qt::Horizontal);
    ui->x_scatter_controls->addWidget(x_vel_slider, 4, 0);

    x_vel_label = new QLabel;
    x_vel_label->setText(QString::number(x_vel_slider->value()));
    ui->x_scatter_controls->addWidget(x_vel_label, 3, 1);

    connect(x_vel_slider, SIGNAL(valueChanged(int)), this, SLOT(setXVelLabel(int)));
    connect(x_vel_button, SIGNAL(clicked(bool)), this, SLOT(setXVel(bool)));
    connect(x_vel_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateXVel(int)));

    x_merger_button = new QRadioButton;
    x_merger_button->setText("merger % > ");
    ui->x_scatter_controls->addWidget(x_merger_button, 5, 0);

    x_merger_slider = new QSlider;
    x_merger_slider->setOrientation(Qt::Horizontal);
    ui->x_scatter_controls->addWidget(x_merger_slider, 6, 0);

    x_merger_label = new QLabel;
    x_merger_label->setText(QString::number(x_merger_slider->value()));
    ui->x_scatter_controls->addWidget(x_merger_label, 5, 1);

    xgroup = new QButtonGroup;
    xgroup->addButton(x_mass_button);
    xgroup->addButton(x_vel_button);
    xgroup->addButton(x_merger_button);
    xgroup->setExclusive(true);
    x_merger_button->setChecked(true);

    connect(x_merger_button, SIGNAL(clicked(bool)), this, SLOT(setXMerger(bool)));
    connect(x_merger_slider, SIGNAL(valueChanged(int)), this, SLOT(setXMergerLabel(int)));
    connect(x_merger_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateXMerger(int)));

    ////////////////
    /// Y AXIS
    ////////////////

    QLabel *y_label = new QLabel;
    y_label->setText("      Y Axis");
    ui->y_scatter_controls->addWidget(y_label, 0, 0);

    y_mass_button = new QRadioButton;
    y_mass_button->setText("mass at t = ");
    y_mass_button->setChecked(true);
    ui->y_scatter_controls->addWidget(y_mass_button, 1, 0);

    y_mass_slider = new QSlider;
    y_mass_slider->setOrientation(Qt::Horizontal);
    ui->y_scatter_controls->addWidget(y_mass_slider, 2, 0);

    y_mass_label = new QLabel;
    y_mass_label->setText(QString::number(y_mass_slider->value()));
    ui->y_scatter_controls->addWidget(y_mass_label, 1, 1);

    connect(y_mass_slider, SIGNAL(valueChanged(int)), this, SLOT(setYMassLabel(int)));
    connect(y_mass_button, SIGNAL(clicked(bool)), this, SLOT(setYMass(bool)));
    connect(y_mass_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateYMass(int)));

    y_vel_button = new QRadioButton;
    y_vel_button->setText("velocity at t = ");
    ui->y_scatter_controls->addWidget(y_vel_button, 3, 0);

    y_vel_slider = new QSlider;
    y_vel_slider->setOrientation(Qt::Horizontal);
    ui->y_scatter_controls->addWidget(y_vel_slider, 4, 0);

    y_vel_label = new QLabel;
    y_vel_label->setText(QString::number(y_vel_slider->value()));
    ui->y_scatter_controls->addWidget(y_vel_label, 3, 1);

    connect(y_vel_slider, SIGNAL(valueChanged(int)), this, SLOT(setYVelLabel(int)));
    connect(y_vel_button, SIGNAL(clicked(bool)), this, SLOT(setYVel(bool)));
    connect(y_vel_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateYVel(int)));

    y_merger_button = new QRadioButton;
    y_merger_button->setText("merger % > ");
    ui->y_scatter_controls->addWidget(y_merger_button, 5, 0);

    y_merger_slider = new QSlider;
    y_merger_slider->setOrientation(Qt::Horizontal);
    ui->y_scatter_controls->addWidget(y_merger_slider, 6, 0);

    y_merger_label = new QLabel;
    y_merger_label->setText(QString::number(y_merger_slider->value()));
    ui->y_scatter_controls->addWidget(y_merger_label, 5, 1);

    ygroup = new QButtonGroup;
    ygroup->addButton(y_mass_button);
    ygroup->addButton(y_vel_button);
    ygroup->addButton(y_merger_button);
    ygroup->setExclusive(true);

    connect(y_merger_button, SIGNAL(clicked(bool)), this, SLOT(setYMerger(bool)));
    connect(y_merger_slider, SIGNAL(valueChanged(int)), this, SLOT(setYMergerLabel(int)));
    connect(y_merger_slider, SIGNAL(sliderMoved(int)), this, SLOT(updateYMerger(int)));

    updateScatterControls();
}

void MainWindow::updateScatterControls()
{
    auto timesteps = dataManager->timesteps;
    if (timesteps.size() > 0){

        int min = 0;
        int max = timesteps.size()-1;


        x_mass_slider->setRange(min, max);
        x_mass_slider->setValue(max);
        x_mass_label->setText(QString::number(timesteps[max]));

        x_vel_slider->setRange(min, max);
        x_vel_slider->setValue(max);
        x_vel_label->setText(QString::number(timesteps[max]));

        x_merger_slider->setRange(0, 120);
        x_merger_slider->setValue(60);
        x_merger_label->setText(QString::number(0.5));

        y_mass_slider->setRange(min, max);
        y_mass_slider->setValue(max);
        y_mass_label->setText(QString::number(timesteps[max]));

        y_vel_slider->setRange(min, max);
        y_vel_slider->setValue(max);
        y_vel_label->setText(QString::number(timesteps[max]));

        y_merger_slider->setRange(0, 120);
        y_merger_slider->setValue(60);
        y_merger_label->setText(QString::number(0.5));
    }
}

void MainWindow::setColor(QAction *a){
    if (a == color_ratio){
        clusters->setColorScheme("ratio");
    }
    else if (a == color_value){
        clusters->setColorScheme("value");
    }
    else if (a == color_category){
        clusters->colorTrees(true);
        clusters->setColorScheme("category");
    }
}

void MainWindow::setXMass(bool t){ emit setScatterX("mass", float(x_mass_slider->value())); }
void MainWindow::updateXMass(int t){ if (x_mass_button->isChecked()) emit setScatterX("mass", float(x_mass_slider->value())); }

void MainWindow::setXVel(bool t){ emit setScatterX("velocity", float(x_vel_slider->value())); }
void MainWindow::updateXVel(int t){ if (x_vel_button->isChecked()) emit setScatterX("velocity", float(x_vel_slider->value())); }

void MainWindow::setXMerger(bool t){ emit setScatterX("n_mergers", float(x_merger_slider->value())); }
void MainWindow::updateXMerger(int t){ if (x_merger_button->isChecked()) emit setScatterX("n_mergers", float(x_merger_slider->value())); }

void MainWindow::setYMass(bool t){ emit setScatterY("mass", float(y_mass_slider->value())); }
void MainWindow::updateYMass(int t){ if (y_mass_button->isChecked()) emit setScatterY("mass", float(y_mass_slider->value())); }

void MainWindow::setYVel(bool t){ emit setScatterY("velocity", float(y_vel_slider->value())); }
void MainWindow::updateYVel(int t){ if (y_vel_button->isChecked()) emit setScatterY("velocity", float(y_vel_slider->value())); }

void MainWindow::setYMerger(bool t){ std::cout << "set y merger" << std::endl; emit setScatterY("n_mergers", float(y_merger_slider->value())); }
void MainWindow::updateYMerger(int t){ if (y_merger_button->isChecked()) emit setScatterY("n_mergers", float(y_merger_slider->value())); }

void MainWindow::setXMassLabel(int t){ x_mass_label->setNum(dataManager->timesteps[t]); }
void MainWindow::setXVelLabel(int t){ x_vel_label->setNum(dataManager->timesteps[t]); }
void MainWindow::setXMergerLabel(int t){ x_merger_label->setNum(float(t)/100.); }

void MainWindow::setYMassLabel(int t){ y_mass_label->setNum(dataManager->timesteps[t]); }
void MainWindow::setYVelLabel(int t){ y_vel_label->setNum(dataManager->timesteps[t]); }
void MainWindow::setYMergerLabel(int t){ y_merger_label->setNum(float(t)/100.); }

void MainWindow::setXProperty(bool t){ emit setScatterX("property", float(xgroup->checkedId())); }
void MainWindow::setYProperty(bool t){ emit setScatterY("property", float(ygroup->checkedId())); }

void MainWindow::updateProperties(){
    std::vector<std::string> names = dataManager->getPropertyNames();
    std::vector<std::vector<float>> properties = dataManager->getProperties();

    QVector<QRadioButton*> buttons;

    for (int i = 0; i < names.size(); i++){
        QRadioButton *x = new QRadioButton;
        QRadioButton *y = new QRadioButton;
        x->setText(QString::fromStdString(names[i]));
        y->setText(QString::fromStdString(names[i]));
        ui->x_scatter_controls->addWidget(x, 7+i, 0);
        ui->y_scatter_controls->addWidget(y, 7+i, 0);

        xgroup->addButton(x, i);
        ygroup->addButton(y, i);

        connect(x, SIGNAL(clicked(bool)), this, SLOT(setXProperty(bool)));
        connect(y, SIGNAL(clicked(bool)), this, SLOT(setYProperty(bool)));

        buttons << x; buttons << y;
    }
}

std::string MainWindow::constructPath(std::map<std::string, std::string> p){
    std::string path;
    path = p.find("base")->second
            + p.find("similarity")->second
            + "/" + p.find("norm")->second
            + "/" + p.find("linkage")->second
            + "/" + p.find("bin")->second + p.find("set")->second + "/";

    std::cout << "loading file at: " << std::endl;
    std::cout << path << std::endl;

    return path;
}

std::string MainWindow::constructUSGSPath(std::map<std::string, std::string> p, bool linkage){
    std::string path;
    path = p.find("base")->second
            + "t_" + p.find("time")->second
            + "_36_";
    if (linkage == true){
        path += "linkage_" + p.find("linkage")->second;
    }

    std::cout << "usgs path: " << path << std::endl;
    return path;
}

void MainWindow::toggleFullTrees(bool t){
    clusters->full_trees = t;
    refresh();
}

void MainWindow::setPathDefaults(){
    path_components["similarity"] = "mass_comparison";
    path_components["norm"] = "normalized";
    path_components["linkage"] = "complete";
    path_components["bin"] = "e";
    path_components["set"] = "0";

    usgs_path_components["time"] = "0";
    usgs_path_components["linkage"] = "complete";
}

void MainWindow::renameWindow(QAction *a){
    if (a == cosmologyData) this->setWindowTitle("Hardware/Hybrid Accelerated Cosmology Code");
    else if (a == climateData) this->setWindowTitle("USGS Dynamical Downscaled Regional Climate");
}
