#ifndef HEATMAP_H
#define HEATMAP_H

#include <QGLWidget>
#include "GLShaderProgram.h"
#include "math.h"
#include "dataManage.h"

class Heatmap : public QGLWidget
{
    Q_OBJECT

public:
    Heatmap(QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());
    struct Halo{
        int time;
        int index;
        unsigned int tag;
        float mass;
        float scaled_mass; //scaled mass, in range -1 to 1
        float merger_ratio;
    };

public slots:
    void setTimeSeries(std::vector<std::vector<Halo> > s);
    void setPermutation(std::vector<int> p);
    void setClusters(std::vector<int> c);
    void highlightTrees(int idx);

private:
    std::vector<std::vector<Halo>> series;
    std::vector<int> permutation;
    std::vector<int> clusters;
    std::vector<Vec3f> colors = {Vec3f(178, 24, 43), Vec3f(214, 96, 77), Vec3f(244, 165, 130),
                                 Vec3f(253, 219, 199), Vec3f(247, 247, 247), Vec3f(209, 229, 240),
                                 Vec3f(146, 197, 222), Vec3f(67, 147, 195), Vec3f(33, 102, 172)};

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();

    GLArrayBuffer::Ptr posVBO;
    GLArrayBuffer::Ptr colorVBO;
    GLArrayBuffer::Ptr alphaVBO;
    GLVertexArray::Ptr VAO;
    GLShaderProgram::Ptr heatmapProgram;

    void setVBOs();
    void initShader();
    Vec3f getColor(float ratio);
    int dataSize;
    int highlight;

    //FIXME:
    std::string path = "/Users/annie/repos/tree_clustering/interface/";
};

#endif // HEATMAP_H
