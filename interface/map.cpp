#include "Map.h"
#include "mainwindow.h"
#include <iostream>
#include <fstream>
#include <sstream>

extern clusterTree tree;
Vec4f blue = Vec4f(0.0, 0.5, 1.0, 1.0);
Vec4f gray = Vec4f(0.5, 0.5, 0.5, 0.5);
int arraySize;
int nodeSize;

Map::Map(QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent)
{
}

void Map::resizeGL(int w, int h)
{
    std::cout << "resizing map" << std::endl;
    glViewport(0, 0, w, h);
    width = w;
    height = h;
    paintGL();
}

void Map::initializeGL()
{
    makeCurrent();
    //openGL settings
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glCullFace(GL_FRONT);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(10);

    posVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    indexVBO = GLArrayBuffer::create(GL_ELEMENT_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    mapVAO = GLVertexArray::create();

    nodePosVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeColorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeCenterVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeVAO = GLVertexArray::create();

    mapProgram = GLShaderProgram::create(path + "map.vert", path + "map.frag");
    if (mapProgram == NULL) close();
    nodeProgram = GLShaderProgram::create(path + "node.vert", path + "node.frag");
    if (nodeProgram == NULL) close();

    loadLinkage();
    //createTree();

    loadMap();

    setShaderAttributes();

    paintGL();
    doneCurrent();
}

float Map::mapPos(bool horiz, int i, int n, int scale)
{
    float max;
    float min;

    if (horiz == false){
        max = 1.0;
        min = -1.0;
    }
    else {
        max = float(n/450.);
        min = -1.0*float(n/450.);
    }
    return (i+1)*(max-min)/(n+1) + min;
}

void Map::loadMap()
{
    int num_selected = 0;

    std::vector<int> indices;
    std::vector<Vec2f> positions;
    std::vector<Vec4f> colors;

    int node_index = highlightedColors.size()-1;
    Vec4f nodeColor;
    if (!highlightedColors.empty()) { nodeColor = Vec4f(highlightedColors[node_index].red(), highlightedColors[node_index].green(),
                highlightedColors[node_index].blue(), highlightedColors[node_index].alpha()); }

    int dataSize = mapLinkage.size() + 1;

    clusterTree::node* node = tree.root;
    int idx = 0;
    int rowSize = 1;

    std::vector<clusterTree::node*> rowNodes = {node};
    std::vector<clusterTree::node*> tempNodes;
    for (int i = 0; i < dataSize; i++){
        int highRowIdx = 0;
        int lowRowIdx = 0;
        tempNodes.clear();
        for (int k = 0; k < rowNodes.size(); k++){
            clusterTree::node* n = rowNodes[k];
            Vec2f parentPos = {mapPos(true, highRowIdx, rowSize, dataSize), -1*mapPos(false, i, dataSize, 0)};

            if (!highlightedNodes.empty() && n == highlightedNodes[node_index]) {
                nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor);
                nodePositions.push_back(parentPos);
            }

            if (n->left != NULL){
                for (int j = 0; j < 4; j++) { indices.push_back(idx); idx++; }
                Vec2f leftPos = {mapPos(true, lowRowIdx, rowSize+1, dataSize), -1*mapPos(false, i+1, dataSize, 0)};
                positions.push_back(parentPos); positions.push_back(leftPos);
                if (n->left->flag == true) { colors.push_back(blue); colors.push_back(blue); }
                else { colors.push_back(gray); colors.push_back(gray); }
                if (!highlightedNodes.empty() && n->left == highlightedNodes[node_index]) {
                    nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor);
                    nodePositions.push_back(leftPos);
                }

                lowRowIdx++;

                Vec2f rightPos = {mapPos(true, lowRowIdx, rowSize+1, dataSize), -1*mapPos(false, i+1, dataSize, 0)};
                positions.push_back(parentPos); positions.push_back(rightPos);
                if (n->right->flag == true) { colors.push_back(blue); colors.push_back(blue); }
                else { colors.push_back(gray); colors.push_back(gray); }

                if (!highlightedNodes.empty() && n->right == highlightedNodes[node_index]) {
                    nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor); nodeColors.push_back(nodeColor);
                    nodePositions.push_back(rightPos);
                }

                lowRowIdx++;

                tempNodes.push_back(n->left);
                tempNodes.push_back(n->right);
            }

            else if (n->left == NULL && n->right != NULL){

                Vec2f rightPos = {mapPos(true, lowRowIdx, rowSize+1, dataSize), -1*mapPos(false, i+1, dataSize, 0)};
                for (int j = 0; j < 2; j++) { indices.push_back(idx); idx++; }
                positions.push_back(parentPos); positions.push_back(rightPos);
                tempNodes.push_back(n->right);
                if (n->right->flag == true) { colors.push_back(blue); colors.push_back(blue); }
                else { colors.push_back(gray); colors.push_back(gray); }

                lowRowIdx++;
            }

            highRowIdx++;
        }

        rowNodes = tempNodes;
        rowSize++;
    }

    posVBO->update(positions.size()*sizeof(Vec2f), &positions.front(), GL_STATIC_DRAW);
    indexVBO->update(indices.size()*sizeof(GLint), &indices.front(), GL_STATIC_DRAW);
    colorVBO->update(colors.size()*sizeof(Vec4f), &colors.front(), GL_STATIC_DRAW);
    mapVAO = GLVertexArray::create();

    //FIXME: there must be a better way to do this
    std::vector<Vec2f> nodeCenters;
    std::vector<Vec2f> nodeVertices;
    for (auto& n : nodePositions)
    {
        nodeCenters.push_back(n); nodeCenters.push_back(n); nodeCenters.push_back(n);
        nodeVertices.push_back(n + Vec2f(0, 0.1));
        nodeVertices.push_back(n + Vec2f(-0.1, -0.1));
        nodeVertices.push_back(n +  Vec2f(0.1, -0.1));
    }

    std::cout << "vertices: " << nodeVertices.size() << std::endl;
    std::cout << "colors: " << nodeColors.size() << std::endl;
    std::cout << "centers: " << nodeCenters.size() << std::endl;

    nodePosVBO->update(nodeVertices.size()*sizeof(Vec2f), &nodeVertices.front(), GL_STATIC_DRAW);
    nodeCenterVBO->update(nodeCenters.size()*sizeof(Vec2f), &nodeCenters.front(), GL_STATIC_DRAW);
    nodeColorVBO->update(nodeColors.size()*sizeof(Vec4f), &nodeColors.front(), GL_STATIC_DRAW);
    nodeVAO = GLVertexArray::create();

    arraySize = indices.size();
    nodeSize = nodePositions.size();

    for (int i = 0; i < nodeSize; i++){
        std::cout << "node center: " << nodeCenters[i].x << ", " << nodeCenters[i].y << std::endl;
    }
}

void Map::paintGL()
{
    makeCurrent();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    std::cout << "data size: " << arraySize << std::endl;

    mapProgram->begin();
    mapVAO->drawArrays(GL_LINES, arraySize);
    mapProgram->end();

    nodeProgram->begin();
    nodeVAO->drawArrays(GL_TRIANGLES, 3*nodeSize);
    nodeProgram->end();

    makeCurrent();
}

void Map::setShaderAttributes()
{
    mapProgram->setVertexAttribute("pos", mapVAO, posVBO, 2, GL_FLOAT, false);
    mapProgram->setVertexAttribute("color", mapVAO, colorVBO, 4, GL_FLOAT, false);

    nodeProgram->setVertexAttribute("pos", nodeVAO, nodePosVBO, 2, GL_FLOAT, false);
    nodeProgram->setVertexAttribute("color", nodeVAO, nodeColorVBO, 4, GL_FLOAT, false);
    nodeProgram->setVertexAttribute("center", nodeVAO, nodeCenterVBO, 2, GL_FLOAT, false);
    nodeProgram->setUniform("screenWidth", float(width));
    nodeProgram->setUniform("screenHeight", float(height));
}

void Map::loadLinkage()
{
    mapLinkage.clear();

    std::vector<int> line;
    std::ifstream linkage_file("/Users/annie/repos/tree_clustering/e_linkage.csv");
    std::string row, field;
    if (linkage_file.is_open()){
        while (std::getline(linkage_file, row))
        {
            line.clear();
            std::stringstream ss(row);
            while (std::getline(ss, field, ','))
            {
                line.push_back(std::stoi(field));
            }
            mapLinkage.push_back({line[0], line[1]});
        }
    }
    else{
        std::cout << "map: couldn't open linkage file " << std::endl;
    }
}

void Map::highlightTree(std::vector<clusterTree::node *> nodes)
{
    this->makeCurrent();
    for (auto n : nodes)
    {
        tree.turn_on_descendants(n);
    }

    loadMap();
    setShaderAttributes();
    paintGL();
    update();
}

void Map::highlightNode(QColor color, clusterTree::node *node)
{
    highlightedNodes.push_back(node);
    highlightedColors.push_back(color);
    loadMap();
    setShaderAttributes();
    paintGL();
    update();
}
