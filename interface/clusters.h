#ifndef CLUSTERS_H
#define CLUSTERS_H

#include "dataManage.h"
#include <QGLWidget>
#include <iostream>

class Clusters : public QGLWidget
{
    Q_OBJECT
    dataManage* dataManager;
public:
    Clusters(dataManage* dataManager = 0, QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();
    bool braid;
    bool random; //or representative
    bool full_trees; //or main masses
    int full_tree_count;
    int posArraySize;

private:
    //shaders
    GLShaderProgram::Ptr groupProgram;
    GLShaderProgram::Ptr singleProgram;
    GLShaderProgram::Ptr nodeProgram;
    GLShaderProgram::Ptr braidProgram;

    //USED IN CURRENT VERSION:
    GLArrayBuffer::Ptr   posVBO; //positions
    GLArrayBuffer::Ptr   lowPosVBO; //position of lower border of current band
    GLArrayBuffer::Ptr   colorVBO; //colors
    GLArrayBuffer::Ptr   vertNormVBO;
    GLArrayBuffer::Ptr   alphaVBO; //low opacity border values
    GLArrayBuffer::Ptr   yvalVBO;

    GLVertexArray::Ptr   groupVAO;
    GLVertexArray::Ptr   singleVAO;

    //for braided clusters:
    GLArrayBuffer::Ptr braidPosVBO;
    GLArrayBuffer::Ptr braidColorVBO;
    GLVertexArray::Ptr braidVAO;

    //USED FOR ADDITION OF INDIVIDUAL TREE RENDERING:
    //first, we'll try making it just lines:
    GLArrayBuffer::Ptr singlePosVBO;
    GLArrayBuffer::Ptr singleColorVBO;
    GLArrayBuffer::Ptr singleAlphaVBO;
    GLArrayBuffer::Ptr singleYVBO;

    //used for circles representing mergers:
    GLArrayBuffer::Ptr nodePosVBO;
    GLArrayBuffer::Ptr nodeColorVBO;
    GLArrayBuffer::Ptr nodeSizeVBO;
    GLArrayBuffer::Ptr nodeCenterVBO;

    GLVertexArray::Ptr nodeVAO;

    std::vector<std::vector<std::vector<dataManage::Halo>>> clusters;
    std::vector<std::vector<std::vector<dataManage::climateNode>>> climate_clusters;
    std::vector<int> currentTrees;

    std::string color_scheme;

public slots:
    void spreadLayout();
    void stackLayout();
    void numLayout();
    void showDiscarded(bool on);
    void showTrees(bool t);
    void showRandomTrees(int n);
    void showRepresentativeTrees();
    void colorTrees(bool t);
    void stackTrees();
    void showAllData(bool t);
    void showClusters(bool t);
    void showTreesInGrid(bool t);
    void showBraid(bool t);

    void refresh();
    void setColorScheme(std::string);

signals:
    void addAxes();

private:
    void setSingleShader();
    void fillVBOs();
    void setShaderAttributes();
    Vec2f y_range;
    int height; int width;
    std::vector<float> y_vals;
    unsigned int currentTag = 0;

    void setYRange(float min, float max);
    bool show_clusters;
    std::vector<int> cluster_sizes;
    bool render_nodes = false;
    Vec3f getColor(float ratio);
    Vec3f getValueColor(float temp);
    void setYValues(std::vector<int> values);
    float timestepToTimeIndex(int t);

    void highlightMergers(std::vector<Vec2f> pos, std::vector<float> ratios);
    float delta = 0.004; //FIXME: rendering setting; temp
};

#endif // CLUSTERS_H
