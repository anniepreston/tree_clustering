#ifndef BRACKETS_H
#define BRACKETS_H
#include <QWidget>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

class Brackets : public QWidget
{
    Q_OBJECT

public:
    Brackets(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

public slots:
    void drawBrackets();

signals:
    void bracketsDrawn();

private:
    QPen *pen;
    QBrush *brush;
    std::vector<QColor> color_set = {QColor(228, 26, 28), QColor(55, 126, 184), QColor(77, 175, 74), QColor(152, 78, 163), QColor(255, 127, 0), QColor(255, 255, 51), QColor(166, 86, 40), QColor(247, 129, 191), QColor(153, 153, 153)};
};

#endif // BRACKETS_H
