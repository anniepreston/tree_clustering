#version 330
in vec4 out_color;
in vec4 out_pos;
in vec2 out_center;
in float out_ratio;
out vec4 FragColor;
uniform float screenWidth;
uniform float screenHeight;

void main(void)
{
    float radius = 0.02;//0.5*out_ratio;

    float ratio = float(screenHeight/screenWidth);
    float x = float(gl_FragCoord.x)/float(screenWidth);
    float y = float(gl_FragCoord.y)/float(screenHeight);

    // Converting from range [0,1] to NDC [-1,1]
    float ndcx = x * 2.0 - 1.0;
    float ndcy = y * 2.0 - 1.0;
    vec2 ndc = vec2(ndcx, ndcy);

    //if (sqrt(dot(ndc.xy, out_center.xy)) < radius)
    if (sqrt(pow(out_pos.x - out_center.x, 2) + pow((out_pos.y - out_center.y)*ratio, 2)) < radius)
    {
        FragColor = vec4(out_color.x, out_color.y, out_color.z, 0.8);
    }
    else { FragColor = vec4(1.0, 1.0, 1.0, 0.0); }
}
