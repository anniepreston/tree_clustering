#version 330
in vec4  color;
in vec2  position;

out vec4 out_color;

void main(void)
{
    out_color = color;

    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}
