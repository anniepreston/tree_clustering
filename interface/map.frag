#version 330
in vec4 out_color;
out vec4 FragColor;

void main(void)
{
    FragColor = vec4(out_color.x, out_color.y, out_color.z, out_color.w);
}

