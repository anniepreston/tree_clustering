#ifndef PIEPLOT_H
#define PIEPLOT_H

#include "dataManage.h"
#include <QGLWidget>

class piePlot : public QGLWidget
{
    Q_OBJECT
    dataManage* dataManager;
public:
    piePlot(dataManage* dataManager = 0, QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();

public slots:
    void refresh();

private:
    GLShaderProgram::Ptr pieShader;

    GLVertexArray::Ptr pieVAO;
    GLArrayBuffer::Ptr pieVBO;
    GLArrayBuffer::Ptr colorVBO;
    GLArrayBuffer::Ptr centerVBO;

    void createPies(std::vector<int> cs);
    void setShaders();
    int vboSize;
};

#endif // PIEPLOT_H
