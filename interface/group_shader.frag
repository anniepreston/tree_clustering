#version 330
in vec4 out_color;
in float dist;
in float out_alpha;

out vec4 FragColor;

void main(void)
{
    FragColor = vec4(out_color);
    //FragColor = vec4(1.0,1.0, 1.0, 0.0);
}
