#ifndef MAP_H
#define MAP_H

#include <QGLWidget>
#include "GLArrayBuffer.h"
#include "GLShaderProgram.h"
#include "GLVertexArray.h"
#include <QApplication>
#include <vector>
#include "clusterTree.h"

class Map : public QGLWidget
{
    Q_OBJECT

public:
    Map(QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    std::string path = "/Users/annie/repos/tree_clustering/interface/";
    void loadMap();
    float mapPos(bool horiz, int i, int n, int scale);
    void setShaderAttributes();

    int height; int width;

    //map / hierarchy navigation
    GLShaderProgram::Ptr mapProgram;
    GLShaderProgram::Ptr nodeProgram;
    GLVertexArray::Ptr   mapVAO;
    GLVertexArray::Ptr   nodeVAO;
    GLArrayBuffer::Ptr   posVBO;
    GLArrayBuffer::Ptr   indexVBO;
    GLArrayBuffer::Ptr   colorVBO;
    GLArrayBuffer::Ptr   nodeColorVBO;
    GLArrayBuffer::Ptr   nodePosVBO;
    GLArrayBuffer::Ptr   nodeCenterVBO;

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    std::vector<std::vector<int> > mapLinkage;
    clusterTree mapTree;

    void loadLinkage();

    std::vector<clusterTree::node*> highlightedNodes;
    std::vector<QColor> highlightedColors;
    std::vector<Vec2f> nodePositions;
    std::vector<Vec4f> nodeColors;

public slots:
    void highlightTree(std::vector<clusterTree::node *> nodes);
    void highlightNode(QColor color, clusterTree::node* node);

};

#endif // MAP_H
