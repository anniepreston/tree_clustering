#ifndef SCATTERPLOT_H
#define SCATTERPLOT_H

#include <QGLWidget>
#include "GLShaderProgram.h"
#include "dataManage.h"

class ScatterPlot : public QGLWidget
{
    Q_OBJECT

public:
    ScatterPlot(dataManage* dataManager = 0, QWidget *parent = 0, const QGLFormat &format = QGLFormat::defaultFormat());

    virtual void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();

    std::string x_var; std::string y_var;
    float x_threshold; float y_threshold;

private:
    std::unique_ptr<dataManage> dataManager;
    GLShaderProgram::Ptr scatterProgram;

    GLVertexArray::Ptr scatterVAO;
    GLArrayBuffer::Ptr colorVBO;
    GLArrayBuffer::Ptr centerVBO;
    GLArrayBuffer::Ptr scatterVBO;

    void createPlot();
    void setShader();
    void scale(std::vector<float> &vec);

public slots:
    void setXAxis(std::string value, float threshold);
    void setYAxis(std::string value, float threshold);
    void refresh();

signals:
    void setXAxis(std::vector<float> range, std::string label);
    void setYAxis(std::vector<float> range, std::string label);
    void updateXAxis(float min, float max);
    void updateYAxis(float min, float max);
    void updateXLabel(QString l);
    void updateYLabel(QString l);
};

#endif // SCATTERPLOT_H
