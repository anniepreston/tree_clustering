#version 330
in vec2 pos;
in vec4 color;

out vec4 out_color;

void main(void)
{
    out_color = vec4(color.x, color.y, color.z, color.w);
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}

