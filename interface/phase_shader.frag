#version 330
in vec3 out_color;
in vec2 out_center;
in float out_width;
in float out_height;
in float out_scale;

out vec4 FragColor;

void main(void)
{
    float x = gl_FragCoord.x/out_width;
    float y = gl_FragCoord.y/out_height;
    float x_0 = out_center.x; float y_0 = out_center.y;

    float dist = sqrt((x - x_0)*(x - x_0)*(out_width/out_height)
                      + (y - y_0)*(y - y_0)*(out_height/out_width));
    if (dist < out_scale) FragColor = vec4(out_color.x/255., out_color.y/255., out_color.z/255., 0.4);
    else FragColor = vec4(1.0, 1.0, 1.0, 0.0);
}
