#version 330
in vec4 pos_diff;
in vec4 vel_diff;
in vec3 color;
in float time;

in vec2 offset;
in float radius;

uniform float width;
uniform float height;

uniform int pos_or_vel; // 1 = pos, 0 = vel
uniform int component; // 0 = x, 1 = y, 2 = z, 3 = w (mag)

out float out_width;
out float out_height;
out float out_scale;
out vec3 out_color;
out vec2 out_pos;
out vec2 out_center;

void main(void)
{
    out_scale = 0.01*radius; //?

    vec4 temp_center;
    vec2 coord;
    pos_or_vel == 1 ? temp_center = pos_diff : temp_center = vel_diff;
    if (component == 0) coord.y = temp_center.x;
    else if (component == 1) coord.y = temp_center.y;
    else if (component == 2) coord.y = temp_center.z;
    else coord.y = temp_center.w;
    coord.x = time;

    out_center = vec2(coord.x, coord.y);

    vec2 pos = out_center + offset;

    out_color = color;
    out_width = width; out_height = height;
    gl_Position = vec4(2*pos.x-1., 2*pos.y-1., 0, 1);
}
