#include "mainwindow.h"
#include <QApplication>
#include <GLUT/glut.h>

int main(int argc, char *argv[])
{
    QGLFormat glFormat = QGLFormat::defaultFormat();
    glFormat.setVersion(3, 3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);              //multisample buffer support
    glFormat.setSamples(4);                       //number of samples per pixel
    QGLFormat::setDefaultFormat(glFormat);

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
