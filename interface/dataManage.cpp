#include "dataManage.h"
#include "mainwindow.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QOpenGLFunctions>
#include <QDebug>

//FIXME: break the data manager into a polymorphic class structure; climate data manager + cosmology data manager ( + ... )

int n_clusters;
clusterTree tree;
Vec2f y_range;
int num_discarded;
std::vector<int> history;

bool sortFunction(int i, int j){ return(i>j); }
bool discarded;

template <typename T>
std::vector<size_t> sort_indices(const std::vector<T> &v) {
  // initialize original index locations
  std::vector<size_t> idx(v.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indices based on comparing values in v, from largest to smallest
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}

template <typename T>
std::vector<size_t> sort_indices_smallest(const std::vector<T> &v) {
  // initialize original index locations
  std::vector<size_t> idx(v.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indices based on comparing values in v, from largest to smallest
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}

void dataManage::showHalos(QString tag)
{
}

struct {
    bool operator()(dataManage::Halo a, dataManage::Halo b){
        return a.merger_ratio > b.merger_ratio;
    }
} mergerCompare;

dataManage::dataManage(QObject *parent) :
    QObject(parent)
{
}

void dataManage::init(){
    loadLinkage();
    createTree();
    if (dataType == "cosmology") {
        loadTimeSeries();
        loadFlatTrees();
        loadFlatProgenitors();
    }
    else if (dataType == "climate") {
        loadClimateTimeSeries();
    }

    cluster_assignments = tree.get_clusters(tree.root, linkage, resolution); //ADAPTIVELY DETERMINE LEVEL !
    loadClusters(cluster_assignments);
}

void dataManage::loadLinkage()
{
    linkage.clear();

    std::string file;
    if (dataType == "cosmology") file = linkage_filename;
    else if (dataType == "climate") file = climate_linkage_filename;

    std::vector<int> line;
    std::ifstream linkage_file(file);
    std::string row, field;
    if (linkage_file.is_open()){
        while (std::getline(linkage_file, row))
        {
            line.clear();
            std::stringstream ss(row);
            while (std::getline(ss, field, ','))
            {
                line.push_back(std::stoi(field));
            }
            linkage.push_back({line[0], line[1]});
        }
    }
    else{
        std::cout << "couldn't open linkage file " << std::endl;
    }
    std::cout << "done loading linkage" << std::endl;
}

void dataManage::createTree()
{
    std::cout << "creating cluster tree" << std::endl;
    //create clustering tree
    int n = linkage.size();
    tree.root = new clusterTree::node;
    tree.root->data = 2*n;
    for(int i = 0; i < n; i++)
    {
        int index = n-1-i;
        tree.insert({linkage[index][0], linkage[index][1]}, (2*n)-i, i);
    }
}

void dataManage::loadTimeSeries()
{
    timesteps.clear();
    //FIXME: hack
    //timesteps.push_back(499);
    bool p = (property_filename != "");

    //load merger tree data representations
    float min = 9999;
    float max = -9999;
    int total_trees = 0;

    trees.clear();
    std::string mass_row, mass_field;
    std::string vel_row, vel_field;
    std::string time_row, time_field;
    std::string property_row, property_field;
    std::string merger_row, merger_field;
    std::vector<int> mass_line, merger_line, time_line, vel_line;
    std::vector<Halo> tree_row;
    std::vector<float> property_row_float;

    std::ifstream mass_file(mass_filename);
    std::ifstream vel_file(velocity_filename);
    std::ifstream merger_file(merger_filename);
    std::ifstream time_file(timestep_filename);
    std::ifstream property_file(property_filename);

    if (mass_file.is_open() && merger_file.is_open() && time_file.is_open()
            && vel_file.is_open() && (p == false || property_file.is_open())){
        while (std::getline(mass_file, mass_row) && std::getline(merger_file, merger_row) && std::getline(time_file, time_row)
               && std::getline(vel_file, vel_row) && (std::getline(property_file, property_row) || p == false))
        {
            std::vector<float> temp_mergers;
            std::vector<int> temp_times;
            std::vector<float> temp_properties;
            std::vector<float> temp_vels;
            tree_row.clear();
            mass_line.clear(); merger_line.clear(); vel_line.clear();
            std::stringstream mass_ss(mass_row);
            std::stringstream vel_ss(vel_row);
            std::stringstream merger_ss(merger_row);
            std::stringstream time_ss(time_row);
            std::stringstream property_ss(property_row);
            while (std::getline(mass_ss, mass_field, ','))
            {
                float mass = std::stof(mass_field);
                Halo halo;
                halo.mass = mass;
                halo.scaled_mass = mass;
                if (mass > 0) { tree_row.push_back(halo); }
                if (mass > 0 && mass > max) { max = mass; } //min, max = mass range
                if (mass > 0 && mass < min) { min = mass; }
            }
            while (std::getline(merger_ss, merger_field, ','))
            {
                float merger = std::stof(merger_field);
                temp_mergers.push_back(merger);
            }
            while (std::getline(vel_ss, vel_field, ','))
            {
                float vel = std::stof(vel_field);
                temp_vels.push_back(vel);
            }
            while (std::getline(time_ss, time_field, ','))
            {
                int time = std::stoi(time_field);
                temp_times.push_back(time);
                //FIXME: this method misses t=499...
                if (std::find(timesteps.begin(), timesteps.end(), time) == timesteps.end()) timesteps.push_back(time);
            }
            if (p){
                while (std::getline(property_ss, property_field, ',')){
                    float property = std::stof(property_field);
                    temp_properties.push_back(property);
                }
            }
            for (int i = 0; i < tree_row.size(); i++){
                tree_row[i].merger_ratio = temp_mergers[i];
                tree_row[i].time = temp_times[i];
                tree_row[i].velocity = temp_vels[i];
            }
            std::reverse(tree_row.begin(), tree_row.end());
            //expand time series to same length:
            while (tree_row.size() < 100) //FIXME
            {
                std::vector<Halo>::iterator it;
                it = tree_row.begin();
                Halo halo;
                halo.scaled_mass = min;
                halo.mass = min;
                halo.velocity = 0;
                halo.scaled_vel = 0;
                halo.merger_ratio = 0;
                tree_row.insert(it, halo);
            }
            float max_vel = 0;
            for (auto &h : tree_row){
                if (h.velocity > max_vel) max_vel = h.velocity;
            }
            //scale
            for (auto &h : tree_row){
                h.scaled_mass = 2*(h.mass - tree_row[0].mass)/(tree_row[99].mass - tree_row[0].mass) - 1.0;
                h.scaled_vel = 2*(h.velocity - tree_row[0].velocity)/(max_vel - tree_row[0].velocity) - 1.0;
            }

            trees.push_back(tree_row);
            if (p) properties.push_back(temp_properties);
        }
        mass_file.close();
        merger_file.close();
        if (p) property_file.close();
    }
    else{
        std::cout << "couldn't open merger and/or mass file" << std::endl;
    }
    std::sort(timesteps.begin(), timesteps.end());
}

float dataManage::scalePairedTreeMass(float m, int idx){
    auto mainMassTree = trees[idx];
    return 2*(m - mainMassTree[0].mass)/(mainMassTree[99].mass - mainMassTree[0].mass) - 1.0;
}

void dataManage::loadClimateTimeSeries()
{
    std::cout << "loading tg file: " << tg_filename << std::endl;

    timesteps.clear();

    climateSeries.clear();

    min_temp = 100; max_temp = -100;

    std::vector<climateNode> row;
    std::string tg_row, tg_field;
    std::string tf_row, tf_field;
    std::string t0_row, t0_field;
    std::string t33_row, t33_field;
    std::string lat_row, lat_field;
    std::string long_row, long_field;

    std::ifstream tg_file(tg_filename);
    std::ifstream tf_file(tf_filename);
    std::ifstream t0_file(t0_filename);
    std::ifstream t33_file(t33_filename);
    std::ifstream lat_file(lat_filename);
    std::ifstream long_file(long_filename);

    if (tg_file.is_open() && tf_file.is_open() && t0_file.is_open()
            && t33_file.is_open() && lat_file.is_open() && long_file.is_open()){
        while (std::getline(tg_file, tg_row) && std::getline(tf_file, tf_row)
            && std::getline(t0_file, t0_row) && std::getline(t33_file, t33_row)
            && std::getline(lat_file, lat_row) && std::getline(long_file, long_row))
        {
            row.clear();

            std::vector<float> temp_tg, temp_tf;
            std::vector<float> temp_t0, temp_t33;
            std::vector<float> temp_lat, temp_long;

            std::stringstream tg_ss(tg_row);
            std::stringstream tf_ss(tf_row);
            std::stringstream t0_ss(t0_row);
            std::stringstream t33_ss(t33_row);
            std::stringstream lat_ss(lat_row);
            std::stringstream long_ss(long_row);

            while (std::getline(tg_ss, tg_field, ',')){
                float tg = std::stof(tg_field);
                temp_tg.push_back(tg);
                if (tg > max_temp) max_temp = tg;
                if (tg < min_temp) min_temp = tg;
            }
            //FIXME: TF data is incomplete
            /*
            std::cout << "reading TF: " << std::endl;
            while (std::getline(tf_ss, tf_field, ',')){
                float tf = std::stof(tf_field);
                temp_tf.push_back(tf);
            }
            */
            while (std::getline(t0_ss, t0_field, ',')){
                float t0 = std::stof(t0_field);
                temp_t0.push_back(t0);
            }
            while (std::getline(t33_ss, t33_field, ',')){
                float t33 = std::stof(t33_field);
                temp_t33.push_back(t33);
            }
            while (std::getline(lat_ss, lat_field,',')){
                float lat = std::stof(lat_field);
                temp_lat.push_back(lat);
            }
            while (std::getline(long_ss, long_field, ',')){
                float lon = std::stof(long_field);
                temp_long.push_back(lon);
            }
            for (int i = 0; i < temp_tg.size(); i++){
                climateNode node;
                node.tg = temp_tg[i]; //node.tf = temp_tf[i];
                node.t0 = temp_t0[i]; node.t33 = temp_t33[i];
                node.lat = temp_lat[0]; node.lon = temp_long[0];
                row.push_back(node);
            }
            climateSeries.push_back(row);
        }
        for (auto & t : climateSeries){
            for (auto & n : t) n.scaled_temp = 2*(n.tg - min_temp)/(max_temp - min_temp) - 1.0;
        }

        tf_file.close(); tg_file.close();
        t0_file.close(); t33_file.close();
        lat_file.close(); long_file.close();
    }
}

void dataManage::loadFlatTrees()
{
    flatTrees.clear();
    std::string mass_row, mass_field;
    std::string vel_row, vel_field;
    std::string time_row, time_field;
    std::vector<Halo> tree_row;

    std::ifstream mass_file(paired_mass_filename);
    std::ifstream vel_file(paired_vel_filename);
    std::ifstream time_file(paired_time_filename);

    int idx = 0;

    if (mass_file.is_open() && vel_file.is_open() && time_file.is_open()){
        while(std::getline(mass_file, mass_row) && std::getline(vel_file, vel_row)
              && std::getline(time_file, time_row)){
            std::vector<float> vels;
            std::vector<int> times;
            tree_row.clear();

            std::stringstream mass_ss(mass_row);
            std::stringstream vel_ss(vel_row);
            std::stringstream time_ss(time_row);
            while (std::getline(mass_ss, mass_field, ',')){
                float mass = std::stof(mass_field);
                Halo halo;
                halo.mass = mass;
                halo.scaled_mass = mass;
                if (mass > 0) { tree_row.push_back(halo); }
            }
            while (std::getline(time_ss, time_field, ','))
            {
                times.push_back(std::stoi(time_field));
            }
            while (std::getline(vel_ss, vel_field, ','))
            {
                float vel = std::stof(vel_field);
                vels.push_back(vel);
            }
            for (int i = 0; i < tree_row.size(); i++){
                tree_row[i].time = times[i];
                tree_row[i].velocity = vels[i];
            }
            float max_mass = 0; float max_vel = 0;
            float min_mass = 10000000000000000000; float min_vel = 1000000000000000000;
            for (auto &h : tree_row){
                if (h.mass > max_mass) max_mass = h.mass;
                if (h.mass < min_mass) min_mass = h.mass;
            }
            for (auto &h : tree_row){
                h.scaled_mass = scalePairedTreeMass(h.mass, idx);
                //h.scaled_mass = 2*(h.mass - min_mass)/(max_mass - min_mass) - 1.0;
                h.scaled_vel = 2*(h.velocity - min_vel)/(max_vel - min_vel) - 1.0;
            }
            flatTrees.push_back(tree_row);
            idx++;
        }
        mass_file.close();
        vel_file.close();
        time_file.close();
    }
    else std::cout << "couldn't open flattened tree files" << std::endl;
}

void dataManage::loadFlatProgenitors()
{
    flatProgs.clear();
    int count = 0;
    std::string vel_row, vel_field;
    std::string pos_row, pos_field;
    std::string time_row, time_field;
    std::string mass_row, mass_field;

    std::ifstream vel_file(flat_xyzvel_filename);
    std::ifstream pos_file(flat_xyzpos_filename);
    std::ifstream time_file(flat_time_filename);
    std::ifstream mass_file(flat_mass_filename);

    if (vel_file.is_open() && pos_file.is_open() && time_file.is_open() && mass_file.is_open()){
        while(std::getline(vel_file, vel_row) && std::getline(pos_file, pos_row)
            && std::getline(time_file, time_row) && std::getline(mass_file, mass_row)){

            float ref_mass = 0;

            std::multimap<int, Halo> tree_map;
            std::vector<Halo> tree_halos;

            std::vector<int> times;
            std::vector<Vec3f> vels;
            std::vector<Vec3f> poss;
            std::vector<float> ms;

            std::stringstream vel_ss(vel_row);
            std::stringstream pos_ss(pos_row);
            std::stringstream time_ss(time_row);
            std::stringstream mass_ss(mass_row);
            while (std::getline(vel_ss, vel_field, ',')){
                float x, y, z;
                vel_field.erase(vel_field.begin()); vel_field.erase(vel_field.end());
                std::stringstream v(vel_field);
                v >> x >> y >> z;
                Vec3f vel = Vec3f(x, y, z);
                vels.push_back(vel);
            }
            while (std::getline(pos_ss, pos_field, ',')){
                float x, y, z;
                pos_field.erase(pos_field.begin()); pos_field.erase(pos_field.end());
                std::stringstream p(pos_field);
                p >> x >> y >> z;
                Vec3f pos = Vec3f(x, y, z);
                poss.push_back(pos);
            }
            while (std::getline(time_ss, time_field, ',')){
                times.push_back(std::stoi(time_field));
            }
            while (std::getline(mass_ss, mass_field, ',')){
                ms.push_back(std::stof(mass_field));
            }
            for (int i = 0; i < times.size(); i++){
                if (i==0) ref_mass = ms[i];
                Halo h;
                h.mass = ms[i]; h.xyzvel = vels[i];
                h.xyzpos = poss[i]; h.time = times[i];
                if (h.mass > 0.1*ref_mass) tree_halos.push_back(h);
            }
            flatProgs.push_back(tree_halos);
            count += tree_halos.size();
        }
    }
    std::cout << "number of halos: " << count << std::endl;
}

void dataManage::loadClusters(std::vector<int> cluster_labels)
{  
    cluster_sizes.clear(); //a list of the size of each cluster (length = number of clusters)
    clusters.clear();
    climate_clusters.clear();
    cluster_indices.clear();

    std::vector<int> permutation = tree.get_permutation(tree.root);

    num_discarded = 0;

    //FIXME: i don't think "cluster_indices" is necessary. it's just an ordered list of the cluster labels
    for (int i = 0; i < permutation.size(); i++)
    {
        int index = cluster_labels[permutation[i]];
        if (std::find(cluster_indices.begin(), cluster_indices.end(), index) == cluster_indices.end()
                && index != 0){
            cluster_indices.push_back(index);
        }
    }

    n_clusters = *std::max_element(cluster_labels.begin(), cluster_labels.end());
    for (auto &i : cluster_indices)
    {
        int size = 0;
        std::vector<std::vector<Halo>> cluster_members;
        std::vector<std::vector<climateNode>> climate_cluster_members;
        for (int j = 0; j < cluster_labels.size(); j++)
        {
            if (cluster_labels[permutation[j]] == i)
            {
                if (dataType == "climate") climate_cluster_members.push_back(climateSeries[permutation[j]]);
                else if (dataType == "cosmology") cluster_members.push_back(trees[permutation[j]]);
                size++;
            }
        }
        cluster_sizes.push_back(size);

        clusters.push_back(cluster_members);
        climate_clusters.push_back(climate_cluster_members);
    }

    //set cluster order
    cluster_order.clear();
    std::vector<int> cluster_sizes_temp = cluster_sizes;
    for (auto i : sort_indices(cluster_sizes_temp)){
        cluster_order.push_back(int(i));
    }
}

void dataManage::subCluster()
{
    //FIXME: properly integrate this
    /*
    this->makeCurrent();
    emit this->clearTicks();
    int index = cluster_indices[sender()->property("index").toInt()];
    int n_clusters = 4;

    std::vector<clusterTree::node*> nodes = tree.get_n_sub_nodes(tree.root, n_clusters);  //can iterate here as needed

    if (history.size() > 1)
    {
        for (int i = 1; i < history.size(); i++){
            nodes = tree.get_n_sub_nodes(nodes[history[i]], n_clusters);
        }
    }

    clusterTree::node* startNode = nodes[index-1];

    cluster_assignments = tree.descendant_indices_n_clusters(startNode, n_clusters, linkage.size()); //FIXME ?

    history.push_back(index);

    loadClusters(cluster_assignments);
    paintGL();
    update();
    */
}

void dataManage::updateMap(int ci, int index, clusterTree::node* node)
{
    /*
    std::cout << "updating map with color index " << ci << std::endl;
    emit highlightTree({node});

    QColor color = QColor(color_sets[(ci-1)%4][index][0], color_sets[(ci-1)%4][index][1], color_sets[(ci-1)%4][index][2]);
    emit highlightNode(color, node);
    */
}

clusterTree dataManage::getTree()
{
    return tree;
}

std::vector<std::vector<int> > dataManage::getLinkage()
{
    return linkage;
}

void dataManage::findSignificantTags(unsigned int tag)
{
    std::vector<Halo> topMergers;
    std::vector<unsigned int> returnTags;
    std::vector<int> returnTimes;
    std::vector<Vec2f> nodePos;
    std::vector<float> nodeRatios;
    //get tree whose final tag matches this tag
    for (int i = 0; i < trees.size(); i++){
        if (trees[i][99].tag == tag){
            for (int j = 30; j < trees[i].size(); j++){
                trees[i][j].index = j;
                topMergers.push_back(trees[i][j]);
            }
        }
    }
    std::sort(topMergers.begin(), topMergers.end(), mergerCompare);
    for (int i = 0; i < 4; i++){
        returnTags.push_back(topMergers[i].tag);
        returnTimes.push_back(topMergers[i].time);
        nodePos.push_back(Vec2f(2*float(topMergers[i].index)/100 - 1, topMergers[i].scaled_mass));
        nodeRatios.push_back(topMergers[i].merger_ratio);
    }
    //highlightMergers(nodePos, nodeRatios);

    //emit showTags(returnTags, returnTimes);
}

void dataManage::updateResolution(int r)
{
    //FIXME: properly integrate this
    //this->makeCurrent();
    resolution = r;

    cluster_assignments = tree.get_clusters(tree.root, linkage, resolution); //ADAPTIVELY DETERMINE LEVEL !
    loadClusters(cluster_assignments);
    //fillVBOs();

    //this->doneCurrent();
}

void dataManage::sortTrees(){
    //sort trees in order from most to least common cluster:
    std::map<int, int> countToCluster;

    int max_idx = std::distance(cluster_assignments.begin(), std::max_element(cluster_assignments.begin(), cluster_assignments.end()));
    for (int i = 0; i <= cluster_assignments[max_idx]; i++){
        int count = std::count(cluster_assignments.begin(), cluster_assignments.end(), i);
        countToCluster.insert(std::make_pair(count, i));
    }

    std::vector<int> sorted_indices;
    for (std::map<int, int>::reverse_iterator it = countToCluster.rbegin(); it != countToCluster.rend(); ++it){
        int index = it->second;
        for (int j = 0; j < cluster_assignments.size(); j++) {
            if (cluster_assignments[j] == index) sorted_indices.push_back(j);
        }
    }

    //sort trees
    std::vector<std::vector<Halo>> temp_trees;
    for (int i = 0; i < trees.size(); i++) temp_trees.push_back(trees[sorted_indices[i]]);
    trees = temp_trees;

    //sort cluster assignments
    std::vector<int> temp_assignments;
    for (int i = 0; i < cluster_assignments.size(); i++) temp_assignments.push_back(cluster_assignments[sorted_indices[i]]);
    cluster_assignments = temp_assignments;
}

std::vector<std::vector<dataManage::Halo>> dataManage::getRepresentativeTrees()
{
    std::vector<std::vector<Halo>> rep_trees;

    if (similarity_filename != ""){
        //load similarity matrix
        std::vector<std::vector<float>> sim_matrix;

        std::ifstream sim_file(similarity_filename);
        std::string row;
        std::vector<float> sim_row;
        std::string field;

        if (sim_file.is_open()){
            while (std::getline(sim_file, row)){
                sim_row.clear();
                std::stringstream ss(row);
                while (std::getline(ss, field, ',')){
                    float s = std::stof(field);
                    sim_row.push_back(s);
                }
                sim_matrix.push_back(sim_row);
            }
        }
        sim_file.close();

        for (int i = 1; i <= cluster_sizes.size(); i++){
            std::vector<int> members;
            std::vector<float> averages;
            for (int j = 0; j < trees.size(); j++){
                if (cluster_assignments[j] == i){
                    members.push_back(j);
                }
            }
            for (auto & m : members){
                float avg = 0;
                for (auto & n : members){
                    avg += sim_matrix[m][n];
                }
                avg = avg/(float(members.size()));
                averages.push_back(avg);
            }
            rep_trees.push_back(trees[members[std::distance(averages.begin(), std::min_element(averages.begin(), averages.end()))]]);
        }
    }

    return rep_trees;
}

std::vector<std::vector<int>> dataManage::getRepresentativeIndices(int n)
{
    std::vector<std::vector<int>> indices;
    indices.resize(cluster_sizes.size());

    if (similarity_filename != ""){
        //load similarity matrix
        std::vector<std::vector<float>> sim_matrix;

        std::ifstream sim_file(similarity_filename);
        std::string row;
        std::vector<float> sim_row;
        std::string field;

        if (sim_file.is_open()){
            while (std::getline(sim_file, row)){
                sim_row.clear();
                std::stringstream ss(row);
                while (std::getline(ss, field, ',')){
                    float s = std::stof(field);
                    sim_row.push_back(s);
                }
                sim_matrix.push_back(sim_row);
            }
        }
        sim_file.close();

        for (int i = 0; i < cluster_sizes.size(); i++){
            std::vector<int> members;
            std::vector<float> averages;
            for (int j = 0; j < trees.size(); j++){
                if (cluster_assignments[j] == i+1){
                    members.push_back(j);
                }
            }
            for (auto & m : members){
                float avg = 0;
                for (auto & mem : members){
                    avg += sim_matrix[m][mem];
                }
                avg = avg/(float(members.size()));
                averages.push_back(avg);
            }
            for (auto & j : sort_indices_smallest(averages)){
                indices[i].push_back(members[j]);
                if (indices[i].size() == n) break;
            }
        }
    }

    return indices;
}
