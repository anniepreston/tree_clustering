#ifndef DATAMANAGE_H
#define DATAMANAGE_H

#include "GLArrayBuffer.h"
#include "GLShaderProgram.h"
#include "GLVertexArray.h"
#include "GLFramebuffer.h"
#include <QApplication>
#include <QObject>
#include <vector>
#include "clusterTree.h"
#include <iostream>

class dataManage : public QObject
{
    Q_OBJECT

public:
    QString dataType;

    //FIXME: masses are already normalized
    struct Halo{
        int time;
        int index;
        unsigned int tag;
        float mass;
        float scaled_mass; //scaled mass, in range -1 to 1
        float merger_ratio;
        float velocity;
        float scaled_vel;
        Vec3f xyzvel;
        Vec3f xyzpos;
    };

    struct climateNode{
        float tg; //ground temp
        float tf; //foliage temp
        float lat; //latitude temp
        float lon; //longitude temp
        float t0;
        float t33;

        float scaled_temp;
    };

    void init();
    std::string path = "/Users/annie/repos/tree_clustering/interface/"; //FIXME: eliminate this

    //view/appearance
    std::vector<Vec3f> heat_colors = {Vec3f(178, 24, 43), Vec3f(214, 96, 77), Vec3f(244, 165, 130),
                                    Vec3f(253, 219, 199), Vec3f(220, 220, 220), Vec3f(209, 229, 240),
                                    Vec3f(146, 197, 222), Vec3f(67, 147, 195), Vec3f(33, 102, 172)};

    std::vector<Vec3f> color_set = {{102, 194, 165}, {252, 141, 98}, {141, 160, 203},
                                    {231, 138, 195}, {166, 216, 84}, {255, 217, 47},
                                    {229, 196, 148}, {179, 179, 179}, {0, 0, 0}};

    std::string color_scheme = "heatmap"; //or 'gray'
    std::vector<int> cluster_indices;

    //dendrogram settings etc
    std::vector<std::vector<int> > linkage;
    int resolution = 4; //number of clusters
    std::vector<int> timesteps;

    std::vector<int> getClusterAssignments() { return cluster_assignments; }
    std::vector<int> getClusterIndices() { return cluster_indices; }
    std::vector<int> getClusterSizes() { return cluster_sizes; }
    std::vector<std::vector<Halo>> getTrees() { /* sortTrees(); */ return trees; }
    std::vector<std::vector<Halo>> getRepresentativeTrees();
    std::vector<std::vector<Halo>> getFlatTrees() { return flatTrees; }
    std::vector<std::vector<climateNode>> getSeries() { return climateSeries; }
    std::vector<std::vector<climateNode>> getRepresentativeSeries();
    std::vector<std::vector<int>> getRepresentativeIndices(int);
    std::vector<std::vector<Halo>> getAllProgs() { return flatProgs; }
    std::vector<int> getTimesteps() { return timesteps; }
    float getMaxTemp() { return max_temp; }
    float getMinTemp() { return min_temp; }

    clusterTree getTree();
    std::vector<std::vector<int> > getLinkage();

    std::vector<int> getClusterOrder() { return cluster_order; }

    dataManage(QObject *parent = 0);
    std::vector<std::vector<float> > getPaths();
    std::vector<std::vector<std::vector<Halo>>> getClusters() { return clusters; }
    std::vector<std::vector<std::vector<climateNode>>> getClimateClusters() { return climate_clusters; }
    void findSignificantTags(unsigned int tag);

    void setMassFile(std::string fn) { mass_filename = fn; }
    void setVelocityFile(std::string fn) { velocity_filename = fn; }
    void setMergerFile(std::string fn) { merger_filename = fn;  }
    void setTagFile(std::string fn) { tag_filename = fn; }
    void setLinkageFile(std::string fn) { linkage_filename = fn; }
    void setTimestepFile(std::string fn) { timestep_filename = fn; }
    void setSimilarityFile(std::string fn) { similarity_filename = fn; }
    void setPropertyFile(std::string fn) { property_filename = fn; }
    void setProperties(std::vector<std::string> p) { propertyNames = p; }

    void setFlatMassFile(std::string fn) { flat_mass_filename = fn; }
    void setPairedMassFile(std::string fn) { paired_mass_filename = fn; }
    void setFlatVelFile(std::string fn) { flat_vel_filename = fn; }
    void setPairedVelFile(std::string fn) { paired_vel_filename = fn; }
    void setFlatTimeFile(std::string fn) { flat_time_filename = fn; }
    void setPairedTimeFile(std::string fn) { paired_time_filename = fn; }
    void setFlatXYZVelFile(std::string fn) { flat_xyzvel_filename = fn; }
    void setFlatXYZPosFile(std::string fn) { flat_xyzpos_filename = fn; }

    void setTGFile(std::string fn) { tg_filename = fn; }
    void setTFFile(std::string fn) { tf_filename = fn; }
    void setLatFile(std::string fn) { lat_filename = fn; }
    void setLonFile(std::string fn) { long_filename = fn; }
    void setT33File(std::string fn) { t33_filename = fn; }
    void setT0File(std::string fn) { t0_filename = fn; }
    void setUSGSLinkageFile(std::string fn) { climate_linkage_filename = fn; }
    //FIXME: similarity file - need csv format first

    std::vector<std::string> getPropertyNames(){ return propertyNames; }
    std::vector<std::vector<float>> getProperties(){ return properties; }

    void loadClusters(std::vector<int> cluster_labels);
    void loadLinkage();
    void createTree();
    void loadTimeSeries();
    void loadClimateTimeSeries();
    void loadFlatTrees();
    void loadFlatProgenitors();

public slots:
    void subCluster();
    void updateResolution(int r); //level = new dendrogram level (?)
    void showHalos(QString tag);
    void setDataType(QAction* type){ if (type->text() == "cosmology" || type->text() == "climate") dataType = type->text(); }

signals:
    void addButtons(int num);
    void addAxes(int num);
    void removeButtons(int num);
    void highlightTree(std::vector<clusterTree::node *> nodes);
    void highlightNode(QColor color, clusterTree::node* node);
    void setAxisRange(bool horiz, float min, float max);
    void addTick(float frac, int name);
    void clearTicks();
    void updateList(std::vector<unsigned int> tags);
    void showTags(std::vector<unsigned int> tags, std::vector<int> times);
    void setTimeSeries(std::vector<std::vector<Halo>> s);
    void setTimeSeries(std::vector<std::vector<climateNode>> s);
    void setPermutation(std::vector<int> p);
    void setClusters(std::vector<int> c);

private:
    //FIXME: consolidate.
    std::string mass_filename = "";
    std::string merger_filename = "";
    std::string tag_filename = "";
    std::string linkage_filename = "";
    std::string timestep_filename = "";
    std::string similarity_filename = "";
    std::string velocity_filename = "";
    std::string property_filename = "";
    std::string flat_mass_filename = "";
    std::string paired_mass_filename = "";
    std::string flat_vel_filename = "";
    std::string paired_vel_filename = "";
    std::string flat_time_filename = "";
    std::string paired_time_filename = "";
    std::string flat_xyzvel_filename = "";
    std::string flat_xyzpos_filename = "";
    std::vector<std::string> propertyNames;
    std::vector<std::vector<float>> properties;

    std::vector<unsigned int> tags;

    //climate files (temp: will make this modular)
    std::string tg_filename = "";
    std::string tf_filename = "";
    std::string lat_filename = "";
    std::string long_filename = "";
    std::string t33_filename = "";
    std::string t0_filename = "";
    std::string climate_linkage_filename = "";
    std::string climate_similarity_filename = ""; //FIXME (?)

    //vectors containing n_clusters arrays of n_timesteps boundary mass arrays
    //FIXME: this could be better
    std::vector<std::vector<Halo>> trees;
    std::vector<std::vector<Halo>> flatTrees;
    std::vector<std::vector<Halo>> flatProgs;

    std::vector<std::vector<climateNode>> climateSeries;

    std::vector<std::vector<std::vector<Halo>>> clusters;
    std::vector<std::vector<std::vector<climateNode>>> climate_clusters;

    std::vector<int> cluster_sizes;
    std::vector<int> cluster_order;
    std::vector<int> cluster_assignments;
    void updateMap(int color_index, int index, clusterTree::node* node);
    void sortTrees();
    float scalePairedTreeMass(float m, int idx);

    float min_temp; float max_temp;
};

#endif // DATAMANAGE_H
