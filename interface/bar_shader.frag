#version 330
in vec3 out_color;

out vec4 FragColor;

void main(void)
{
    FragColor = vec4(out_color.x/255., out_color.y/255., out_color.z/255., 1.0);
}
