#include "clusters.h"
#include <stdlib.h>
#include <algorithm>

float scale;
std::vector<float> alphas;
int n_single_trees;
int color_index;

Clusters::Clusters(dataManage *dataManager, QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent), dataManager(dataManager)
{
}

void Clusters::initializeGL()
{
    makeCurrent();
    scale = 0.25;
    alphas = {0.5, 0.8, 1.0, 1.0, 0.8, 0.5};
    n_single_trees = 0;
    show_clusters = true;
    braid = false;

    //openGL settings
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glCullFace(GL_FRONT);
    glColorMask(true, true, true, true);

    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    //data for shaders
    posVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    lowPosVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    alphaVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    vertNormVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    singlePosVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    singleColorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    singleAlphaVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    singleYVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    nodePosVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeColorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeSizeVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    nodeCenterVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    braidPosVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    braidColorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    //FIXME: keep both?
    yvalVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);

    //get initial set of clusters
    clusters = dataManager->getClusters();
    climate_clusters = dataManager->getClimateClusters();
    fillVBOs();

    //shader program for tree clusters
    groupProgram = GLShaderProgram::create(dataManager->path + "group_shader.vert",dataManager-> path + "group_shader.frag");
    if (groupProgram == NULL)
        close();

    singleProgram = GLShaderProgram::create(dataManager->path + "shader.vert", dataManager->path + "shader.frag");
    if (singleProgram == NULL)
        close();

    nodeProgram = GLShaderProgram::create(dataManager->path + "node.vert", dataManager->path + "node.frag");
    if (nodeProgram == NULL)
        close();

    braidProgram = GLShaderProgram::create(dataManager->path + "braid.vert", dataManager->path + "braid.frag");

    setShaderAttributes();

    paintGL();

    doneCurrent();
}

void Clusters::setShaderAttributes()
{
    GLenum err;

    groupProgram->setVertexAttribute("rgb", groupVAO, colorVBO, 4, GL_FLOAT, false);
    groupProgram->setVertexAttribute("in_position", groupVAO, posVBO, 2, GL_FLOAT, false);
    groupProgram->setVertexAttribute("alpha", groupVAO, alphaVBO, 1, GL_FLOAT, false);
    groupProgram->setVertexAttribute("low_position", groupVAO, lowPosVBO, 1, GL_FLOAT, false);
    groupProgram->setVertexAttribute("vertNorm", groupVAO, vertNormVBO, 2, GL_FLOAT, false);
    groupProgram->setVertexAttribute("y_value", groupVAO, yvalVBO, 1, GL_FLOAT, false);

    groupProgram->setUniform("y_range", y_range);
    groupProgram->setUniform("scale", scale);

    while ((err = glGetError()) != GL_NO_ERROR) {
        std::cerr << "OpenGL error in setShaderAttributes: " << err << std::endl;
    }
}

void Clusters::resizeGL(int w, int h)
{
    makeCurrent();
    glViewport(0, 0, w, h);
    width = w;
    height = h;
    paintGL();
    doneCurrent();
}

void Clusters::paintGL()
{
    makeCurrent();

    //emit this->addAxes();

    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    if (show_clusters == true){
        groupProgram->begin();
        int num = 0;
        if (dataManager->dataType == "climate") num = posArraySize;
        else if (dataManager->dataType == "cosmology") num = 6*201*cluster_sizes.size();
        groupVAO->drawArrays(GL_TRIANGLE_STRIP, num);
        groupProgram->end();
    }

    if (n_single_trees > 0){
        singleProgram->begin();
        if (full_trees == false) singleVAO->drawArrays(GL_TRIANGLE_STRIP, currentTrees.size()*100*2);
        else { singleVAO->drawArrays(GL_TRIANGLE_STRIP, full_tree_count); }
        singleProgram->end();
    }

    if (render_nodes == true){
        nodeProgram->begin();
        nodeVAO->drawArrays(GL_TRIANGLES, 12);
        nodeProgram->end();
    }

    if (braid == true){
        auto rep_trees = dataManager->getRepresentativeTrees();
        int n = rep_trees.size()*rep_trees[0].size()*5;

        braidProgram->begin();
        braidVAO->drawArrays(GL_TRIANGLE_STRIP, n);
        braidProgram->end();
    }

    doneCurrent();
}

Vec3f Clusters::getColor(float ratio)
{
    int idx;
    if (ratio < 0.9) idx = 0;
    //FIXME: stop hard-coding ratios
    else if (ratio > 0.9 && ratio <= 0.92) idx = 1;
    else if (ratio > 0.92 && ratio <= 0.95) idx = 2;
    else if (ratio > 0.95 && ratio <= 1.0) idx = 3;
    else if (ratio > 1.0 && ratio <= 1.05)  idx = 4;
    else if (ratio > 1.05 && ratio <= 1.09) idx = 5;
    else if (ratio > 1.09 && ratio <= 1.2) idx = 6;
    else if (ratio > 1.2 && ratio <= 1.4) idx = 7;
    else if (ratio > 1.4) idx = 8;

    Vec3f color = dataManager->heat_colors[idx];
    color.x = color.x/255.; color.y = color.y/255.; color.z = color.z/255.;
    //Vec3f color = (0.4, 0.4, 0.4);
    return color;
}

Vec3f Clusters::getValueColor(float temp){
    float min = dataManager->getMinTemp();
    float max = dataManager->getMaxTemp();

    int idx;
    float a = (temp - min)/(max - min);
    //float a = temp;
    if (a >= 0 && a <= 0.11) idx = 8;
    if (a > 0.11 && a <= 0.22) idx = 7;
    if (a > 0.22 && a <= 0.33) idx = 6;
    if (a > 0.33 && a <= 0.44) idx = 5;
    if (a > 0.44 && a <= 0.55) idx = 4;
    if (a > 0.55 && a <= 0.66) idx = 3;
    if (a > 0.66 && a <= 0.77) idx = 2;
    if (a > 0.77 && a <= 0.88) idx = 1;
    if (a > 0.88 && a <= 1) idx = 0;

    Vec3f color = dataManager->heat_colors[idx];
    color.x = color.x/255.; color.y = color.y/255.; color.z = color.z/255.;
    return color;
}

void Clusters::highlightMergers(std::vector<Vec2f> pos, std::vector<float> ratios)
{
}
void Clusters::fillVBOs()
{
    //create arrays for VBO data
    std::vector<Vec2f> posArray;
    std::vector<float> lowPosArray;
    std::vector<Vec2f> vertNormArray;
    std::vector<Vec4f> colorArray;
    std::vector<float> alphaArray;

    //for each cluster:
    clusters = dataManager->getClusters();
    climate_clusters = dataManager->getClimateClusters();

    int size = 0;
    int series_length;
    if (dataManager->dataType == "cosmology"){
        series_length = 100;
        size = clusters.size();
    }
    else if (dataManager->dataType == "climate"){
        series_length = 36; //FIXME
        size = climate_clusters.size();
    }

    for (int i = 0; i < size; i++){
        //render the largest cluster first
        //FIXME
        int idx = dataManager->getClusterOrder()[i];

        //loop over each timestep in this cluster 6x (we render each cluster in 6 bands)
        for (int band = 0; band < 6; band++){
            //loop over each timestep
            for (int j = 0; j < series_length; j++)
            {
                float x = 2*float(j)/(series_length) - 1; //FIXME (x = x position corresponding to this timestep)

                //FIXME: this sucks
                //get list of halos at this timestep
                std::vector<float> masses;
                std::vector<float> scaled_masses;

                std::vector<float> temps;
                float avg_temp = 0;

                if (dataManager->dataType == "cosmology"){
                    for (auto& t : clusters[idx]){ //for trees in this cluster:
                        masses.push_back(t[j].mass);
                        scaled_masses.push_back(t[j].scaled_mass);
                    }
                }
                else if (dataManager->dataType == "climate"){
                    for (auto& t : climate_clusters[idx]){ //for trees in this cluster:
                        temps.push_back(t[j].scaled_temp);
                        avg_temp += t[j].tg;
                    }
                    avg_temp = avg_temp/float(temps.size());
                }

                //sort masses
                std::sort(masses.begin(), masses.end());
                std::sort(scaled_masses.begin(), scaled_masses.end());
                std::sort(temps.begin(), temps.end());

                //stretch factor
                float fac = 0.0; //FIXME

                int low_mass_idx; dataManager->dataType == "cosmology" ? low_mass_idx = floor((masses.size()-1)*(band/6.)) : low_mass_idx = floor((temps.size()-1)*(band/6.));
                int high_mass_idx; dataManager->dataType == "cosmology" ?  high_mass_idx = floor((masses.size()-1)*((band+1)/6.)) : high_mass_idx = floor((temps.size()-1)*((band+1)/6.));

                float lower_mass, higher_mass, lower_temp, higher_temp;
                if (dataManager->dataType == "cosmology"){
                    lower_mass = scaled_masses[low_mass_idx];
                    higher_mass = scaled_masses[high_mass_idx];
                }
                else if (dataManager->dataType == "climate"){
                    lower_temp = temps[low_mass_idx];
                    higher_temp = temps[high_mass_idx];
                }

                float ratio;
                if (j == 0) ratio = 0;
                else {
                    float init_mass = 0;
                    float final_mass = 0;
                    if (dataManager->dataType == "cosmology"){
                        for (auto& t : clusters[idx]){
                            init_mass += t[j-1].mass;
                        }
                    }
                    else if (dataManager->dataType == "climate"){
                        for (auto& t : climate_clusters[idx]){
                            init_mass += t[j-1].tg;
                        }
                    }
                    for (auto& m : masses) final_mass += m;
                    ratio = fabs(final_mass/init_mass);
                }
                Vec3f color;
                if (color_scheme == "ratio") color = getColor(ratio);
                else if (color_scheme == "category") {
                    color = dataManager->color_set[i];
                    color = Vec3f(color.x/255., color.y/255., color.z/255.);
                }
                else if (color_scheme == "value"){
                    color = getValueColor(avg_temp);
                }

                float low; dataManager->dataType == "cosmology" ? low = lower_mass : low = lower_temp;
                float high; dataManager->dataType == "cosmology" ? high = higher_mass : high = higher_temp;
                if (band < 3) {
                    posArray.push_back(Vec2f(float(x), low));
                    posArray.push_back(Vec2f(float(x), high));
                }
                else if (band == 3){
                    posArray.push_back(Vec2f(float(x), low));
                    posArray.push_back(Vec2f(float(x), high + fac));
                }
                else{
                    posArray.push_back(Vec2f(float(x), low + fac));
                    posArray.push_back(Vec2f(float(x), high + fac));
                }

                //FIXME: using this?
                vertNormArray.push_back(Vec2f(0,-1));
                vertNormArray.push_back(Vec2f(0,1));

                colorArray.push_back(Vec4f(color, 0.8));
                colorArray.push_back(Vec4f(color, 0.8));

                alphaArray.push_back(alphas[band]);
                alphaArray.push_back(alphas[band]);

            }
            posArray.push_back(Vec2f(NAN, NAN));
            colorArray.push_back(NAN);
            alphaArray.push_back(0.0);
            vertNormArray.push_back(Vec2f(NAN,NAN));
        }
    }
    posVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_STATIC_DRAW);
    lowPosVBO->update(lowPosArray.size()*sizeof(GLfloat), &lowPosArray.front(), GL_STATIC_DRAW);
    colorVBO->update(colorArray.size()*sizeof(Vec4f), &colorArray.front(), GL_STATIC_DRAW);
    alphaVBO->update(alphaArray.size()*sizeof(GLfloat), &alphaArray.front(), GL_STATIC_DRAW);
    vertNormVBO->update(vertNormArray.size()*sizeof(Vec2f), &vertNormArray.front(), GL_STATIC_DRAW);

    posArraySize = posArray.size();

    cluster_sizes = dataManager->getClusterSizes();
    setYRange(0, cluster_sizes.size());
    std::vector<int> y_values;
    for (int i = 0; i < cluster_sizes.size(); i++) { y_values.push_back(cluster_sizes.size()-i-1); }
    setYValues(y_values);

    groupVAO = GLVertexArray::create();

    for (int i = 0; i < climate_clusters.size(); i++){
        std::cout << "cluster size: " << climate_clusters[i].size() << std::endl;
        std::cout << "temp range: " << std::endl;
        float max_temp = 0; float min_temp = 100;
        float avg_lat = 0; float avg_lon = 0;
        int count = 0;
        for (int j = 0; j < climate_clusters[i].size(); j++){
            avg_lat += climate_clusters[i][j][0].lat;
            avg_lon += climate_clusters[i][j][0].lon;
            count ++;
            for (int k = 0; k < climate_clusters[i][j].size(); k++){
                float temp = climate_clusters[i][j][k].tg;
                if (temp < min_temp) min_temp = temp;
                if (temp > max_temp) max_temp = temp;
            }
        }
        std::cout << min_temp << ", " << max_temp << std::endl;
        std::cout << "avg coords: " << std::endl;
        std::cout << avg_lon/float(count) << ", " << avg_lat/float(count) << std::endl;
    }
}

void Clusters::setYValues(std::vector<int> values){

    y_vals.clear();

    y_vals.resize(posArraySize); //FIXME: no hard-code
    for (int i = 0; i < values.size(); i++){
        for (int j = 0; j < posArraySize/cluster_sizes.size(); j++){ //FIXME: no hard-code
            y_vals[int(posArraySize/cluster_sizes.size())*i + j] = values[i];
        }
    }

    yvalVBO->update(y_vals.size()*sizeof(GLfloat), &y_vals.front(), GL_STATIC_DRAW);
}

void Clusters::setYRange(float min, float max){
    y_range = Vec2f(min, max);
}

void Clusters::showTrees(bool t)
{
}

void Clusters::setSingleShader()
{
    singleProgram->setVertexAttribute("color", singleVAO, singleColorVBO, 3, GL_FLOAT, false);
    singleProgram->setVertexAttribute("alpha", singleVAO, singleAlphaVBO, 1, GL_FLOAT, false);
    singleProgram->setVertexAttribute("in_position", singleVAO, singlePosVBO, 2, GL_FLOAT, false);
    singleProgram->setVertexAttribute("y_offset", singleVAO, singleYVBO, 1, GL_FLOAT, false);

    singleProgram->setUniform("y_range", y_range);
    singleProgram->setUniform("scale", scale);
}

void Clusters::spreadLayout(){
}

void Clusters::stackLayout(){
}

void Clusters::numLayout(){
}

void Clusters::showDiscarded(bool on){
}

void Clusters::showRandomTrees(int n){
    this->makeCurrent();
    auto ids = dataManager->getClusterAssignments();

    std::vector<int> counts;
    int count = 1;
    int i = 1;
    while (count != 0){
        count = std::count(ids.begin(), ids.end(), i);
        counts.push_back(count);
        i++;
    }
    for (int i = 0; i < counts.size(); i++) std::cout << i << std::endl;

    std::vector<std::vector<int>> indices;
    indices.resize(counts.size()+1);
    for (int j = 0; j < counts.size(); j++){
        int c = counts[j];
        if (random == true){
            if (c < n) for (int i = 0; i < c; i++) indices[j+1].push_back(i);
            else {
                srand(time(NULL));
                for (int i = 0; i < n; i++){ indices[j+1].push_back(rand() % c); }
            }
        }
    }
    for (int i = 0; i < indices.size(); i++){
        std::sort(indices[i].begin(), indices[i].end());
    }
    if (random == false) indices = dataManager->getRepresentativeIndices(n);

    std::vector<Vec2f> posArray;
    std::vector<Vec3f> colorArray;
    std::vector<float> alphaArray;
    std::vector<float> offsets;

    Vec3f mid_color = {0.3, 0.3, 0.3};

    std::vector<std::vector<dataManage::Halo>> trees;
    if (full_trees == true) trees = dataManager->getFlatTrees();
    else trees = dataManager->getTrees();

    currentTrees.clear();

    std::vector<int> counter; counter.resize(counts.size()); std::fill(counter.begin(), counter.end(), 0);
    std::vector<int> iterations; iterations.resize(counts.size()); std::fill(iterations.begin(), iterations.end(), 0);

    //FIXME! this is so bad.
    if (full_trees == false && random == true)
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] != 0){ //FIXME
            int idx = counter[ids[i]];
            if (iterations[ids[i]] == indices[ids[i]][idx]){
                currentTrees.push_back(i);
                for (int j = 0; j < trees[i].size(); j++){
                    colorArray.push_back(mid_color);
                    colorArray.push_back(mid_color);
                    float alpha;
                    alpha = 0.5;
                    alphaArray.push_back(alpha); alphaArray.push_back(alpha);
                    float x = 2*float(j)/100 - 1.;
                    posArray.push_back(Vec2f(float(x), trees[i][j].scaled_mass) + Vec2f(-delta/2., delta));
                    posArray.push_back(Vec2f(float(x), trees[i][j].scaled_mass) + Vec2f(delta/2., -delta));
                    offsets.push_back(ids[i]); offsets.push_back(ids[i]);
                }
                posArray.push_back(Vec2f(NAN, NAN));
                colorArray.push_back(Vec3f(NAN, NAN, NAN));
                alphaArray.push_back(NAN);
                offsets.push_back(NAN);
                counter[ids[i]]++;
            }
            else{
            }
            iterations[ids[i]]++;
        }
    }
    else if (full_trees == true || random == false){ //FIXME: update for multiple trees
        full_tree_count = 0;
        for (int i = 0; i < indices.size(); i++){
            for (int n = 0; n < indices[i].size(); n++){
                int idx = indices[i][n];
                std::vector<int> order = dataManager->getClusterOrder();
                int cluster_idx = std::distance(order.begin(), std::find(order.begin(), order.end(), ids[idx]-1)); //ordered cluster rendering ... CHECK IF THIS IS RIGHT
                currentTrees.push_back(idx);
                for (int j = 0; j < trees[idx].size(); j++){
                    full_tree_count++;
                    colorArray.push_back(mid_color);
                    colorArray.push_back(mid_color);
                    float alpha;
                    if (j < trees[idx].size() - 1
                            && ((j%2 == 0  && trees[idx][j+1].scaled_mass < -0.85)
                              || (j%2 == 1 && trees[idx][j].scaled_mass < -0.85))){
                        alpha = 0.2;
                    }
                    else alpha = 1.0;
                    alphaArray.push_back(alpha); alphaArray.push_back(alpha);
                    float x;
                    if (full_trees == false) x = 2*float(j)/trees[idx].size() - 1.;
                    else x = timestepToTimeIndex(trees[idx][j].time);
                    posArray.push_back(Vec2f(float(x), trees[idx][j].scaled_mass) + Vec2f(-delta/4., 2*delta));
                    posArray.push_back(Vec2f(float(x), trees[idx][j].scaled_mass) + Vec2f(delta/4., -2*delta));
                    offsets.push_back(cluster_idx+1); offsets.push_back(cluster_idx+1);
                    if (full_trees == true && j % 2 == 1) {
                        posArray.push_back(Vec2f(NAN, NAN));
                        offsets.push_back(NAN);
                        colorArray.push_back(Vec3f(NAN, NAN, NAN));
                        alphaArray.push_back(alpha);
                    }
                }
                posArray.push_back(Vec2f(NAN, NAN));
                offsets.push_back(NAN);
                colorArray.push_back(Vec3f(NAN, NAN, NAN));
                alphaArray.push_back(NAN);
            }
        }
    }

    full_tree_count = posArray.size();

    n_single_trees = 0;
    for (auto & c : counter) n_single_trees += c;
    if (n_single_trees == 0) for (auto & i : indices) n_single_trees += indices.size();

    singleColorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_DYNAMIC_DRAW);
    singleAlphaVBO->update(alphaArray.size()*sizeof(float), &alphaArray.front(), GL_DYNAMIC_DRAW);
    singlePosVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_DYNAMIC_DRAW);
    singleYVBO->update(offsets.size()*sizeof(float), &offsets.front(), GL_DYNAMIC_DRAW);
    singleVAO = GLVertexArray::create();

    setSingleShader();

    paintGL();
    update();
}

void Clusters::showRepresentativeTrees()
{
    //...
}

float Clusters::timestepToTimeIndex(int t){
    std::vector<int> timesteps = dataManager->getTimesteps();
    auto it = std::find(timesteps.begin(), timesteps.end(), t);
    int i = std::distance(it, timesteps.end());
    int j = 99 - i;
    return (2*float(j)/100.)-1.;
}

void Clusters::colorTrees(bool t)
{
    this->makeCurrent();
    auto assignments = dataManager->getClusterAssignments();
    auto colors = dataManager->color_set;

    std::vector<Vec3f> colorArray;
    for (auto & i : currentTrees){
        Vec3f color;
        if ( t == true) color = Vec3f(colors[assignments[i]].x/255., colors[assignments[i]].y/255., colors[assignments[i]].z/255.);
        else color = Vec3f(0.3, 0.3, 0.3);
        for (int j = 0; j < 100; j++) { colorArray.push_back(color); colorArray.push_back(color); }
        colorArray.push_back(Vec3f(NAN));
    }
    singleColorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_DYNAMIC_DRAW);

    paintGL();
    update();
}

void Clusters::stackTrees()
{
    this->makeCurrent();
    std::vector<Vec3f> yArray;
    for (auto & i : currentTrees){
        for (int j = 0; j < 100; j++) { yArray.push_back(1.0); yArray.push_back(1.0); }
        yArray.push_back(NAN);
    }
    singleYVBO->update(yArray.size()*sizeof(float), &yArray.front(), GL_DYNAMIC_DRAW);
    setYRange(0, 1);

    setSingleShader();
    paintGL();
    update();
}

void Clusters::showAllData(bool t)
{
    this->makeCurrent();
    show_clusters = false;
    std::vector<float> yArray; std::vector<Vec2f> posArray; std::vector<Vec3f> colorArray;

    currentTrees.clear();
    n_single_trees = 0;
    std::vector<std::vector<dataManage::Halo>> trees;
    trees = dataManager->getTrees();

    for (int i = 0; i < trees.size(); i++){
        currentTrees.push_back(i);
        n_single_trees++;
        for (int j = 0; j < trees[i].size(); j++){
            yArray.push_back(1.0); yArray.push_back(1.0);
            float x;
            if (full_trees == false) x = 2*float(j)/trees[i].size() - 1.;
            else x = 2*trees[i][j].time/499.-1.;
            posArray.push_back(Vec2f(float(x), trees[i][j].scaled_mass) + Vec2f(-delta/2., delta));
            posArray.push_back(Vec2f(float(x), trees[i][j].scaled_mass) + Vec2f(delta/2., -delta));
            colorArray.push_back(Vec3f(0.3, 0.3, 0.3));
            colorArray.push_back(Vec3f(0.3, 0.3, 0.3));

            if (full_trees == true && j % 2 == 1) {
                posArray.push_back(Vec2f(NAN, NAN));
                colorArray.push_back(Vec3f(NAN, NAN, NAN));
                yArray.push_back(1.0);
            }
        }
        if (full_trees == false){
            yArray.push_back(NAN);
            posArray.push_back(Vec2f(NAN, NAN));
            colorArray.push_back(Vec3f(NAN, NAN, NAN));
        }
    }
    if (t == true){
        singleYVBO->update(yArray.size()*sizeof(float), &yArray.front(), GL_DYNAMIC_DRAW);
        singlePosVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_DYNAMIC_DRAW);
        singleColorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_DYNAMIC_DRAW);
        setYRange(0, 1);
    }
    else {
        currentTrees.clear();
        singleYVBO->update(0, NULL, GL_DYNAMIC_DRAW);
        singlePosVBO->update(0, NULL, GL_DYNAMIC_DRAW);
        singleColorVBO->update(0, NULL, GL_DYNAMIC_DRAW);
    }

    if (full_trees == true) full_tree_count = posArray.size();
    singleVAO = GLVertexArray::create();

    setSingleShader();
    paintGL();
    update();
}

void Clusters::showClusters(bool t){
    this->makeCurrent();
    braid = !t;
    show_clusters = t;
    paintGL();
    update();
}

void Clusters::showTreesInGrid(bool t){
    this->makeCurrent();

    show_clusters = false;

    auto trees = dataManager->getTrees();
    auto c = dataManager->getClusterAssignments();
    int n_clusters = c[std::distance(c.begin(), std::max_element(c.begin(), c.end()))];
    int n_rows = 2; //FIXME
    int n_columns = ceil(n_clusters/n_rows);

    currentTrees.clear();
    std::vector<Vec2f> posArray;
    std::vector<float> yArray;
    std::vector<Vec3f> colorArray;

    for (int i = 0; i < c.size(); i++){
        currentTrees.push_back(i);
        int col = (c[i] - 1) % n_columns;
        int row = floor((c[i]-1)/n_columns);

        int max = 1; int min = -1; //GL range
        float xmin = float(col)/float(n_columns)*(max-min) + min; float xmax = xmin + float((max-min)/n_columns);
        float ymin = float(row)/float(n_rows)*(max-min) + min; float ymax = ymin + float((max-min)/n_rows);

        for (int j = 0; j < 100; j++){
            yArray.push_back(ymin+0.5); yArray.push_back(ymin+0.5); //FIXME
            float x = (xmax-xmin)*float(j)/100 + xmin;
            float y = trees[i][j].scaled_mass*0.5;
            posArray.push_back(Vec2f(float(x), y) + Vec2f(-delta/2., delta));
            posArray.push_back(Vec2f(float(x), y) + Vec2f(delta/2., -delta));
            colorArray.push_back(Vec3f(0.3, 0.3, 0.3));
            colorArray.push_back(Vec3f(0.3, 0.3, 0.3));
        }
        yArray.push_back(NAN);
        posArray.push_back(Vec2f(NAN, NAN));
        colorArray.push_back(Vec3f(NAN, NAN, NAN));
    }

    singleYVBO->update(yArray.size()*sizeof(float), &yArray.front(), GL_DYNAMIC_DRAW);
    singlePosVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_DYNAMIC_DRAW);
    singleColorVBO->update(colorArray.size()*sizeof(Vec3f), &colorArray.front(), GL_DYNAMIC_DRAW);
    setYRange(0,2);

    scale = 0.0;
    setSingleShader();
    paintGL();
    update();
}

void Clusters::showBraid(bool t){
    this->makeCurrent();

    show_clusters = false;
    braid = t;

    auto rep_trees = dataManager->getRepresentativeTrees();

    //goal: use gl_triangle_strips; overdraw each color w/ alpha=1

    //check 0-1 range situation

    std::vector<Vec2f> posArray;
    std::vector<Vec4f> colorArray;

    //get representative trees from dataManage
    int n = rep_trees[0].size();
    for (int i = 0; i < n; i++){
        float low_t = 2*float(i)/float(n)-1.0;
        float high_t = 2*float(i+1)/float(n)-1.0;
        std::vector<float> y0; std::vector<float> y1;
        std::vector<int> indices;
        for (int j = 0; j < rep_trees.size(); j++){
            y0.push_back(rep_trees[j][i].scaled_mass); y1.push_back(rep_trees[j][i+1].scaled_mass);
            indices.push_back(j);
        }
        while (y0.size() > 0){
            int j = std::distance(y0.begin(), std::max_element(y0.begin(), y0.end()));
            posArray.push_back(Vec2f(low_t, -1)); posArray.push_back(Vec2f(low_t, y0[j]));
            posArray.push_back(Vec2f(high_t, -1)); posArray.push_back(Vec2f(high_t, y1[j]));
            posArray.push_back(Vec2f(NAN, NAN));

            Vec3f color = dataManager->color_set[indices[j]];
            for (int k = 0; k < 4; k++){ colorArray.push_back(Vec4f(color.x/255., color.y/255., color.z/255., 1.0)); }
            colorArray.push_back(Vec4f(NAN, NAN, NAN, NAN));

            y0.erase(y0.begin()+j); y1.erase(y1.begin()+j); indices.erase(indices.begin()+j);
        }
    }

    braidPosVBO->update(posArray.size()*sizeof(Vec2f), &posArray.front(), GL_STATIC_DRAW);
    braidColorVBO->update(colorArray.size()*sizeof(Vec4f), &colorArray.front(), GL_STATIC_DRAW);
    braidVAO = GLVertexArray::create();

    braidProgram->setVertexAttribute("color", braidVAO, braidColorVBO, 4, GL_FLOAT, false);
    braidProgram->setVertexAttribute("position", braidVAO, braidPosVBO, 2, GL_FLOAT, false);

    paintGL();
    update();
}

void Clusters::setColorScheme(std::string c){
    makeCurrent();
    color_scheme = c;
    refresh();
}

void Clusters::refresh(){
    makeCurrent();
    initializeGL();
    update();
}
