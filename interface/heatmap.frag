#version 330
in vec3 out_color;
in float out_alpha;

out vec4 FragColor;

void main(void)
{
    FragColor = vec4(out_color.x, out_color.y, out_color.z, out_alpha);
}
