#include "heatmap.h"
#include <iostream>

Heatmap::Heatmap(QWidget *parent, const QGLFormat &format) :
    QGLWidget(parent)
{

}

void Heatmap::setTimeSeries(std::vector<std::vector<Halo> > s)
{
    this->makeCurrent();
    series = s;
}

void Heatmap::setPermutation(std::vector<int> p)
{
    this->makeCurrent();
    permutation = p;
    setVBOs();
    paintGL();
    update();
}

void Heatmap::setClusters(std::vector<int> c)
{
    this->makeCurrent();
    clusters = c;
    update();
}

void Heatmap::initializeGL()
{
    this->makeCurrent();
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glCullFace(GL_FRONT);
    glColorMask(true, true, true, true);

    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);


    highlight = 0;
    posVBO   = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    colorVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    alphaVBO = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    initShader();
}

void Heatmap::setVBOs()
{
    std::vector<Vec2f> positions; //NOTE: these positions will be scaled to (0, 1)
    std::vector<Vec3f> colors;
    std::vector<float> alphas;

    positions.clear();
    colors.clear();
    alphas.clear();

    float x = 0;
    float y = 0;

    float x_scale = series[0].size(); //FIXME: are time series lengths always going to be constant?
    float y_scale = series.size();

    for (auto& i : permutation)
    {
        std::vector<Halo> timeSeries = series[i];
        for (int j = 0; j < timeSeries.size(); j++)
        {
            float ratio;
            if (j == 0) ratio = 0.0;
            else (ratio = fabs(timeSeries[j].mass/timeSeries[j-1].mass));
            Vec3f color = getColor(ratio);
            colors.push_back(color); colors.push_back(color);
            positions.push_back(Vec2f(x/x_scale, 1.0 - y/y_scale));
            positions.push_back(Vec2f(x/x_scale, 1.0 - (y+1)/y_scale));
            if (highlight == 0) { alphas.push_back(1.0); alphas.push_back(1.0); }
            else if (clusters[i] == highlight) { alphas.push_back(1.0); alphas.push_back(1.0); }
            else { alphas.push_back(0.3); alphas.push_back(0.3); }
            x += 1;
        }
        //to complete the rightmost triangle:
        positions.push_back(Vec2f(x/x_scale, 1.0 - y/y_scale));
        positions.push_back(Vec2f(x/x_scale, 1.0 - (y+1)/y_scale));
        colors.push_back(Vec3f(0, 0, 0)); colors.push_back(Vec3f(0, 0, 0));
        alphas.push_back(1.0); alphas.push_back(1.0);

        //to force a new row:
        positions.push_back(Vec2f(NAN, NAN)); positions.push_back(Vec2f(NAN, NAN));
        colors.push_back(Vec3f(NAN, NAN, NAN)); colors.push_back(Vec3f(NAN, NAN, NAN));
        alphas.push_back(1.0); alphas.push_back(1.0);

        y += 1;
        x = 0;
    }

    dataSize = positions.size();


    posVBO->update(positions.size()*sizeof(Vec2f), &positions.front(), GL_STATIC_DRAW);
    colorVBO->update(colors.size()*sizeof(Vec3f), &colors.front(), GL_STATIC_DRAW);
    alphaVBO->update(alphas.size()*sizeof(float), &alphas.front(), GL_STATIC_DRAW);
    VAO = GLVertexArray::create();

    heatmapProgram->setVertexAttribute("position", VAO, posVBO, 2, GL_FLOAT, false);
    heatmapProgram->setVertexAttribute("color", VAO, colorVBO, 3, GL_FLOAT, false);
    heatmapProgram->setVertexAttribute("alpha", VAO, alphaVBO, 1, GL_FLOAT, false);
}

void Heatmap::initShader()
{
    heatmapProgram = GLShaderProgram::create(path + "heatmap.vert", path + "heatmap.frag");
    if (heatmapProgram == NULL)
        close();
}

void Heatmap::resizeGL(int w, int h)
{
    makeCurrent();
    glViewport(0, 0, w, h);
    paintGL();
    doneCurrent();
}

void Heatmap::paintGL()
{
    makeCurrent();
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    if (dataSize > 0){
        heatmapProgram->begin();
        std::cout << "drawing " << dataSize << " heatmap boxes" << std::endl;
        VAO->drawArrays(GL_TRIANGLE_STRIP, dataSize);
        heatmapProgram->end();
    }
    doneCurrent();
}

Vec3f Heatmap::getColor(float ratio)
{
    int idx;
    if (ratio < 0.9) idx = 0;
    else if (ratio > 0.9 && ratio <= 0.92) idx = 1;
    else if (ratio > 0.92 && ratio <= 0.95) idx = 2;
    else if (ratio > 0.95 && ratio <= 1.0) idx = 3;
    else if (ratio > 1.0 && ratio <= 1.05)  idx = 4;
    else if (ratio > 1.05 && ratio <= 1.09) idx = 5;
    else if (ratio > 1.09 && ratio <= 1.2) idx = 6;
    else if (ratio > 1.2 && ratio <= 1.4) idx = 7;
    else if (ratio > 1.4) idx = 8;

    Vec3f color = colors[idx];
    color.x = color.x/255.; color.y = color.y/255.; color.z = color.z/255.;
    return color;
}

void Heatmap::highlightTrees(int idx)
{
    this->makeCurrent();

    std::cout << "highlighting for index: " << idx << std::endl;

    //look up which indices (offset by permutation) correspond to this cluster number
    //(set tree highlighting to on; refill VBOs)
    highlight = idx;
    setVBOs();
    paintGL();
    update();
    doneCurrent();
}
