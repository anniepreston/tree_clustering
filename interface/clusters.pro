#-------------------------------------------------
#
# Project created by QtCreator 2016-05-26T23:42:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets opengl

CONFIG += c++11

TARGET = clusters
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    clusterTree.cpp \
    dataManage.cpp \
    GLVertexArray.cpp \
    GLArrayBuffer.cpp \
    GLShaderProgram.cpp \
    GLTexture.cpp \
    Map.cpp \
    TreeAnimation.cpp \
    brackets.cpp \
    axis.cpp \
    GLFramebuffer.cpp \
    heatmap.cpp \
    scatterplot.cpp \
    clusters.cpp \
    barplot.cpp \
    pieplot.cpp \
    phaseplot.cpp

HEADERS  += mainwindow.h \
    clusterTree.h \
    dataManage.h \
    GLShaderProgram.h \
    GLVertexArray.h \
    GLArrayBuffer.h \
    GLConfig.h \
    Vec.h \
    Mat.h \
    MathUtil.h \
    GLTexture.h \
    Map.h \
    TreeAnimation.h \
    brackets.h \
    axis.h \
    GLFramebuffer.h \
    heatmap.h \
    scatterplot.h \
    clusters.h \
    barplot.h \
    pieplot.h \
    phaseplot.h

OTHER_FILES += shader.frag \
               shader.vert

FORMS    += mainwindow.ui

INCLUDEPATH += /opt/local/include/ \

DISTFILES += \
    /Users/annie/repos/tree_clustering/interface/shader.frag \
    /Users/annie/repos/tree_clustering/interface/shader.vert \
    group_shader.vert \
    group_shader.frag \
    map.frag \
    map.vert \
    node.vert \
    node.frag \
    legend.png \
    heatmap.vert \
    heatmap.frag \
    bar_shader.vert \
    bar_shader.frag \
    scatter_shader.vert \
    scatter_shader.frag \
    braid.vert \
    braid.frag \
    pie_shader.vert \
    pie_shader.frag \
    phase_shader.vert \
    phase_shader.frag

# mac setup
macx: Data.path = Contents/MacOS
macx: QMAKE_BUNDLE_DATA += Data

RESOURCES += \
    icons.qrc

LIBS += -L/usr/local/lib/ \

LIBS += -framework GLUT
