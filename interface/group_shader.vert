#version 330
in vec4  rgb;
in vec2  vertNorm;
in vec2  in_position;
in float alpha;
in float low_position;

in float y_value;

uniform vec2  y_range;
uniform float scale;

out vec4 out_color;
out float dist;
out float out_alpha;

void main(void)
{
    out_color = rgb;
    dist = in_position.y - low_position;
    out_alpha = 1.0;

    //"standard deviation" bands:
    float y = 2.0*(y_value-y_range.x)/(y_range.y-y_range.x) - 1.0;
    float x = in_position.x;
    float test_scale = 1.0/(y_range.y);
    float delta_y = test_scale*(0.9*in_position.y+1.0);
    vec4 scaled_pos = vec4(x, y + delta_y, 0.0, 1.0);

    //extend vertically:
    vec4 delta = vec4(vertNorm*0.01, 0, 0);

    gl_Position = scaled_pos;// + delta;
}
