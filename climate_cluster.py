import numpy as np
import random
import csv
from netCDF4 import Dataset

from fastdtw import fastdtw                         # fast DTW algorithm
import fastcluster                                  # third party package - better documentation
from scipy.spatial.distance import euclidean        # euclidean dist for fast DTW

def main():
    global query_name
    query_name = 'climate_data/t_744_36'
    global input_file
    input_file = 'interface/climate_data/RegCM3_Monthly_psw_EH5.ncml.nc'
    global n
    n = 500 #number of time series (i.e. number of spatial coords to sample(
    global sample_length
    sample_length = 36 #number of months in a time series

    # time units = days since 1900-01-01;
    # min = 24851 (corresponds to 16-Jan-1968)
    global start_time
    start_time = 744
    global cluster_by
    cluster_by = 'TG'
    global linkage_method
    linkage_method = 'average'

    # foliage temp (TF), ground temp (TG), days w/ T < 0C (T0), days w/ T > 33C (T33),
    # latitude (lat), longitude (long)

    data = readfile()
    sim = similarity(data)
    linkage(sim)

def readfile():
    rootgrp = Dataset(input_file, 'r', format='NETCDF4')
    
    x = rootgrp.variables["x"]
    y = rootgrp.variables["y"]
    lat = rootgrp.variables["lat"]
    long = rootgrp.variables["lon"]
    time = rootgrp.variables["time"]
    TF = rootgrp.variables["TF"]
    TG = rootgrp.variables["TG"]
    T0 = rootgrp.variables["T0"]
    T33 = rootgrp.variables["T33"]

    var = rootgrp.variables[cluster_by]

    x_vals = []
    y_vals = []

    # get n (x, y) pairs
    for i in range(n):
        x_vals.append(random.randint(1, len(x)-1))
        y_vals.append(random.randint(1, len(y)-1))

    lat_array = []
    long_array = []
    TF_array = []
    TG_array = []
    T0_array = []
    T33_array = []
    var_array = []

    for i in range(n):
        TF_row = []
        TG_row = []
        T0_row = []
        T33_row = []
        var_row = []
        lat_row = []
        long_row = []
        lat_row.append(lat[y_vals[i]][x_vals[i]])
        long_row.append(long[y_vals[i]][x_vals[i]])
        for j in range(sample_length):
            t = start_time + j
            TF_row.append(TF[t][y_vals[i]][x_vals[i]])
            TG_row.append(TG[t][y_vals[i]][x_vals[i]])
            T0_row.append(T0[t][y_vals[i]][x_vals[i]])
            T33_row.append(T33[t][y_vals[i]][x_vals[i]])
            var_row.append(var[t][y_vals[i]][x_vals[i]])
        lat_array.append(lat_row)
        long_array.append(long_row)
        TF_array.append(TF_row)
        TG_array.append(TG_row)
        T0_array.append(T0_row)
        T33_array.append(T33_row)
        var_array.append(var_row)

    #write data to files
    csvwrite(lat_array, query_name + '_lat.csv')
    csvwrite(long_array, query_name + '_long.csv')
    csvwrite(TF_array, query_name + '_TF.csv')
    csvwrite(TG_array, query_name + '_TG.csv')
    csvwrite(T0_array, query_name + '_T0.csv')
    csvwrite(T33_array, query_name + '_T33.csv')
  
    #return data for clustering
    return var_array

def csvwrite(data, csvfilename):
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in data:
            writer.writerow(item)

def similarity(data):
    sim_matrix = np.zeros([n, n])
    for i in range(n):
        for j in range(n-i):
            if (i+j)%10 == 0: print(i+j)
            similarity, path = fastdtw(data[i], data[i+j], dist=euclidean)

            sim_matrix[i][j+i] = similarity
            sim_matrix[j+i][i] = similarity
    np.save(query_name + '_sim', sim_matrix)
    return sim_matrix

def linkage(sim_matrix):
    data = sim_matrix
    connectivity = np.array(data)

    similarity_vectors = []
    for i in range(len(data)):
        for j in range(len(data)-i-1):
            similarity_vectors.append(data[i][i+j+1])
    sim_vectors = np.array(similarity_vectors)

    linkage = fastcluster.linkage(sim_vectors, method=linkage_method)

    csvoutfile = query_name + '_linkage_' + linkage_method + '.csv'
    csvwrite(linkage, csvoutfile)

main()
