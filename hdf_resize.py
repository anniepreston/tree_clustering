import numpy as np
import h5py

def main():
    input_file = '/Users/annie/Desktop/plank_p10.0.hdf5'
    
    print('reading')
    with h5py.File(input_file, 'r+') as hf:
        data = hf.get('haloTrees/timestep')
        del hf[']
