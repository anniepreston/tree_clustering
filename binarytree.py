import numpy as np

class Node(object):
    #"""
    #Tree node: left and right child + data which can be any object
    #"""
    def __init__(self, data):
        #"""
        #Node Constructor
        #@param data node data object
        #"""
        self.left = None
        self.right = None
        self.data = data

    def __repr__(self):
        return "Node With Data: %d" % self.data
    
    def lookup(self, data, parent=None): #THIS IS NO GOOD!
        #"""
        #Lookup node containing data
        
        #@param data node data object to look up
        #@param parent node's parent
        #   @returns node and node's parent if found or None, None
        #   """
        if data < self.data:
            if data > self.left.data or self.left == None:
                return self.right.lookup(data, self)
            else:
                return self.left.lookup(data, self)
        else:
            return self, parent

    def get_nodes_at_level(self, k):
        nodes = []
        nodes.append(self)
        i = 0
        while i < k:
            old_nodes = nodes
            nodes = []
            for item in old_nodes:
                if item.left != None:
                    nodes.append(item.left)
                if item.right != None:
                    nodes.append(item.right)
            i += 1
        return nodes

    def insert(self, data, parent, level):
        #******
        # Insert pair of data nodes below parent;
        # extend all leaf nodes that now exist at this level (to the right?)
        #******
        possible_leaves = self.get_nodes_at_level(level)
        #print("possible leaves", possible_leaves)
        for each in possible_leaves:
            if each.data == parent:
                each.right = Node(max(data))
                each.left  = Node(min(data))
            if each.data != parent and each.right == None and each.left == None:
                each.right = Node(each.data)

    def children_count(self):
        #    """
        #Returns the number of children for a given node

    #@returns number of children: 0, 1, 2
    #   """
        count = 0
        if self.left:
            count += 1
        if self.right:
            count += 1
        return count

    def descendant_count(self):
        #   """
        #Counts all descendant nodes
        #"""
        count = 0
        if self.left:
            count += 1 + self.left.descendant_count()
        if self.right:
            count += 1 + self.right.descendant_count()
        return count

    def descendant_indices(self, index_list):
        ###
        #gets indices of all descendants
        ###
        if self.left and self.right:
            index_list.append(self.left.data)
            index_list.append(self.right.data)
            index_list.append(self.left.descendant_indices(index_list))
            index_list.append(self.right.descendant_indices(index_list))
        if self.right and not self.left:
            index_list.append(self.right.descendant_indices(index_list))

    def descendant_indices_at_level(self, k, n):
        ###
        # gets indices of all descendants
        ###
        nodes = self.get_nodes_at_level(k)
        cluster_assignments = np.zeros(n+1)
        
        cluster_label = 1 # label 0 saved for fuckups
        for node in nodes:
            index_list = []
            indices = []
            if node.left:
                indices.append(node.left.descendant_indices(index_list))
            if node.right:
                indices.append(node.right.descendant_indices(index_list))
            return_list = [int(item) for item in index_list if item != None and item <= n]
            for item in return_list:
                cluster_assignments[item] = cluster_label
            cluster_label += 1
        return cluster_assignments

    def delete(self, data):
        #"""
        #Delete node containing data
        #@param data node's content to delete
        #"""
        node, parent = self.lookup(data)
        if node:
            children_count = node.children_count()
            if children_count == 0:
                # If node has no children then remove it
                if parent.left is node:
                    parent.left = None
                else:
                    parent.right = None
                del node
            elif children_count == 1:
                if node.left:
                    child = node.left
                else:
                    child = node.right
                if parent:
                    if parent.left is node:
                        parent.left = child
                    else:
                        parent.right = child
                del node
            else:
                parent = node
                successor = node.right
                while successor.left:
                    parent = successor
                    successor = successor.left
                node.data = successor.data
                if parent.left == successor:
                    parent.left = successor.right
                else:
                    parent.right = successor.right
    def inorder_print(self):
        if self.left:
            self.left.inorder_print()
        print(self.data)
        if self.right:
            self.right.inorder_print()

    def print_each_level(self):
        # Start off with root node
        thislevel = [self]

        # While there is another level
        while thislevel:
            nextlevel = list()
            #Print all the nodes in the current level, and store the next level in a list
            for node in thislevel:
                print(node.data)
                if node.left: nextlevel.append(node.left)
                if node.right: nextlevel.append(node.right)
            print
            thislevel = nextlevel

    def tree_data(self):
        # """
        #Generator to get the tree nodes data
        #"""
        # we use a stack to traverse the tree in a non-recursive way
        stack = []
        node = self
        while stack or node: 
            if node:
                stack.append(node)
                node = node.left
            else: # we are returning so we pop the node and we yield it
                node = stack.pop()
                yield node.data
                node = node.right