import numpy as np
import sys
import binarytree as bt
np.set_printoptions(threshold=np.nan)
import h5py                                         # file reading
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
import pickle
import random
import math

#imports particular to clustering
from scipy.spatial.distance import euclidean        # euclidean dist for fast DTW
from fastdtw import fastdtw                         # fast DTW algorithm
from sklearn.cluster import AgglomerativeClustering # scipy package
import fastcluster                                  # third party package - better documentation
import csv

#################
# DO THINGS HERE.

def main():
    #for HACC:
    
    input_path = '/Users/annie/Desktop/merger_trees_e1/'

    mass_file = 'e1_mass'
    merger_file = 'e1_mergers'
    timestep_file = 'e1_times'
    velocity_file = 'e1_velocity'
    halo_tag_file = 'e1_tags'
    linkage_file = 'e1_linkage_complete'
    sim_file = 'e1_sim'
    flatmassfile = 'e1_flat_mass'
    flattimefile = 'e1_flat_time'
    flatvelfile = 'e1_flat_vel'
    flatxyzvelfile = 'e1_flat_xyzvel'
    flatxyzposfile = 'e1_flat_xyzpos'
    
    pairedmassfile = 'e1_paired_mass'
    pairedtimefile = 'e1_paired_time'
    pairedvelfile = 'e1_paired_vel'

    DI, ms, NI, tags, times, vels, xyzvels, xyzpos = readfiles(input_path, 1.0e+14, 1.0e+15)

    #DI, ms, NI, tags, times, vels = readfiles(input_path, 1.0e+13, 1.0e+14)
    #getFlattenedTrees(DI, ms, vels, times, xyzvels, xyzpos, flatmassfile, flattimefile, flatvelfile, flatxyzvelfile, flatxyzposfile)
    getPairedTrees(DI, ms, vels, times, pairedmassfile, pairedtimefile, pairedvelfile)
    

    """
    getMainMasses(DI, ms, NI, tags, times, vels, mass_file, halo_tag_file, timestep_file, velocity_file)
    getMergerRatios(DI, ms, NI, merger_file)

    mass_npy_file = mass_file + '_normed.npy'
    vel_npy_file = velocity_file + '_normed.npy'
    sim_matrix = similarity(vel_npy_file, sim_file)
    links = linkage(sim_file + '.npy', linkage_file)

    """
    #sim_matrix = similarity(mass_npy_file, sim_file)
    #links = linkage(sim_file + '.npy', linkage_file)

    """
    #for Millennium:
    input_file = 'millennium/millennium_e.csv'
    mass_file = 'mill_e_mass_raw_NEW'
    timestep_file = 'mill_e_time_NEW'
    merger_file = 'mill_e_mergers_NEW'
    property_file = 'mill_e_properties_NEW'
    sim_file = 'mill_e_sim_NEW'
    linkage_file = 'mill_e_linkage_NEW'
    
    readMillFile(input_file, mass_file, timestep_file, merger_file, property_file)

    mass_npy_file = mass_file + '.npy'
    sim_matrix = similarity(mass_npy_file, sim_file)
    links = linkage(sim_file + '.npy', linkage_file)
    """

    example_pairing('/Users/annie/repos/tree_clustering/interface/data/mass_comparison/normalized/complete/d0/d0_mass.npy', '/Users/annie/repos/tree_clustering/interface/data/mass_comparison/normalized/complete/d0/d0_sim.npy')

def readfiles(path, min_mass = 0, max_mass = 10e17):
    print('reading files for halos with mass between ', min_mass, ' and ', max_mass)
    
    #arrays of arrays holding tree data (each sub-array is one tree):
    rawDIList = []      # descendent index
    rawTagList = []     # halotag
    rawMassList = []    # mass
    rawVelList = []
    rawNIList = []      # node index
    rawTList = []       # timestep
    rawXYZVelList = []
    rawXYZPosList = []
    
    #open HDF5 file(s):
    filenames = [f for f in listdir(path) if isfile(join(path, f))]
    for filename in filenames:
        if filename[-4:] == 'hdf5':
            print("filename: ", filename)
            with h5py.File(path + filename, 'r') as hf:
                
                #get all the relevant data:
                
                if 'descIndices' in dir():
                    data = hf.get('haloTrees/descendentIndex')
                    descIndices = np.concatenate((descIndices, np.array(data)))
                    print("number of trees in file: ", data.size)
                
                    data = hf.get('haloTrees/haloTag')
                    haloTags = np.concatenate((haloTags, np.array(data)))
                    
                    data = hf.get('haloTrees/nodeIndex')
                    nodeIndices = np.concatenate((nodeIndices, np.array(data)))
                    
                    data = hf.get('haloTrees/timestep')
                    timesteps = np.concatenate((timesteps, np.array(data)))
                    
                    data = hf.get('haloTrees/velocity')
                    velocity = np.concatenate((velocity, np.array(data)))
                    
                    data = hf.get('haloTrees/nodeMass')
                    mass = np.concatenate((mass, np.array(data)))
                
                    data = hf.get('haloTrees/position')
                    position = np.concatenate((position, np.array(data)))
                
                else:
                    data = hf.get('haloTrees/descendentIndex')
                    descIndices = np.array(data)
                    print("number of trees in file: ", data.size)

                    data = hf.get('haloTrees/haloTag')
                    haloTags = np.array(data)

                    data = hf.get('haloTrees/nodeIndex')
                    nodeIndices = np.array(data)
            
                    data = hf.get('haloTrees/timestep')
                    timesteps = np.array(data)
                    
                    data = hf.get('haloTrees/velocity')
                    velocity = np.array(data)
                
                    data = hf.get('haloTrees/nodeMass')
                    mass = np.array(data)

                    data = hf.get('haloTrees/position')
                    position = np.array(data)

    #sort into arrays per tree:
    j = -1
    rootmasses = [] #final node masses, for binning
    
    for i in range(descIndices.size):
        if descIndices[i] == -1:  #start of a tree
            j += 1
            
            newDIList = []
            newTagList = []
            newMassList = []
            newVelList = []
            newNIList = []
            newTList = []
            newXYZVelList = []
            newXYZPosList = []
            
            newDIList.append(descIndices[i])
            newTagList.append(haloTags[i])
            newMassList.append(mass[i])
            newNIList.append(nodeIndices[i])
            newVelList.append(np.linalg.norm(velocity[i]))
            newTList.append(timesteps[i])
            newXYZVelList.append(velocity[i])
            newXYZPosList.append(position[i])
            
            rawDIList.append(newDIList)
            rawTagList.append(newTagList)
            rawMassList.append(newMassList)
            rawNIList.append(newNIList)
            rawVelList.append(newVelList)
            rawTList.append(newTList)
            rawXYZVelList.append(newXYZVelList)
            rawXYZPosList.append(newXYZPosList)
        
            rootmasses.append(mass[i])
        
        if descIndices[i] != -1: #non-start of a tree
            rawDIList[j].append(descIndices[i])
            rawTagList[j].append(haloTags[i])
            rawMassList[j].append(mass[i])
            rawNIList[j].append(nodeIndices[i])
            rawTList[j].append(timesteps[i])
            rawVelList[j].append(np.linalg.norm(velocity[i]))
            rawXYZVelList[j].append(velocity[i])
            rawXYZPosList[j].append(position[i])

    max_size = 0
    for i in range(j):
        if len(rawDIList[i]) > max_size:
            max_size = len(rawDIList[i])

    binnedDIList = []
    binnedMassList = []
    binnedNIList = []
    binnedTagList = []
    binnedTList = []
    binnedVelList = []
    binnedXYZVelList = []
    binnedXYZPosList = []

    count = 0
    for i in range(j):
        if rootmasses[i] > min_mass and rootmasses[i] < max_mass:
            count += 1
            binnedDIList.append(rawDIList[i])
            binnedMassList.append(rawMassList[i])
            binnedNIList.append(rawNIList[i])
            binnedTagList.append(rawTagList[i])
            binnedTList.append(rawTList[i])
            binnedVelList.append(rawVelList[i])
            binnedXYZVelList.append(rawXYZVelList[i])
            binnedXYZPosList.append(rawXYZPosList[i])

    print('...')
    print(count, ' trees found in mass bin')

    return binnedDIList, binnedMassList, binnedNIList, binnedTagList, binnedTList, binnedVelList, binnedXYZVelList, binnedXYZPosList

def getFlattenedTrees(DI, mass, vel, time, xyzvels, xyzpos, flatmassfile, flattimefile, flatvelfile, flatxyzvelfile, flatxyzposfile):
    flatMasses = []
    flatTimes = []
    flatVels = []
    flatXYZVel = []
    flatXYZPos = []
    for j in range(len(DI)):
        row = DI[j]
        mass_row = mass[j]
        time_row = time[j]
        vel_row = vel[j]
        xyzvel_row = xyzvels[j]
        xyzpos_row = xyzpos[j]
        treeMass = []
        treeTime = []
        treeVel = []
        treeXYZVel = []
        treeXYZPos = []
        
        for i in range(1, len(row)):
            treeMass.append(mass_row[row[i]])
            treeMass.append(mass_row[i])
            treeVel.append(vel_row[row[i]])
            treeVel.append(vel_row[i])
            treeTime.append(time_row[row[i]])
            treeTime.append(time_row[i])
            treeXYZVel.append(xyzvel_row[i])
            treeXYZPos.append(xyzpos_row[i])
        flatMasses.append(treeMass)
        flatTimes.append(treeTime)
        flatVels.append(treeVel)
        flatXYZVel.append(treeXYZVel)
        flatXYZPos.append(treeXYZPos)

    csvfilename = flatmassfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mass:
            writer.writerow(item)

    csvfilename = flattimefile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in time:
            writer.writerow(item)

    csvfilename = flatvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in vel:
            writer.writerow(item)

    csvfilename = flatxyzvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in xyzvels:
            writer.writerow(item)

    csvfilename = flatxyzposfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in xyzpos:
            writer.writerow(item)

def getPairedTrees(DI, mass, vel, time, pairedmassfile, pairedtimefile, pairedvelfile):
    pairedMasses = []
    pairedTimes = []
    pairedVels = []
    for j in range(len(DI)):
        row = DI[j]
        mass_row = mass[j]
        time_row = time[j]
        vel_row = vel[j]
        treeMass = []
        treeTime = []
        treeVel = []
        
        for i in range(1, len(row)):
            treeMass.append(mass_row[row[i]])
            treeMass.append(mass_row[i])
            treeVel.append(vel_row[row[i]])
            treeVel.append(vel_row[i])
            treeTime.append(time_row[row[i]])
            treeTime.append(time_row[i])
        pairedMasses.append(treeMass)
        pairedTimes.append(treeTime)
        pairedVels.append(treeVel)

    for i in range(len(pairedMasses[0])):
        print(pairedMasses[0][i], ", ", pairedTimes[0][i])


    csvfilename = pairedmassfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedMasses:
            writer.writerow(item)

    csvfilename = pairedtimefile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedTimes:
            writer.writerow(item)

    csvfilename = pairedvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedVels:
            writer.writerow(item)

#read millennium-derived file and save scaled mass lists
#for now: assume these are already binned
#(NOTE: this bypasses a step that the HDF HACC data must go through!)
def readMillFile(infile, timeseriesfile, timestepfile, mergerfile, propertyfile, minlength=10):
    
    massLists = []
    timeLists = []
    mergerLists = []
    propLists = []
    rawData = []
    
    with open(infile, 'r') as csvfile:
        millReader = csv.reader(csvfile, delimiter=',')
        for row in millReader:
            insert_row = []
            for item in row:
                insert_row.append(float(item))
            rawData.append(insert_row)
    row = []
    timerow = []
    mergerrow = [] #to store merger ratios
    proprow = [] #to store halo/galaxy properties for final timestep
    for i in range(len(rawData)):
        if i == 0:
            mergerrow.append(0.0)
            row.append(rawData[i][1])
            proprow.append(rawData[i][2:])
            propLists.append(proprow)
            proprow = []
            timerow.append(rawData[i][0])
        if rawData[i][0] < rawData[i-1][0] and i != 0:
            row.append(rawData[i][1])
            timerow.append(rawData[i][0])
            if rawData[i][1] != 0:
                mergerrow.append(float(rawData[i-1][1]/rawData[i][1]))
            else: mergerrow.append(0)
        if rawData[i][0] > rawData[i-1][0] and i != 0:
            proprow.append(rawData[i][2:])
            if len(row) > minlength:
                propLists.append(proprow)
                massLists.append(row)
                timeLists.append(timerow)
                mergerLists.append(mergerrow)
            mergerrow = []
            proprow = []
            timerow = []
            row = []
            row.append(rawData[i][1])
            timerow.append(rawData[i][0])
            mergerrow.append(0.0)
    if len(row) > minlength:
        massLists.append(row)
        mergerLists.append(mergerrow)
        timeLists.append(timerow)

    #create uniform lengths
    for i in range(len(massLists)):
        while len(massLists[i]) < 100:
            massLists[i].append(-99)
            mergerLists[i].append(0.0)
            timeLists[i].append(-99)

    #write unnormalized data to CSV files for visualization program
    csvfilename = timeseriesfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in massLists:
            writer.writerow(item)

    csvfilename = timestepfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in timeLists:
            writer.writerow(item)

    csvfilename = mergerfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergerLists:
            writer.writerow(item)
                                 
    csvfilename = propertyfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in propLists:
            writer.writerow(item)
                 
    #normalize mass data
    for i in range(len(massLists)):
        max_mass = max(massLists[i])
        if max_mass > 0:
            massLists[i] = [float(val)/max_mass for val in massLists[i]]
            print("normalized: ", massLists[i])

    #write to npy file for similarity function
    np.save(timeseriesfile, massLists)

########################

# obtain main mass representation of the data
# (for HACC data)

#utility function:
def findMainMass(descIndices, masses, nodeIndices, tags, times, vels, node):
    candidate_masses = []
    candidate_nodes = []
    candidate_tags = []
    candidate_times = []
    candidate_vels = []
    desc_found = 0
    for i in range(len(descIndices)):
        if descIndices[i] == node:
            desc_found = 1
            candidate_masses.append(masses[i])
            candidate_nodes.append(nodeIndices[i])
            candidate_tags.append(tags[i])
            candidate_times.append(times[i])
            candidate_vels.append(vels[i])

    if desc_found == 1:
        maxMass = max(candidate_masses)
        maxIndex = candidate_masses.index(maxMass)
        return candidate_nodes[maxIndex], candidate_masses[maxIndex], candidate_tags[maxIndex], candidate_times[maxIndex], candidate_vels[maxIndex]
    if desc_found == 0:
        return -99, -99, -99, -99, -99

def getMainMasses(dI, ms, nI, in_tags, in_times, in_vels, outfile, tagfile, timefile, velfile):
    print('getting main masses...')
    mainMassLists = []
    tagLists = []
    timeLists = []
    velLists = []
    j = 0
    normalize = 0
    for i in range(len(dI)):
        if len(dI[i]) > 1:     # if the tree is more than just a single halo
            newMassList = []
            newTagList = []
            newVelList = []
            newTimeList = []
            mainMassLists.append(newMassList)
            tagLists.append(newTagList)
            timeLists.append(newTimeList)
            velLists.append(newVelList)
            
            node = nI[i][0]
            descIndices = [num for num in dI[i] if num != -99]
            nodeIndices = [num for num in nI[i] if num != -99]
            masses = [num for num in ms[i] if num != -99]
            tags = [num for num in in_tags[i] if num != -99]
            times = [num for num in in_times[i] if num != -99]
            vels = [num for num in in_vels[i] if num != -99]
            
            while node != -99:
                node, mass, tag, time, vel = findMainMass(descIndices, masses, nodeIndices, tags, times, vels, node)
                if node != -99:
                    mainMassLists[j].append(mass)
                    tagLists[j].append(tag)
                    timeLists[j].append(time)
                    velLists[j].append(vel)
            #normalize?
            if normalize == 1:
                max_mass = max(mainMassLists[j])
                if max_mass > 0:
                    mainMassLists[j] = [float(num)/max_mass for num in mainMassLists[j]]
            #uniform length: (FIXME: is this needed?)
            while len(mainMassLists[j]) < 100:
                mainMassLists[j].append(-99)
            j += 1

    np.save(outfile, mainMassLists)
    np.save(tagfile, tagLists)
    np.save(timefile, timeLists)
    np.save(velfile, velLists)

    csvoutfile = outfile + '.csv'
    csvtagfile = tagfile + '.csv'
    csvtimefile = timefile + '.csv'
    csvvelfile = velfile + '.csv'
    
    with open(csvoutfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mainMassLists:
                writer.writerow(item)
    with open(csvtagfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in tagLists:
            writer.writerow(item)
    with open(csvtimefile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in timeLists:
            writer.writerow(item)
    with open(csvvelfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in velLists:
            writer.writerow(item)

    #normalize mass data
    for i in range(len(mainMassLists)):
        max_mass = max(mainMassLists[i])
        if max_mass > 0:
            mainMassLists[i] = [float(val)/max_mass for val in mainMassLists[i]]

    #normalize vel data
    for i in range(len(velLists)):
        max_vel = max(velLists[i]);
        if max_vel > 0:
            velLists[i] = [float(val)/max_vel for val in velLists[i]]

    #write to npy file for similarity function
    np.save(outfile + '_normed', mainMassLists)
    np.save(velfile + '_normed', velLists)

#########################

# obtain merger ratio representation of data
# (for HACC data)

#utility function:
def findMergerRatio(descIndices, masses, nodeIndices, node):
    desc_found = 0
    ratio = 0.0
    prog_masses = []
    prog_nodes = []
    for i in range(len(descIndices)):
        if descIndices[i] == node:
            desc_found += 1
            prog_masses.append(masses[i])
            prog_nodes.append(nodeIndices[i])
    if desc_found > 1:
        maxMass = max(prog_masses)
        maxIndex = prog_masses.index(maxMass)
        ratio = (sum(prog_masses) - maxMass)/maxMass
        return prog_nodes[maxIndex], ratio
    if desc_found == 1:
        return prog_nodes[0], 0.0
    if desc_found == 0:
        return -99, -99

def getMergerRatios(dI, ms, nI, outfile):
    print('getting merger ratios...')
    mergerRatioLists = []
    j = 0
    for i in range(len(dI)):
        if len(dI[i]) > 1:   # if the tree is more than just a single halo
            newList = []
            mergerRatioLists.append(newList)
            node = nI[i][0]
            descIndices = dI[i]
            nodeIndices = nI[i]
            masses = ms[i]
            
            while node != -99:
                node, ratio = findMergerRatio(descIndices, masses, nodeIndices, node)
                if node != -99:
                    mergerRatioLists[j].append(ratio)
            j += 1
    np.save(outfile, mergerRatioLists)
    csvoutfile = outfile + '.csv'
    with open(csvoutfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergerRatioLists:
            writer.writerow(item)

#######################

# FIXME:
def verifyData(filename, cluster_labels, cluster_colors):
    data = json.load(open(filename, "rb"))
    times = list(range(100))
    print("data size:", len(data))
    for i in range(len(data)):
        while (len(data[i]) < 100):
            data[i].append(0.0)
        data[i].reverse()
        plt.figure(int(cluster_labels[i]))
		#plt.scatter(times, data[i], color=cluster_colors[int(cluster_labels[i])])
        plt.scatter(times, data[i], color=cluster_colors[i % 14])
    plt.show()

#go through time series and find outliers
#FIXME: needs new function
"""
def getOutliers():
	index = 0
	index_list = []
	for series in pruned_data:
	n_mergers = 0
	for j in range(len(series)):
	if j > 0 and series[j] < 0.5*series[j-1] and series[j] != -99 and series[j] > 0.01:
	n_mergers += 1
	if n_mergers == 2:
	index_list.append(index)
	break
	size = len([elem for elem in series if elem > 0])
	print(series[int(size/4)])
	if series[int(size/4)] > 0.4:
	index_list.append(index)
	index += 1
	
	print("number of outliers: ", len(index_list))
	
	pickle.dump(index_list, open('e_outliers_early_mass.p', 'wb'))
"""


###################
# perform the DTW
# ...and get linkage

def dynamicTimeWarp(list1, list2):
    print('dynamic time warping...')
    n = len(list1)
    m = len(list2)
    dtw = np.zeros([n,m])
    for i in range(n):
        dtw[i,0] = float("inf")
    for i in range(m):
        dtw[0,1] = float("inf")
    dtw[0,0] = 0

    for i in range(n):
        for j in range(m):
            cost = np.absolute(list1[i] - list2[j])
            insertion = dtw[i-1,j  ]
            deletion  = dtw[i  ,j-1]
            match     = dtw[i-1,j-1]
            dtw[i,j] = cost + min(insertion, deletion, match)
    return dtw[n-1,m-1]

def similarity(file, outfile):
    print('calculating similarity matrix...')
    data = np.load(file)
    data = data.tolist()
    print(data)
    n = len(data)
    
    sim_matrix = np.zeros([n,n])            # initialize similarity matrix

    for i in range(n):                      # for each pair of arrays:
        print(i)
        for j in range(n-i):                # compare list[i] with list[i+j]
            if (i+j)%10 == 0: print(i+j)
            #similarity = dynamicTimeWarp(pruned_data[i], pruned_data[i+j]) # SLOW VERSION
            similarity, path = fastdtw(data[i], data[i+j], dist=euclidean)  # FAST VERSION

            sim_matrix[i][j+i] = similarity
            sim_matrix[j+i][i] = similarity # makin it symmetric

    np.save(outfile, sim_matrix)
    
    return sim_matrix                       # similarity/affinity matrix

def linkage(sim_matrix_file, outfile):
    print('calculating linkage...')
    data = np.load(sim_matrix_file)
    connectivity = np.array(data)
    
    similarity_vectors = []                             #flattened upper triangular representation
    for i in range(len(data)):
        for j in range(len(data)-i-1):
            similarity_vectors.append(data[i][i+j+1])
    sim_vectors = np.array(similarity_vectors)

    linkage = fastcluster.linkage(sim_vectors, method='complete')

    csvoutfile = outfile + '.csv'
    with open(outfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in linkage:
            writer.writerow(item)

##################

# FIXME:
def plot_completeness(marked_indices, linkage_file):
    out_ids = pickle.load(open(marked_indices, 'rb'))
    n_outliers = len(out_ids)
    for i in out_ids:
        print("outlier:", i)
    
    linkage =[]
    with open(linkage_file, 'r') as csvfile:
        linkreader = csv.reader(csvfile, delimiter=',')
        for row in linkreader:
            insert_row = []
            for item in row:
                insert_row.append(float(item))
            linkage.append(insert_row)
    n = len(linkage)
    print("number of outliers: ", n_outliers)
    print("number of trees: ", n)
    outlier_frac = []
    all_frac = []

    outlier_num = 0
    all_num = 0

    for i in range(len(linkage)):
        row = linkage[i]
        for j in range(2):
            if int(row[j]) in out_ids:
                outlier_num += 1
            if row[j] < n:
                all_num += 1
        outlier_frac.append(outlier_num/n_outliers)
        all_frac.append(all_num/n)

    out_plot, = plt.plot(np.arange(len(all_frac)), outlier_frac, color='g')
    all_plot, = plt.plot(np.arange(len(all_frac)), all_frac, color='b')
    plt.legend([out_plot, all_plot], ['outliers', 'all trees'])
    plt.xlabel('pairing step')
    plt.ylabel('fraction in cluster')
    plt.show()

# FIXME:
def example_pairing(main_masses_file, similarity_file):
    #load the files

    series = np.load(main_masses_file)
    similarity = np.load(similarity_file)
    
    sum = 0
    n = 0
    for i in range(len(similarity)):
        for j in range(len(similarity[i])):
            n += 1
            sum += similarity[i][j]
    print("average similarity: ", float(sum/n))

    for i in range(len(series)):
        max = np.amax(series[i])
        for j in range(len(series[i])):
            series[i][j] = series[i][j]/max
    #idx = random.randint(0, len(similarity))

    #find the worst similarity:
    #find the smallest number in the similarity matrix:
    largest_small = 0
    small_i = 0
    small_j = 0
    for i in range(len(similarity)):
        smallest = 10000
        for j in range(len(similarity[0])):
            if similarity[i][j] < smallest and similarity[i][j] > 0:
                smallest = similarity[i][j]
        print("smallest in row: ", smallest)
        if smallest > largest_small:
            largest_small = smallest
            small_i = i
    
    print("index found: ", small_i)
    idx = small_i


#idx = 200
    print(idx)



    similarity = similarity[idx]

    sorted_sim = np.argsort(similarity)

    ids = [idx, sorted_sim[1], sorted_sim[2], sorted_sim[3], sorted_sim[4], sorted_sim[int(len(sorted_sim)-3)], sorted_sim[int(len(sorted_sim)-2)], sorted_sim[int(len(sorted_sim)-1)]]
    print("similarities: ", similarity[sorted_sim[1]], similarity[sorted_sim[2]], similarity[sorted_sim[3]], similarity[sorted_sim[4]], similarity[sorted_sim[int(len(sorted_sim)-3)]], similarity[sorted_sim[int(len(sorted_sim)-2)]], similarity[sorted_sim[int(len(sorted_sim)-1)]])
    
    #ids = [idx, sorted_sim[1], sorted_sim[2], sorted_sim[3], sorted_sim[4], sorted_sim[5], sorted_sim[6]]

    data = series[ids]
    x = np.arange(len(series[0]))

    for j in range(len(ids)):
        series = data[j] # + np.full(len(data[j]), j)
        if j == 0 or j == 1:
            linewidth = 0
            color = 'b'
        if j == 2 or j == 3 or j == 4:
            linewidth = 0
            color = 'c'
        if j == 5 or j == 6 or j == 7:
            linewidth = 2
            color = 'r'
        plt.plot(x, series[::-1], linewidth=linewidth, color=color)

#plt.ylim([0,1])
    plt.ylim([-0.05, 1.05])
    plt.xlim([20, 100])

    plt.show()

main()
